<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Excel;
use App\Imports\SchoolsImport;
use App\Models\School;
use App\Models\Master\Province;
use App\Models\Master\ProvinceTarget;
use App\Models\Master\City;

class SchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->school = new School();
        $this->limit = 20;
    }

    public function index(Request $request)
    {
        $province       = Province::orderBy('province', 'ASC')->paginate($this->limit);
        $optProvince    = Province::orderBy('province', 'ASC')->get();
        $school_target  = ProvinceTarget::select(DB::raw('SUM(target) as total'))->where('target_year', date('Y'))->first();

        if (Auth::user()->level_id == 2) {
            $province       = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id)->paginate($this->limit);
            $optProvince    = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id)->get();
            $school_target  = ProvinceTarget::where('province_id', Auth::user()->province_id)->select(DB::raw('SUM(target) as total'))->where('target_year', date('Y'))->first();
        }

        $province->appends($request->all());

        $totalProvince = Province::count();
        $jsonProvince = json_encode($this->json());

        return view('school.index', compact('province', 'optProvince', 'school_target', 'jsonProvince', 'totalProvince'));
    }

    public function create(Request $request)
    {
        $province   = Province::orderBy('province', 'ASC')->get();

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id)->get();
        }

        return view('school.create', compact('province'));
    }

    public function store(Request $request)
    {
        $school                 = new School();
        $school->name           = $request->name;
        $school->city_id        = $request->city;
        $school->status         = $request->status;
        $school->accreditation  = $request->accreditation;
        $school->pbkp_ks        = $request->pbkp_ks;
        $school->year_intervention        = $request->year_intervention;
        $school->created_user   = Auth::user()->id;

        if (isset($request->no_pbkp_ks) && isset($request->year_pbkp_ks)) {
            $school->no_pbkp_ks     = $request->no_pbkp_ks;
            $school->year_pbkp_ks   = $request->year_pbkp_ks;
        }

        if ($school->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data sekolah berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data sekolah gagal ditambahkan');
        }

        return redirect('sekolah/detil/' . $request->province);
    }

    public function edit(Request $request, $id)
    {
        $school     = School::find($id);
        $province   = Province::orderBy('province', 'ASC')->get();
        $city       = City::orderBy('city', 'ASC')->get();

        return view('school.edit', compact('school', 'province', 'city'));
    }

    public function update(Request $request, $id)
    {
        $school                 = School::find($id);
        $school->name           = $request->name;
        $school->city_id        = $request->city;
        $school->status         = $request->status;
        $school->accreditation  = $request->accreditation;
        $school->pbkp_ks        = $request->pbkp_ks;
        $school->updated_user   = Auth::user()->id;
        $school->no_pbkp_ks     = $request->no_pbkp_ks;
        $school->year_pbkp_ks   = $request->year_pbkp_ks;
        $school->year_intervention        = $request->year_intervention;

        if ($school->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data sekolah berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data sekolah gagal diubah');
        }

        return redirect('sekolah/detil/' . $request->province);
    }

    public function show(Request $request, $id)
    {
        $school     = $this->school->dataPaginate($request, $id);
        $province   = Province::find($id);

        return view('school.show', compact('school', 'province'));
    }

    public function filter(Request $request)
    {
        $school     = $this->school->dataPaginate($request, $request->province_id);
        $province   = Province::find($request->province_id);

        return view('school.show', compact('school', 'province'));
    }

    public function delete(Request $request, $id)
    {
        School::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data sekolah berhasil dihapus');

        return redirect()->back();
    }

    public function import(Request $request)
    {
        Excel::import(new SchoolsImport($request->province_id), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data sekolah berhasil diimport');

        return redirect()->back();
    }

    public function json()
    {
        if (Auth::user()->level_id == 2) {
            $provinces = Province::where('province_id', Auth::user()->province_id)->get();
        } else {
            $provinces = Province::all();
        }

        $json = [];

        if ($provinces) {
            foreach ($provinces as $key => $province) {
                $json[] = [
                    'name' => $province->province,
                    'location' => [$province->latitude, $province->longitude]
                ];
            }
        }

        return $json;
    }
}
