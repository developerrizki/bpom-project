<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Report;
use App\Models\Report\File;
use App\Models\Setting\Notification;

class TotController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->report = new Report();
        $this->is_type = 0;
    }

    public function index(Request $request)
    {
        $report = $this->report->dataPaginate($request, $this->is_type);

        return view('report.tot.index', compact('report'));
    }

    public function create(Request $request)
    {
        return view('report.tot.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
        ]);

        $file     = $request->file('file');

        DB::beginTransaction();

        $report               = new Report();
        $report->name         = $request->name;
        $report->description  = $request->description;
        $report->created_user = Auth::user()->id;
        $report->type         = $this->is_type;
        $report->save();

        $report_file          = new File();

        if ($file) {
            $report_file->file = $file->getClientOriginalName();
        }
            //save file to folder public
            $file->storeAs('public/laporan-tot', $file->getClientOriginalName());
            $report_file->report_id = $report->report_id;
            $report_file->is_support = 0;

        if ($report_file->save()) {
            $notification = new Notification();
            $notification->title = "Menambahkan Laporan TOT Baru";
            $notification->description = Auth::user()->name . " berhasil menambahkan laporan TOT baru dengan judul : " . $request->name . " ";
            $notification->user_id = Auth::user()->id;
            $notification->save();

            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data Trainning Of Trainner berhasil ditambahkan');
        } else {
            DB::rollback();

            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data Trainning Of Trainner gagal ditambahkan');
        }

        return redirect('laporan-tot');
    }

    public function show(Request $request, $id)
    {
        $tot = Report::find($id);
        $file = File::where('report_id', $id)->first();

        return view('report.tot.show', compact('tot', 'file'));
    }

    public function edit(Request $request, $id)
    {
        $tot = Report::find($id);
        $file = File::where('report_id', $id)->first();

        return view('report.tot.edit', compact('tot', 'file'));
    }

    public function update(Request $request, $id)
    {
        $file     = $request->file('file');

        DB::beginTransaction();

        $report               = Report::find($id);
        $report->name         = $request->name;
        $report->description  = $request->description;
        $report->created_user = Auth::user()->id;
        $report->type         = $this->is_type;
        $report->save();

        if ($file) {
            $report_file          = File::where('report_id', $id)->first();
            $report_file->file = $file->getClientOriginalName();
            //save file to folder public
            $file->storeAs('public/laporan-tot', $file->getClientOriginalName());
            $report_file->report_id = $report->report_id;
            $report_file->is_support = 0;

            if ($report_file->save()) {
                $notification = new Notification();
                $notification->title = "Menambahkan Laporan TOT Baru";
                $notification->description = Auth::user()->name . " berhasil mengubah laporan TOT baru dengan judul : " . $request->name . " ";
                $notification->user_id = Auth::user()->id;
                $notification->save();

                DB::commit();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data Trainning Of Trainner berhasil diubah');
            } else {
                DB::rollback();

                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data Trainning Of Trainner gagal diubah');
            }
        } else {
            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data Trainning Of Trainner berhasil diubah');
        }

        return redirect('laporan-tot');
    }

    public function delete(Request $request, $id)
    {
        Report::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data sekolah berhasil dihapus');

        return redirect()->back();
    }

    public function storeFile(Request $request, $id)
    {
        $request->validate([
            'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
        ]);

        $file     = $request->file('file');
        $report_file          = new File();

        if ($file) {
            $report_file->file = $file->getClientOriginalName();
            //save file to folder public
            $file->storeAs('public/laporan-tot', $file->getClientOriginalName());
            $report_file->report_id = $id;
            $report_file->is_support = 1;
            $report_file->save();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'File pendukung berhasil diunggah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'File pendukung gagal diunggah');
        }


        return redirect()->back();
    }

    public function deleteFile(Request $request, $id)
    {
        $file = File::findOrFail($id);

        if ($file) {
            $filePath = \Storage::url('public/laporan-tot/' . $file->file);

            if (file_exists($filePath)) {
                unlink($filePath);
            }

            $file->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'File Pendukung Laporan TOT berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'File Pendukung Laporan TOT gagal dihapus');

        return redirect()->back();
    }
}
