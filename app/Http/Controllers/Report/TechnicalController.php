<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\IntervieweesImport;
use App\Imports\LinsekReportsImport;
use App\Imports\CommunityReportsImport;
use App\Imports\SchoolReportsImport;
use App\Models\Report;
use App\Models\Report\Interviewees;
use App\Models\Report\Linsek;
use App\Models\Report\LinsekFile;
use App\Models\Report\Gallery;
use App\Models\Report\Document;
use App\Models\Report\School;
use App\Models\Report\Community;
use App\Models\Report\Summary;
use App\Models\Master\Province;
use App\Models\Master\City;
use App\Models\Setting\Notification;
use App\Models\School as SchoolMaster;

ini_set('upload_max_filesize', '10M');
ini_set('post_max_size', '10M');

class TechnicalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->is_type = 2;
    }

    public function index(Request $request)
    {
        $province = Province::orderBy('province', 'ASC');

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id);
        }

        if ($request->has('province')) {
            $province->where('province', 'like', '%' . $request->province . '%');
        }

        $province = $province->get();

        return view('report.technical.index', compact('province'));
    }

    public function detail(Request $request, $province_id)
    {
        $province = Province::find($province_id);
        $report   = Report::where('province_id', $province_id)
                        ->where('type', $this->is_type);

        if ($request->has('key') && $request->has('value')) {
            $report->where($request->key, 'like', '%' . $request->value . '%');
        }

        $report = $report->get();

        return view('report.technical.detail', compact('report', 'province_id', 'province'));
    }

    public function show(Request $request, $id)
    {
        $report = Report::find($id);

        return view('report.technical.show', compact('report'));
    }

    public function foto(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.technical.foto', compact('report'));
    }

    public function createFoto(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $request->validate([
                'file' => 'required|file|mimes:jpg,jpeg,png,svg,bmp|max:10240'
            ]);

            $report = Report::find($id);

            if (sizeof($report->galleries) < 6) {
                $file   = $request->file('file');

                $foto = new Gallery();

                if ($file) {
                    $foto->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/foto-kegiatan', $file->getClientOriginalName());

                $foto->report_id = $report->report_id;

                if ($foto->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Foto kegiatan berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Foto kegiatan gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-bimtek/foto-kegiatan/' . $report->report_id);
        }

        return view('report.technical.foto_create', compact('report'));
    }

    public function document(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.technical.document', compact('report'));
    }

    public function createDocument(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $request->validate([
                'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
            ]);

            $report = Report::find($id);

            if (sizeof($report->documents) < 6) {
                $file   = $request->file('file');

                $document = new Document();

                if ($file) {
                    $document->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/dokumen-bimtek', $file->getClientOriginalName());

                $document->report_id = $report->report_id;

                if ($document->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Foto kegiatan berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Foto kegiatan gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-bimtek/dokumen/' . $report->report_id);
        }

        return view('report.technical.document_create', compact('report'));
    }

    public function createReport(Request $request, $province_id)
    {
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            DB::beginTransaction();
                $report               = new Report();
                $report->name         = $request->name;
                $report->date         = date('Y-m-d', strtotime($request->date));
                $report->description  = $request->description;
                $report->province_id  = $province_id;
                $report->created_user = Auth::user()->id;
                $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data laporan bimtek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data laporan bimtek gagal ditambahkan');
            }

                $notification = new Notification();
                $notification->title = "Menambahkan Laporan Bimtek Baru";
                $notification->description = Auth::user()->name . " berhasil menambahkan laporan bimtek baru dengan judul : " . $request->name . " ";
                $notification->user_id = Auth::user()->id;
                $notification->save();
            DB::commit();

            return redirect('laporan-bimtek/show/' . $report->report_id);
        }

        return view('report.technical.create_report', compact('province', 'province_id'));
    }

    public function editReport(Request $request, $id)
    {
        $report = Report::find($id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $report               = Report::find($id);
            $report->name         = $request->name;
            $report->date         = date('Y-m-d', strtotime($request->date));
            $report->description  = $request->description;
            $report->created_user = Auth::user()->id;
            $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data laporan bimtek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data laporan bimtek gagal ditambahkan');
            }

            return redirect('laporan-bimtek/detil/' . $report->province_id);
        }

        return view('report.technical.edit_report', compact('province', 'report'));
    }

    public function linsek(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.technical.linsek', compact('report'));
    }

    public function createLinsek(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $linsek               = new Linsek();
            $linsek->name         = $request->name;
            $linsek->city_id      = $request->city_id;
            $linsek->participant  = $request->participant;
            $linsek->report_id    = $report_id;

            if ($linsek->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data peserta pada laporan bimtek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data peserta pada laporan bimtek gagal ditambahkan');
            }

            return redirect('laporan-bimtek/linsek/' . $report_id);
        }

        return view('report.technical.linsek_create', compact('report', 'report_id', 'province'));
    }

    public function editLinsek(Request $request, $id)
    {
        $linsek               = Linsek::find($id);
        $province             = Province::orderBy('province', 'ASC')->get();
        $city                 = City::orderBy('city', 'ASC')->where('province_id', $linsek->city->province_id)->get();

        $report               = Report::find($linsek->report_id);

        if ($request->method() == 'POST') {
            $linsek               = Linsek::find($id);
            $linsek->name         = $request->name;
            $linsek->city_id      = $request->city_id;
            $linsek->participant  = $request->participant;

            if ($linsek->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data peserta pada laporan bimtek berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data peserta pada laporan bimtek gagal diubah');
            }

            return redirect('laporan-bimtek/linsek/' . $linsek->report_id);
        }

        return view('report.technical.linsek_edit', compact('linsek', 'province', 'city', 'report'));
    }

    public function createLinsekFile(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $request->validate([
                'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
            ]);

            $report = Report::find($id);

            $file   = $request->file('file');

            $linsek_files = new LinsekFile();

            if ($file) {
                $linsek_files->file = $file->getClientOriginalName();
            }
                //save file to folder public
                $file->storeAs('public/dokumen-linsek', $file->getClientOriginalName());

            $linsek_files->report_id = $report->report_id;

            if ($linsek_files->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Dokumen linsek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Dokumen linsek gagal ditambahkan');
            }

            return redirect('laporan-bimtek/linsek/' . $report->report_id);
        }

        return view('report.technical.linsek_file_create', compact('report'));
    }

    public function school(Request $request, $id)
    {
        $report = Report::find($id);
        $report_school = School::where('report_id', $report->report_id)
                            ->pluck('school_id')->toArray();

        $school = SchoolMaster::whereIn('school_id', $report_school)->paginate(25);

        return view('report.technical.school', compact('report', 'school'));
    }

    public function createSchool(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $school_id = School::where('report_id', $report->report_id)->pluck('school_id')->toArray();

        $school   = SchoolMaster::join('cities', 'schools.city_id', '=', 'cities.city_id')
                            ->join('provinces', 'provinces.province_id', '=', 'cities.province_id')
                            ->where('provinces.province_id', $report->province_id);

        // if(isset($school_id)) {
        //     $school = $school->whereNotIn('school_id', $school_id);
        // }

        $school = $school->get();

        if ($request->method() == 'POST') {
            $school               = new School();
            $school->school_id    = $request->school_id;
            $school->canteen      = $request->canteen;
            $school->condition    = $request->condition;
            $school->report_id    = $report_id;

            if ($school->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan bimtek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan bimtek gagal ditambahkan');
            }

            return redirect('laporan-bimtek/daftar-sekolah/' . $report_id);
        }

        return view('report.technical.school_create', compact('report', 'report_id', 'school'));
    }

    public function editSchool(Request $request, $id)
    {
        $report_school  = School::find($id);
        $report  = Report::find($report_school->report_id);

        $school  = SchoolMaster::where('province_id', $report->province_id)
                    ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            $school               = School::find($id);
            $school->school_id    = $request->school_id;
            $school->canteen      = $request->canteen;
            $school->condition    = $request->condition;

            if ($school->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan bimtek berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan bimtek gagal diubah');
            }

            return redirect('laporan-bimtek/daftar-sekolah/' . $school->report_id);
        }

        return view('report.technical.school_edit', compact('school', 'report_school', 'report'));
    }

    public function community(Request $request, $id)
    {
        $report = Report::find($id);
        $school_id = Community::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school = SchoolMaster::whereIn('school_id', $school_id)->paginate(25);

        return view('report.technical.community', compact('report', 'school'));
    }

    public function createCommunity(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $school_id = Community::where('report_id', $report->report_id)->pluck('school_id')->toArray();

        $school   = SchoolMaster::where('province_id', $report->province_id)
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id');

        if (isset($school_id)) {
            $school = $school->whereNotIn('school_id', $school_id);
        }

        $school = $school->get();

        if ($request->method() == 'POST') {
            $community               = new Community();
            $community->school_id    = $request->school_id;
            $community->school_community      = $request->school_community;
            $community->sum_community    = $request->sum_community;
            $community->report_id    = $report_id;

            if ($community->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan bimtek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan bimtek gagal ditambahkan');
            }

            return redirect('laporan-bimtek/komunitas/' . $report_id);
        }

        return view('report.technical.community_create', compact('report', 'report_id', 'school'));
    }

    public function editCommunity(Request $request, $id)
    {
        $community  = Community::find($id);
        $report  = Report::find($community->report_id);

        $school  = SchoolMaster::where('province_id', $report->province_id)
                    ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            $community                        = Community::find($id);
            $community->school_id             = $request->school_id;
            $community->school_community      = $request->school_community;
            $community->sum_community         = $request->sum_community;

            if ($community->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan bimtek berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan bimtek gagal diubah');
            }

            return redirect('laporan-bimtek/komunitas/' . $community->report_id);
        }

        return view('report.technical.community_edit', compact('school', 'community', 'report'));
    }

    public function createInterview(Request $request, $report_id)
    {
        $report  = Report::find($report_id);

        if ($request->method() == 'POST') {
            $interviewees               = new Interviewees();
            $interviewees->name         = $request->name;
            $interviewees->agency       = $request->agency;
            $interviewees->position     = $request->position;
            $interviewees->theory       = $request->theory;
            $interviewees->report_id    = $report_id;

            if ($interviewees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data narasumber pada laporan bimtek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data narasumber pada laporan bimtek gagal ditambahkan');
            }

            return redirect('laporan-bimtek/show/' . $report_id);
        }

        return view('report.technical.create_interview', compact('report_id', 'report'));
    }

    public function editInterview(Request $request, $id)
    {
        $interviewees               = Interviewees::find($id);
        $report  = Report::find($interviewees->report_id);

        if ($request->method() == 'POST') {
            $interviewees               = Interviewees::find($id);
            $interviewees->name         = $request->name;
            $interviewees->agency       = $request->agency;
            $interviewees->position     = $request->position;
            $interviewees->theory       = $request->theory;

            if ($interviewees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data narasumber pada laporan bimtek berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data narasumber pada laporan bimtek gagal diubah');
            }

            return redirect('laporan-bimtek/show/' . $interviewees->report_id);
        }

        return view('report.technical.edit_interview', compact('interviewees', 'report'));
    }

    public function createSummary(Request $request, $report_id)
    {
        $report  = Report::find($report_id);

        if ($request->method() == 'POST') {
            $summary               = new Summary();
            $summary->summary      = $request->summary;
            $summary->report_id    = $request->report_id;

            if ($summary->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data summary pada laporan bimtek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data summary pada laporan bimtek gagal ditambahkan');
            }

            return redirect('laporan-bimtek/show/' . $request->report_id);
        }

        return view('report.technical.create_summary', compact('report_id', 'report'));
    }

    public function editSummary(Request $request, $id)
    {
        $summary               = Summary::find($id);
        $report  = Report::find($summary->report_id);

        if ($request->method() == 'POST') {
            $summary               = Summary::find($id);
            $summary->summary      = $request->summary;

            if ($summary->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data summary pada laporan bimtek berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data summary pada laporan bimtek gagal diubah');
            }

            return redirect('laporan-bimtek/show/' . $summary->report_id);
        }

        return view('report.technical.edit_summary', compact('summary', 'report'));
    }

    public function importInterview(Request $request, $reportID)
    {
        Excel::import(new IntervieweesImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data narasumber berhasil diimport');

        return redirect()->back();
    }

    public function importSchool(Request $request, $reportID)
    {
        Excel::import(new SchoolReportsImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data sekolah berhasil diimport');

        return redirect()->back();
    }

    public function importCommunity(Request $request, $reportID)
    {
        Excel::import(new CommunityReportsImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data peserta komunitas sekolah berhasil diimport');

        return redirect()->back();
    }

    public function importLinsek(Request $request, $reportID)
    {
        Excel::import(new LinsekReportsImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data peserta linsek berhasil diimport');

        return redirect()->back();
    }

    public function deleteReport(Request $request, $id)
    {
        DB::beginTransaction();

            Interviewees::where('report_id', $id)->delete();
            Summary::where('report_id', $id)->delete();
            Linsek::where('report_id', $id)->delete();
            LinsekFile::where('report_id', $id)->delete();
            Gallery::where('report_id', $id)->delete();
            Document::where('report_id', $id)->delete();
            School::where('report_id', $id)->delete();
            Community::where('report_id', $id)->delete();
            Report::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data laporan bimtek berhasil dihapus');

        return redirect()->back();
    }

    public function deleteInteview(Request $request, $id)
    {
        Interviewees::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data narasumber pada laporan bimtek berhasil dihapus');

        return redirect()->back();
    }

    public function deleteSummary(Request $request, $id)
    {
        Summary::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data summary pada laporan bimtek berhasil dihapus');

        return redirect()->back();
    }

    public function deleteLinsek(Request $request, $id)
    {
        Linsek::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data perserta pada laporan bimtek berhasil dihapus');

        return redirect()->back();
    }

    public function deleteSchool(Request $request, $id)
    {
        School::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Daftar sekolah pada laporan bimtek berhasil dihapus');

        return redirect()->back();
    }

    public function deleteCommunity(Request $request, $id)
    {
        Community::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Peserta komunitas sekolah pada laporan bimtek berhasil dihapus');

        return redirect()->back();
    }

    public function deleteFoto(Request $request, $id)
    {
        $document = Gallery::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/foto-kegiatan/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Foto kegiatan berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Foto kegiatan gagal dihapus');

        return redirect()->back();
    }

    public function deleteDocument(Request $request, $id)
    {
        $document = Document::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/dokumen-bimtek/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dokumen kegiatan berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Dokumen kegiatan gagal dihapus');

        return redirect()->back();
    }

    public function deleteLinsekFile(Request $request, $id)
    {
        $file = LinsekFile::findOrFail($id);

        if ($file) {
            $filePath = \Storage::url('public/dokumen-linsek/' . $file->file);

            if (file_exists($filePath)) {
                unlink($filePath);
            }

            $file->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dokumen linsek berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Dokumen linsek gagal dihapus');

        return redirect()->back();
    }
}
