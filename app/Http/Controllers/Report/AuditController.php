<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Report;
use App\Models\Report\Interviewees;
use App\Models\Report\Summary;
use App\Models\Report\Audit;
use App\Models\Report\Document;
use App\Models\Report\Gallery;
use App\Models\Master\Province;
use App\Models\Setting\Notification;

class AuditController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->is_type = 5;
    }

    public function index(Request $request)
    {
        $province = Province::orderBy('province', 'ASC');

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id);
        }

        if ($request->has('province')) {
            $province->where('province', 'like', '%' . $request->province . '%');
        }

        $province = $province->get();

        return view('report.audit.index', compact('province'));
    }

    public function detail(Request $request, $province_id)
    {
        $report   = Report::where('province_id', $province_id)
                        ->where('type', $this->is_type)->get();

        return view('report.audit.detail', compact('report', 'province_id'));
    }

    public function show(Request $request, $id)
    {
        $report = Report::find($id);

        return view('report.audit.show', compact('report'));
    }

    public function createReport(Request $request, $province_id)
    {
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            DB::beginTransaction();

                $report               = new Report();
                $report->name         = $request->name;
                $report->date         = date('Y-m-d', strtotime($request->date));
                $report->description  = $request->description;
                $report->province_id  = $province_id;
                $report->created_user = Auth::user()->id;
                $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data audit pbkp-ks berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data audit pbkp-ks gagal ditambahkan');
            }

                $notification = new Notification();
                $notification->title = "Menambahkan Laporan Audit Baru";
                $notification->description = Auth::user()->name . " berhasil menambahkan laporan audit baru dengan judul : " . $request->name . " ";
                $notification->user_id = Auth::user()->id;
                $notification->save();

            DB::commit();


            return redirect('laporan-audit/show/' . $report->report_id);
        }

        return view('report.audit.create_report', compact('province', 'province_id'));
    }

    public function editReport(Request $request, $id)
    {
        $report = Report::find($id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $report               = Report::find($id);
            $report->name         = $request->name;
            $report->date         = date('Y-m-d', strtotime($request->date));
            $report->description  = $request->description;
            $report->created_user = Auth::user()->id;
            $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data audit pbkp-ks berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data audit pbkp-ks gagal ditambahkan');
            }

            return redirect('laporan-audit/detil/' . $report->province_id);
        }

        return view('report.audit.edit_report', compact('province', 'report'));
    }

    public function document(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.audit.document', compact('report'));
    }

    public function createDocument(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $report = Report::find($id);

            if (sizeof($report->documents) < 6) {
                $file   = $request->file('file');

                $document = new Document();

                if ($file) {
                    $document->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/dokumen-audit', $file->getClientOriginalName());

                $document->report_id = $report->report_id;

                if ($document->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Dokumen kegiatan berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Dokumen kegiatan gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-audit/dokumen/' . $report->report_id);
        }

        return view('report.audit.document_create', compact('report'));
    }

    public function deleteDocument(Request $request, $id)
    {
        $document = Document::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/document-advokasi/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dokumen kegiatan berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Dokumen kegiatan gagal dihapus');

        return redirect()->back();
    }

    public function school(Request $request, $id)
    {
        $report = Report::find($id);
        $school_id = Audit::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school = \App\Models\School::whereIn('school_id', $school_id)->paginate(25);

        return view('report.audit.school', compact('report', 'school'));
    }

    public function createSchool(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $school_id = Audit::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school   = \App\Models\School::where('province_id', $report->province_id)
                        ->whereNotIn('school_id', $school_id)
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            try {
                DB::beginTransaction();
                    $audit                      = new Audit();
                    $audit->school_id           = $request->school_id;
                    $audit->headmaster          = $request->headmaster;
                    $audit->address             = $request->address;
                    $audit->score               = $request->score;
                    $audit->incompatibility     = $request->incompatibility;
                    $audit->report_id           = $report_id;
                    $audit->save();
                DB::commit();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada data audit pbkp-ks berhasil ditambahkan');
            } catch (\Exception $e) {
                DB::rollback();

                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', $e->getMessage());
            }

            return redirect('laporan-audit/daftar-sekolah/' . $report_id);
        }

        return view('report.audit.school_create', compact('report', 'report_id', 'school'));
    }

    public function editSchool(Request $request, $id)
    {
        $report_school  = Audit::find($id);
        $report  = Report::find($report_school->report_id);

        $school  = \App\Models\School::where('province_id', $report->province_id)
                    ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            try {
                DB::beginTransaction();
                    $audit                      = Audit::find($id);
                    $audit->school_id           = $request->school_id;
                    $audit->headmaster          = $request->headmaster;
                    $audit->address             = $request->address;
                    $audit->score               = $request->score;
                    $audit->incompatibility     = $request->incompatibility;
                    $audit->save();
                DB::commit();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada data audit pbkp-ks berhasil diubah');
            } catch (\Exception $e) {
                DB::rollback();

                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar sekolah pada data audit pbkp-ks gagal diubah');
            }

            return redirect('laporan-audit/daftar-sekolah/' . $report->report_id);
        }

        return view('report.audit.school_edit', compact('school', 'report_school'));
    }

    public function foto(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.audit.foto', compact('report'));
    }

    public function createFoto(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $report = Report::find($id);

            if (sizeof($report->galleries) < 6) {
                $file   = $request->file('file');

                $foto = new Gallery();

                if ($file) {
                    $foto->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/foto-audit', $file->getClientOriginalName());

                $foto->report_id = $report->report_id;

                if ($foto->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Foto audit berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Foto audit gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-audit/foto-audit/' . $report->report_id);
        }

        return view('report.audit.foto_create', compact('report'));
    }

    public function deleteFoto(Request $request, $id)
    {
        $document = Gallery::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/foto-audit/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Foto audit berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Foto audit gagal dihapus');

        return redirect()->back();
    }

    public function deleteReport(Request $request, $id)
    {
        DB::beginTransaction();

            Audit::where('report_id', $id)->delete();
            Report::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data audit pbkp-ks berhasil dihapus');

        return redirect()->back();
    }

    public function deleteSchool(Request $request, $id)
    {
        Audit::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Daftar sekolah pada data audit pbkp-ks berhasil dihapus');

        return redirect()->back();
    }
}
