<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\ReportFinals;
use App\Models\Setting\Notification;

class FinalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->reportFinal = new ReportFinals();
    }

    public function index(Request $request)
    {
        $report     = $this->reportFinal->dataPaginate($request);

        return view('report.final.index', compact('report'));
    }

    public function create(Request $request)
    {
        return view('report.final.create');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

            $report                 = new ReportFinals();
            $report->title           = $request->title;
            $report->summary        = $request->summary;
            $report->foreword         = $request->foreword;
            $report->preliminary  = $request->preliminary;
            $report->destination        = $request->destination;
            $report->target        = $request->target;
            $report->obstacle        = $request->obstacle;
            $report->recommendation        = $request->recommendation;
            $report->closing        = $request->closing;
            $report->created_user   = Auth::user()->id;

            $file   = $request->file('file');

        if ($file) {
            $report->file = $file->getClientOriginalName();
        }
                //save file to folder public
                $file->storeAs('public/laporan-akhir', $file->getClientOriginalName());

        if ($report->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Laporan akhir berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Laporan akhir gagal ditambahkan');
        }

            $notification = new Notification();
            $notification->title = "Menambahkan Laporan Akhir";
            $notification->description = Auth::user()->name . " berhasil menambahkan laporan akhir dengan judul : " . $request->title . " ";
            $notification->user_id = Auth::user()->id;
            $notification->save();

        DB::commit();

        return redirect('laporan-akhir');
    }

    public function edit(Request $request, $id)
    {
        $report     = ReportFinals::find($id);

        return view('report.final.edit', compact('report'));
    }

    public function update(Request $request, $id)
    {
        $report                 = ReportFinals::find($id);
        $report->title           = $request->title;
        $report->summary        = $request->summary;
        $report->foreword         = $request->foreword;
        $report->preliminary  = $request->preliminary;
        $report->destination        = $request->destination;
        $report->target        = $request->target;
        $report->obstacle        = $request->obstacle;
        $report->recommendation        = $request->recommendation;
        $report->closing        = $request->closing;
        $report->updated_user   = Auth::user()->id;

        $file   = $request->file('file');

        if ($file) {
            $report->file = $file->getClientOriginalName();
        }
            //save file to folder public
            $file->storeAs('public/laporan-akhir', $file->getClientOriginalName());

        if ($report->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Laporan akhir berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Laporan akhir gagal diubah');
        }

        return redirect('laporan-akhir');
    }

    public function delete(Request $request, $id)
    {
        ReportFinals::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Laporan akhir berhasil dihapus');

        return redirect()->back();
    }
}
