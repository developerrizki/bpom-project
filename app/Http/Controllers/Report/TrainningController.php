<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Excel;
use App\Imports\IntervieweesImport;
use App\Imports\CommunityReportsImport;
use App\Models\Report;
use App\Models\Report\Interviewees;
use App\Models\Report\Linsek;
use App\Models\Report\LinsekFile;
use App\Models\Report\Gallery;
use App\Models\Report\Document;
use App\Models\Report\School;
use App\Models\Report\Community;
use App\Models\Report\Summary;
use App\Models\Master\Province;
use App\Models\Master\City;
use App\Models\Setting\Notification;

class TrainningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->is_type = 3;
    }

    public function index(Request $request)
    {
        $province = Province::orderBy('province', 'ASC');

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id);
        }

        if ($request->has('province')) {
            $province->where('province', 'like', '%' . $request->province . '%');
        }

        $province = $province->get();

        return view('report.trainning.index', compact('province'));
    }

    public function detail(Request $request, $province_id)
    {
        $province = Province::find($province_id);
        $report   = Report::where('province_id', $province_id)
                        ->where('type', $this->is_type);

        if ($request->has('key') && $request->has('value')) {
            $report->where($request->key, 'like', '%' . $request->value . '%');
        }

        $report = $report->get();

        return view('report.trainning.detail', compact('report', 'province_id', 'province'));
    }

    public function show(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.trainning.show', compact('report'));
    }

    public function foto(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.trainning.foto', compact('report'));
    }

    public function createFoto(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $request->validate([
                'file' => 'required|file|mimes:jpg,jpeg,png,svg,bmp|max:10240'
            ]);

            $report = Report::find($id);

            if (sizeof($report->galleries) < 6) {
                $file   = $request->file('file');

                $foto = new Gallery();

                if ($file) {
                    $foto->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/foto-kegiatan', $file->getClientOriginalName());

                $foto->report_id = $report->report_id;

                if ($foto->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Foto kegiatan berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Foto kegiatan gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-pelatihan/foto-kegiatan/' . $report->report_id);
        }

        return view('report.trainning.foto_create', compact('report'));
    }

    public function document(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.trainning.document', compact('report'));
    }

    public function createDocument(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $report = Report::find($id);

            if (sizeof($report->documents) < 6) {
                $file   = $request->file('file');

                $document = new Document();

                if ($file) {
                    $document->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/dokumen-pelatihan', $file->getClientOriginalName());

                $document->report_id = $report->report_id;

                if ($document->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Foto kegiatan berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Foto kegiatan gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-pelatihan/dokumen/' . $report->report_id);
        }

        return view('report.trainning.document_create', compact('report'));
    }

    public function createReport(Request $request, $province_id)
    {
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            DB::beginTransaction();

                $report               = new Report();
                $report->name         = $request->name;
                $report->date         = date('Y-m-d', strtotime($request->date));
                $report->description  = $request->description;
                $report->province_id  = $province_id;
                $report->created_user = Auth::user()->id;
                $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data laporan pelatihan berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data laporan pelatihan gagal ditambahkan');
            }

                $notification = new Notification();
                $notification->title = "Menambahkan Laporan Pelatihan Baru";
                $notification->description = Auth::user()->name . " berhasil menambahkan laporan pelatihan baru dengan judul : " . $request->name . " ";
                $notification->user_id = Auth::user()->id;
                $notification->save();

            DB::commit();

            return redirect('laporan-pelatihan/show/' . $report->report_id);
        }

        return view('report.trainning.create_report', compact('province', 'province_id'));
    }

    public function editReport(Request $request, $id)
    {
        $report = Report::find($id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $report               = Report::find($id);
            $report->name         = $request->name;
            $report->date         = date('Y-m-d', strtotime($request->date));
            $report->description  = $request->description;
            $report->created_user = Auth::user()->id;
            $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data laporan pelatihan berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data laporan pelatihan gagal ditambahkan');
            }

            return redirect('laporan-pelatihan/detil/' . $report->province_id);
        }

        return view('report.trainning.edit_report', compact('province', 'report'));
    }

    public function linsek(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.trainning.linsek', compact('report'));
    }

    public function createLinsek(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $linsek               = new Linsek();
            $linsek->name         = $request->name;
            $linsek->city_id      = $request->city_id;
            $linsek->participant  = $request->participant;
            $linsek->report_id    = $report_id;

            if ($linsek->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data peserta pada laporan pelatihan berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data peserta pada laporan pelatihan gagal ditambahkan');
            }

            return redirect('laporan-pelatihan/linsek/' . $report_id);
        }

        return view('report.trainning.linsek_create', compact('report', 'report_id', 'province'));
    }

    public function editLinsek(Request $request, $id)
    {
        $linsek               = Linsek::find($id);
        $province             = Province::orderBy('province', 'ASC')->get();
        $city                 = City::orderBy('city', 'ASC')->where('province_id', $linsek->city->province_id)->get();

        if ($request->method() == 'POST') {
            $linsek               = Linsek::find($id);
            $linsek->name         = $request->name;
            $linsek->city_id      = $request->city_id;
            $linsek->participant  = $request->participant;

            if ($linsek->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data peserta pada laporan pelatihan berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data peserta pada laporan pelatihan gagal diubah');
            }

            return redirect('laporan-pelatihan/linsek/' . $linsek->report_id);
        }

        return view('report.trainning.linsek_edit', compact('linsek', 'province', 'city'));
    }

    public function createLinsekFile(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $report = Report::find($id);

            $file   = $request->file('file');

            $linsek_files = new LinsekFile();

            if ($file) {
                $linsek_files->file = $file->getClientOriginalName();
            }
                //save file to folder public
                $file->storeAs('public/dokumen-linsek', $file->getClientOriginalName());

            $linsek_files->report_id = $report->report_id;

            if ($linsek_files->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Dokumen linsek berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Dokumen linsek gagal ditambahkan');
            }

            return redirect('laporan-pelatihan/linsek/' . $report->report_id);
        }

        return view('report.trainning.linsek_file_create', compact('report'));
    }

    public function school(Request $request, $id)
    {
        $report = Report::find($id);
        $report_school = School::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school = \App\Models\School::whereIn('school_id', $report_school)->paginate(25);

        return view('report.trainning.school', compact('report', 'school'));
    }

    public function createSchool(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $school_id = School::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school   = \App\Models\School::where('province_id', $report->province_id)
                        ->whereNotIn('school_id', $school_id)
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            $school               = new School();
            $school->school_id    = $request->school_id;
            $school->canteen      = $request->canteen;
            $school->condition    = $request->condition;
            $school->report_id    = $report_id;

            if ($school->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan pelatihan berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan pelatihan gagal ditambahkan');
            }

            return redirect('laporan-pelatihan/daftar-sekolah/' . $report_id);
        }

        return view('report.trainning.school_create', compact('report', 'report_id', 'school'));
    }

    public function editSchool(Request $request, $id)
    {
        $report_school  = School::find($id);
        $report  = Report::find($report_school->report_id);

        $school  = \App\Models\School::where('province_id', $report->province_id)
                    ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            $school               = School::find($id);
            $school->school_id    = $request->school_id;
            $school->canteen      = $request->canteen;
            $school->condition    = $request->condition;

            if ($school->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan pelatihan berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar sekolah pada laporan pelatihan gagal diubah');
            }

            return redirect('laporan-pelatihan/daftar-sekolah/' . $school->report_id);
        }

        return view('report.trainning.school_edit', compact('school', 'report_school'));
    }

    public function community(Request $request, $id)
    {
        $report = Report::find($id);
        $school_id = Community::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school = \App\Models\School::whereIn('school_id', $school_id)->paginate(25);

        return view('report.trainning.community', compact('report', 'school'));
    }

    public function createCommunity(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $school_id = Community::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school   = \App\Models\School::where('province_id', $report->province_id)
                        ->whereNotIn('school_id', $school_id)
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            $community               = new Community();
            $community->school_id    = $request->school_id;
            $community->school_community      = $request->school_community;
            $community->sum_community    = $request->sum_community;
            $community->report_id    = $report_id;

            if ($community->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan pelatihan berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan pelatihan gagal ditambahkan');
            }

            return redirect('laporan-pelatihan/komunitas/' . $report_id);
        }

        return view('report.trainning.community_create', compact('report', 'report_id', 'school'));
    }

    public function editCommunity(Request $request, $id)
    {
        $community  = Community::find($id);
        $report  = Report::find($community->report_id);

        $school  = \App\Models\School::where('province_id', $report->province_id)
                    ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            $community                        = Community::find($id);
            $community->school_id             = $request->school_id;
            $community->school_community      = $request->school_community;
            $community->sum_community         = $request->sum_community;

            if ($community->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan pelatihan berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar komunitas sekolah pada laporan pelatihan gagal diubah');
            }

            return redirect('laporan-pelatihan/komunitas/' . $community->report_id);
        }

        return view('report.trainning.community_edit', compact('school', 'community', 'report'));
    }

    public function createInterview(Request $request, $report_id)
    {
        $report = Report::find($report_id);

        if ($request->method() == 'POST') {
            $interviewees               = new Interviewees();
            $interviewees->name         = $request->name;
            $interviewees->agency       = $request->agency;
            $interviewees->position     = $request->position;
            $interviewees->theory       = $request->theory;
            $interviewees->report_id    = $report_id;

            if ($interviewees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data narasumber pada laporan pelatihan berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data narasumber pada laporan pelatihan gagal ditambahkan');
            }

            return redirect('laporan-pelatihan/show/' . $report_id);
        }

        return view('report.trainning.create_interview', compact('report', 'report_id'));
    }

    public function editInterview(Request $request, $id)
    {
        $interviewees               = Interviewees::find($id);
        $report                     = Report::find($interviewees->report_id);

        if ($request->method() == 'POST') {
            $interviewees               = Interviewees::find($id);
            $interviewees->name         = $request->name;
            $interviewees->agency       = $request->agency;
            $interviewees->position     = $request->position;
            $interviewees->theory       = $request->theory;

            if ($interviewees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data narasumber pada laporan pelatihan berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data narasumber pada laporan pelatihan gagal diubah');
            }

            return redirect('laporan-pelatihan/show/' . $interviewees->report_id);
        }

        return view('report.trainning.edit_interview', compact('report', 'interviewees'));
    }

    public function createSummary(Request $request, $report_id)
    {
        $report = Report::find($report_id);

        if ($request->method() == 'POST') {
            $summary               = new Summary();
            $summary->summary      = $request->summary;
            $summary->report_id    = $request->report_id;

            if ($summary->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data summary pada laporan pelatihan berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data summary pada laporan pelatihan gagal ditambahkan');
            }

            return redirect('laporan-pelatihan/show/' . $request->report_id);
        }

        return view('report.trainning.create_summary', compact('report', 'report_id'));
    }

    public function editSummary(Request $request, $id)
    {
        $summary               = Summary::find($id);
        $report                = Report::find($summary->report_id);

        if ($request->method() == 'POST') {
            $summary               = Summary::find($id);
            $summary->summary      = $request->summary;

            if ($summary->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data summary pada laporan pelatihan berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data summary pada laporan pelatihan gagal diubah');
            }

            return redirect('laporan-pelatihan/show/' . $summary->report_id);
        }

        return view('report.trainning.edit_summary', compact('report', 'summary'));
    }

    public function deleteReport(Request $request, $id)
    {
        DB::beginTransaction();

            Interviewees::where('report_id', $id)->delete();
            Summary::where('report_id', $id)->delete();
            Linsek::where('report_id', $id)->delete();
            LinsekFile::where('report_id', $id)->delete();
            Gallery::where('report_id', $id)->delete();
            Document::where('report_id', $id)->delete();
            School::where('report_id', $id)->delete();
            Community::where('report_id', $id)->delete();
            Report::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data laporan pelatihan berhasil dihapus');

        return redirect()->back();
    }

    public function deleteInteview(Request $request, $id)
    {
        Interviewees::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data narasumber pada laporan pelatihan berhasil dihapus');

        return redirect()->back();
    }

    public function deleteSummary(Request $request, $id)
    {
        Summary::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data summary pada laporan pelatihan berhasil dihapus');

        return redirect()->back();
    }

    public function deleteLinsek(Request $request, $id)
    {
        Linsek::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data perserta pada laporan pelatihan berhasil dihapus');

        return redirect()->back();
    }

    public function deleteSchool(Request $request, $id)
    {
        School::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Daftar sekolah pada laporan pelatihan berhasil dihapus');

        return redirect()->back();
    }

    public function deleteCommunity(Request $request, $id)
    {
        Community::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Peserta komunitas sekolah pada laporan pelatihan berhasil dihapus');

        return redirect()->back();
    }

    public function deleteFoto(Request $request, $id)
    {
        $document = Gallery::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/foto-kegiatan/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Foto kegiatan berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Foto kegiatan gagal dihapus');

        return redirect()->back();
    }

    public function deleteDocument(Request $request, $id)
    {
        $document = Document::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/dokumen-bimtek/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dokumen kegiatan berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Dokumen kegiatan gagal dihapus');

        return redirect()->back();
    }

    public function importInterview(Request $request, $reportID)
    {
        Excel::import(new IntervieweesImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data narasumber berhasil diimport');

        return redirect()->back();
    }

    public function importCommunity(Request $request, $reportID)
    {
        Excel::import(new CommunityReportsImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data peserta komunitas sekolah berhasil diimport');

        return redirect()->back();
    }
}
