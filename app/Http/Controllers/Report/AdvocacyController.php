<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Excel;
use App\Imports\IntervieweesImport;
use App\Imports\AttendeesImport;
use App\Models\Report;
use App\Models\Report\Interviewees;
use App\Models\Report\Attendees;
use App\Models\Report\AttendanceFile;
use App\Models\Report\Gallery;
use App\Models\Report\Document;
use App\Models\Report\Summary;
use App\Models\Master\Province;
use App\Models\Setting\Notification;

ini_set('upload_max_filesize', '10M');
ini_set('post_max_size', '10M');

class AdvocacyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->is_type = 1;
    }

    public function index(Request $request)
    {
        $province = Province::orderBy('province', 'ASC');

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id);
        }

        if ($request->has('province')) {
            $province->where('province', 'like', '%' . $request->province . '%');
        }

        $province = $province->get();

        return view('report.advocacy.index', compact('province'));
    }

    public function detail(Request $request, $province_id)
    {
        $province = Province::find($province_id);
        $report   = Report::where('province_id', $province_id)
                        ->where('type', $this->is_type);

        if ($request->has('key') && $request->has('value')) {
            $report->where($request->key, 'like', '%' . $request->value . '%');
        }

        $report = $report->get();

        return view('report.advocacy.detail', compact('report', 'province_id', 'province'));
    }

    public function show(Request $request, $id)
    {
        $report = Report::find($id);

        return view('report.advocacy.show', compact('report'));
    }

    public function foto(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.advocacy.foto', compact('report'));
    }

    public function createFoto(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $report = Report::find($id);

            if (sizeof($report->galleries) < 6) {
                $request->validate([
                    'file' => 'required|image|mimes:jpeg,png,jpg,svg,bmp|max:10240'
                ]);

                $file   = $request->file('file');

                $foto = new Gallery();

                if ($file) {
                    $foto->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/foto-kegiatan', $file->getClientOriginalName());

                $foto->report_id = $report->report_id;

                if ($foto->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Foto kegiatan berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Foto kegiatan gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-advokasi/foto-kegiatan/' . $report->report_id);
        }

        return view('report.advocacy.foto_create', compact('report'));
    }

    public function deleteFoto(Request $request, $id)
    {
        $document = Gallery::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/foto-kegiatan/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Foto kegiatan berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Foto kegiatan gagal dihapus');

        return redirect()->back();
    }

    public function document(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.advocacy.document', compact('report'));
    }

    public function createDocument(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $request->validate([
                'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
            ]);

            $report = Report::find($id);

            if (sizeof($report->documents) < 6) {
                $file   = $request->file('file');

                $document = new Document();

                if ($file) {
                    $document->file = $file->getClientOriginalName();
                }
                    //save file to folder public
                    $file->storeAs('public/document-advokasi', $file->getClientOriginalName());

                $document->report_id = $report->report_id;

                if ($document->save()) {
                    $request->session()->flash('status', '200');
                    $request->session()->flash('msg', 'Dokumen kegiatan berhasil ditambahkan');
                } else {
                    $request->session()->flash('status', 'err');
                    $request->session()->flash('msg', 'Dokumen kegiatan gagal ditambahkan');
                }
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Jumlah foto sudah melebihi maksimal (6 foto setiap dokumen)');
            }

            return redirect('laporan-advokasi/dokumen/' . $report->report_id);
        }

        return view('report.advocacy.document_create', compact('report'));
    }

    public function deleteDocument(Request $request, $id)
    {
        $document = Document::findOrFail($id);

        if ($document) {
            $file_document = \Storage::url('public/document-advokasi/' . $document->file);

            if (file_exists($file_document)) {
                unlink($file_document);
            }

            $document->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dokumen kegiatan berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Dokumen kegiatan gagal dihapus');

        return redirect()->back();
    }

    public function createReport(Request $request, $province_id)
    {
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            DB::beginTransaction();

                $report               = new Report();
                $report->name         = $request->name;
                $report->date         = date('Y-m-d', strtotime(date('Y-m-d', strtotime($request->date))));
                $report->description  = $request->description;
                $report->province_id  = $province_id;
                $report->created_user = Auth::user()->id;
                $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data laporan advokasi berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data laporan advokasi gagal ditambahkan');
            }

                $notification = new Notification();
                $notification->title = "Menambahkan Laporan Advokasi Baru";
                $notification->description = Auth::user()->name . " berhasil menambahkan laporan baru dengan judul : " . $request->name . " ";
                $notification->user_id = Auth::user()->id;
                $notification->save();

            DB::commit();

            return redirect('laporan-advokasi/show/' . $report->report_id);
        }

        return view('report.advocacy.create_report', compact('province', 'province_id'));
    }

    public function editReport(Request $request, $id)
    {
        $report = Report::find($id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $report               = Report::find($id);
            $report->name         = $request->name;
            $report->date         = date('Y-m-d', strtotime($request->date));
            $report->description  = $request->description;
            $report->created_user = Auth::user()->id;
            $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data laporan advokasi berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data laporan advokasi gagal ditambahkan');
            }

            return redirect('laporan-advokasi/detil/' . $report->province_id);
        }

        return view('report.advocacy.edit_report', compact('province', 'report'));
    }

    public function attendance(Request $request, $id)
    {
        $report = Report::find($id);
        return view('report.advocacy.attendance', compact('report'));
    }

    public function createAttendance(Request $request, $report_id)
    {
        $report = Report::find($report_id);

        if ($request->method() == 'POST') {
            $attendees               = new Attendees();
            $attendees->name         = $request->name;
            $attendees->agency       = $request->agency;
            $attendees->report_id    = $report_id;

            if ($attendees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data peserta pada laporan advokasi berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data peserta pada laporan advokasi gagal ditambahkan');
            }

            return redirect('laporan-advokasi/daftar-hadir/' . $report_id);
        }

        return view('report.advocacy.attendance_create', compact('report_id', 'report'));
    }

    public function editAttendance(Request $request, $id)
    {
        $attendees               = Attendees::find($id);
        $report                  = Report::find($attendees->report_id);

        if ($request->method() == 'POST') {
            $attendees               = Attendees::find($id);
            $attendees->name         = $request->name;
            $attendees->agency       = $request->agency;

            if ($attendees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data peserta pada laporan advokasi berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data peserta pada laporan advokasi gagal diubah');
            }

            return redirect('laporan-advokasi/daftar-hadir/' . $attendees->report_id);
        }

        return view('report.advocacy.attendance_edit', compact('attendees', 'report'));
    }

    public function importAttendance(Request $request, $reportID)
    {
        Excel::import(new AttendeesImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data daftar hadir berhasil diimport');

        return redirect()->back();
    }

    public function createAttendanceFile(Request $request, $id)
    {
        $report = Report::find($id);

        if ($request->method() == "POST") {
            $request->validate([
                'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
            ]);

            $report = Report::find($id);

            $file   = $request->file('file');

            $attendance_files = new AttendanceFile();

            if ($file) {
                $attendance_files->file = $file->getClientOriginalName();
            }
                //save file to folder public
                $file->storeAs('public/dokumen-daftar-hadir', $file->getClientOriginalName());

            $attendance_files->report_id = $report->report_id;

            if ($attendance_files->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Dokumen daftar hadir berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Dokumen daftar hadir gagal ditambahkan');
            }

            return redirect('laporan-advokasi/daftar-hadir/' . $report->report_id);
        }

        return view('report.advocacy.attendance_file_create', compact('report'));
    }

    public function createInterview(Request $request, $report_id)
    {
        $report = Report::find($report_id);

        if ($request->method() == 'POST') {
            $interviewees               = new Interviewees();
            $interviewees->name         = $request->name;
            $interviewees->agency       = $request->agency;
            $interviewees->position     = $request->position;
            $interviewees->theory       = $request->theory;
            $interviewees->report_id    = $report_id;

            if ($interviewees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data narasumber pada laporan advokasi berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data narasumber pada laporan advokasi gagal ditambahkan');
            }

            return redirect('laporan-advokasi/show/' . $report_id);
        }

        return view('report.advocacy.create_interview', compact('report_id', 'report'));
    }

    public function importInterview(Request $request, $reportID)
    {
        Excel::import(new IntervieweesImport($reportID), request()->file('file'));

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data narasumber berhasil diimport');

        return redirect()->back();
    }

    public function editInterview(Request $request, $id)
    {
        $interviewees               = Interviewees::find($id);
        $report                     = Report::find($interviewees->report_id);

        if ($request->method() == 'POST') {
            $interviewees               = Interviewees::find($id);
            $interviewees->name         = $request->name;
            $interviewees->agency       = $request->agency;
            $interviewees->position     = $request->position;
            $interviewees->theory       = $request->theory;

            if ($interviewees->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data narasumber pada laporan advokasi berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data narasumber pada laporan advokasi gagal diubah');
            }

            return redirect('laporan-advokasi/show/' . $interviewees->report_id);
        }

        return view('report.advocacy.edit_interview', compact('interviewees', 'report'));
    }

    public function createSummary(Request $request, $report_id)
    {
        $report = Report::find($report_id);

        if ($request->method() == 'POST') {
            $summary               = new Summary();
            $summary->summary      = $request->summary;
            $summary->report_id    = $request->report_id;

            if ($summary->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data summary pada laporan advokasi berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data summary pada laporan advokasi gagal ditambahkan');
            }

            return redirect('laporan-advokasi/show/' . $request->report_id);
        }

        return view('report.advocacy.create_summary', compact('report_id', 'report'));
    }

    public function editSummary(Request $request, $id)
    {
        $summary               = Summary::find($id);
        $report                = Report::find($summary->report_id);

        if ($request->method() == 'POST') {
            $summary               = Summary::find($id);
            $summary->summary      = $request->summary;

            if ($summary->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data summary pada laporan advokasi berhasil diubah');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data summary pada laporan advokasi gagal diubah');
            }

            return redirect('laporan-advokasi/show/' . $summary->report_id);
        }

        return view('report.advocacy.edit_summary', compact('summary', 'report'));
    }

    public function deleteReport(Request $request, $id)
    {
        DB::beginTransaction();

            Interviewees::where('report_id', $id)->delete();
            Summary::where('report_id', $id)->delete();
            Attendees::where('report_id', $id)->delete();
            AttendanceFile::where('report_id', $id)->delete();
            Gallery::where('report_id', $id)->delete();
            Document::where('report_id', $id)->delete();
            Report::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data laporan advokasi berhasil dihapus');

        return redirect()->back();
    }

    public function deleteInteview(Request $request, $id)
    {
        Interviewees::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data narasumber pada laporan advokasi berhasil dihapus');

        return redirect()->back();
    }

    public function deleteSummary(Request $request, $id)
    {
        Summary::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data summary pada laporan advokasi berhasil dihapus');

        return redirect()->back();
    }

    public function deleteAttendance(Request $request, $id)
    {
        Attendees::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data perserta pada laporan advokasi berhasil dihapus');

        return redirect()->back();
    }

    public function deleteAttendanceFile(Request $request, $id)
    {
        $file = AttendanceFile::findOrFail($id);

        if ($file) {
            $filePath = \Storage::url('public/dokumen-daftar-hadir/' . $file->file);

            if (file_exists($filePath)) {
                unlink($filePath);
            }

            $file->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dokumen daftar hadir berhasil dihapus');

            return redirect()->back();
        }


        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Dokumen daftar hadir gagal dihapus');

        return redirect()->back();
    }
}
