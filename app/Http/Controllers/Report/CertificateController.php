<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Report;
use App\Models\School;
use App\Models\Report\Interviewees;
use App\Models\Report\Summary;
use App\Models\Report\Certificate;
use App\Models\Master\Province;
use App\Models\Setting\Notification;

class CertificateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->is_type = 4;
    }

    public function index(Request $request)
    {
        // $province = Province::orderBy('province','ASC');

        // if(Auth::user()->level_id == 2) {
        //     $province   = Province::orderBy('province','ASC')->where('province_id', Auth::user()->province_id);
        // }

        // if($request->has('province')) {
        //     $province->where('province','like','%'. $request->province .'%');
        // }

        // $province = $province->get();

        $school = Province::select(
            DB::raw('COUNT(school_id) as sum_school'),
            'year_pbkp_ks',
            'provinces.province as name_province',
            'provinces.province_id as id_province'
        )
                    ->join(
                        'cities',
                        'cities.province_id',
                        'provinces.province_id'
                    )
                    ->join(
                        'schools',
                        'schools.city_id',
                        'cities.city_id'
                    )
                    ->where('pbkp_ks', 1)
                    ->whereRaw('year_pbkp_ks != ""')
                    ->groupBy('cities.province_id', 'year_pbkp_ks');

        if (Auth::user()->level_id == 2) {
            $school->where('cities.province_id', Auth::user()->province_id);
        }

        if ($request->has('province')) {
            $school->where('province', 'like', '%' . $request->province . '%');
        }

        $school = $school->get();

        return view('report.certificate.index', compact('school'));
    }

    public function detail(Request $request, $province_id)
    {
        $report   = Report::where('province_id', $province_id)
                        ->where('type', $this->is_type)->get();

        return view('report.certificate.detail', compact('report', 'province_id'));
    }

    public function show(Request $request, $id)
    {
        $report = Report::find($id);

        return view('report.certificate.show', compact('report'));
    }

    public function createReport(Request $request, $province_id)
    {
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            DB::beginTransaction();

                $report               = new Report();
                $report->name         = $request->name;
                $report->date         = date('Y-m-d', strtotime($request->date));
                $report->description  = $request->description;
                $report->province_id  = $province_id;
                $report->created_user = Auth::user()->id;
                $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data sertifikat pbkp-ks berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data sertifikat pbkp-ks gagal ditambahkan');
            }

                $notification = new Notification();
                $notification->title = "Menambahkan Laporan Sertifikat PBKP-KS Baru";
                $notification->description = Auth::user()->name . " berhasil menambahkan laporan sertifikat pbkp-ks baru dengan judul : " . $request->name . " ";
                $notification->user_id = Auth::user()->id;
                $notification->save();

            DB::commit();


            return redirect('laporan-sertifikat/show/' . $report->report_id);
        }

        return view('report.certificate.create_report', compact('province', 'province_id'));
    }

    public function editReport(Request $request, $id)
    {
        $report = Report::find($id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $report               = Report::find($id);
            $report->name         = $request->name;
            $report->date         = date('Y-m-d', strtotime($request->date));
            $report->description  = $request->description;
            $report->created_user = Auth::user()->id;
            $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data sertifikat pbkp-ks berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data sertifikat pbkp-ks gagal ditambahkan');
            }

            return redirect('laporan-sertifikat/detil/' . $report->province_id);
        }

        return view('report.certificate.edit_report', compact('province', 'report'));
    }

    public function school(Request $request, $id)
    {
        $report = Report::find($id);
        $school_id = Certificate::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school = \App\Models\School::whereIn('school_id', $school_id)->paginate(25);

        return view('report.certificate.school', compact('report', 'school'));
    }

    public function createSchool(Request $request, $report_id)
    {
        $report   = Report::find($report_id);
        $school_id = Certificate::where('report_id', $report->report_id)->pluck('school_id')->toArray();
        $school   = \App\Models\School::where('province_id', $report->province_id)
                        ->whereNotIn('school_id', $school_id)
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            try {
                DB::beginTransaction();
                    $certificate               = new Certificate();
                    $certificate->school_id    = $request->school_id;
                    $certificate->report_id    = $report_id;
                    $certificate->save();

                    $school = \App\Models\School::find($request->school_id);
                    $school->pbkp_ks = 1;
                    $school->save();
                DB::commit();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada data sertifikat pbkp-ks berhasil ditambahkan');
            } catch (\Exception $e) {
                DB::rollback();

                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', $e->getMessage());
            }

            return redirect('laporan-sertifikat/daftar-sekolah/' . $report_id);
        }

        return view('report.certificate.school_create', compact('report', 'report_id', 'school'));
    }

    public function editSchool(Request $request, $id)
    {
        $report_school  = Certificate::find($id);
        $report  = Report::find($report_school->report_id);

        $school  = \App\Models\School::where('province_id', $report->province_id)
                    ->join('cities', 'schools.city_id', '=', 'cities.city_id')->get();

        if ($request->method() == 'POST') {
            try {
                DB::beginTransaction();
                    $certificate               = Certificate::find($id);
                    $certificate->school_id    = $request->school_id;
                    $certificate->save();
                DB::commit();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Daftar sekolah pada data sertifikat pbkp-ks berhasil diubah');
            } catch (\Exception $e) {
                DB::rollback();

                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Daftar sekolah pada data sertifikat pbkp-ks gagal diubah');
            }

            return redirect('laporan-sertifikat/daftar-sekolah/' . $report->report_id);
        }

        return view('report.certificate.school_edit', compact('school', 'report_school'));
    }

    public function deleteReport(Request $request, $id)
    {
        DB::beginTransaction();

            Certificate::where('report_id', $id)->delete();
            Report::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data sertifikat pbkp-ks berhasil dihapus');

        return redirect()->back();
    }

    public function deleteSchool(Request $request, $id)
    {
        Certificate::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Daftar sekolah pada data sertifikat pbkp-ks berhasil dihapus');

        return redirect()->back();
    }
}
