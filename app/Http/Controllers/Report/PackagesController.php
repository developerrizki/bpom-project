<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Report;
use App\Models\Report\Package;
use App\Models\Master\Province;
use App\Models\Setting\Notification;

class PackagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->is_type = 7;
    }

    public function index(Request $request)
    {
        $province = Province::orderBy('province', 'ASC');

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id);
        }

        if ($request->has('province')) {
            $province->where('province', 'like', '%' . $request->province . '%');
        }

        $province = $province->get();

        return view('report.package.index', compact('province'));
    }

    public function detail(Request $request, $province_id)
    {
        $report   = Report::where('province_id', $province_id)
                        ->where('type', $this->is_type)->get();

        return view('report.package.detail', compact('report', 'province_id'));
    }

    public function show(Request $request, $id)
    {
        $report = Report::find($id);

        return view('report.package.show', compact('report'));
    }

    public function createReport(Request $request, $province_id)
    {
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            DB::beginTransaction();

                $report               = new Report();
                $report->name         = $request->name;
                $report->date         = date('Y-m-d', strtotime($request->date));
                $report->description  = $request->description;
                $report->province_id  = $province_id;
                $report->created_user = Auth::user()->id;
                $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Laporan paket edukasi berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Laporan paket edukasi gagal ditambahkan');
            }

                $notification = new Notification();
                $notification->title = "Menambahkan Laporan Paket Edukasi Baru";
                $notification->description = Auth::user()->name . " berhasil menambahkan laporan paket edukasi baru dengan judul : " . $request->name . " ";
                $notification->user_id = Auth::user()->id;
                $notification->save();

            DB::commit();

            return redirect('laporan-paket/daftar-paket/' . $report->report_id);
        }

        return view('report.package.create_report', compact('province', 'province_id'));
    }

    public function editReport(Request $request, $id)
    {
        $report = Report::find($id);
        $province = Province::orderBy('province', 'ASC')->get();

        if ($request->method() == 'POST') {
            $report               = Report::find($id);
            $report->name         = $request->name;
            $report->date         = date('Y-m-d', strtotime($request->date));
            $report->description  = $request->description;
            $report->created_user = Auth::user()->id;
            $report->type         = $this->is_type;

            if ($report->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Laporan paket edukasi berhasil ditambahkan');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Laporan paket edukasi gagal ditambahkan');
            }

            return redirect('laporan-paket/detil/' . $report->province_id);
        }

        return view('report.package.edit_report', compact('province', 'report'));
    }

    public function package(Request $request, $id)
    {
        $report = Report::find($id);

        return view('report.package.package', compact('report'));
    }

    public function createPackage(Request $request, $report_id)
    {
        $report   = Report::find($report_id);

        if ($request->method() == 'POST') {
            try {
                DB::beginTransaction();
                    $package               = new Package();
                    $package->report_id    = $report_id;
                    $package->name         = Auth::user()->name;
                    $package->address      = $request->address;
                    $package->province_id  = $report->province_id;
                    $package->school       = $request->school;
                    $package->periode      = date('Y-m-d', strtotime($request->periode));
                    $package->package      = $request->package;
                    $package->package_type = $request->package_type;
                    $package->save();
                DB::commit();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data paket edukasi berhasil ditambahkan');
            } catch (\Exception $e) {
                DB::rollback();

                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', $e->getMessage());
            }

            return redirect('laporan-paket/daftar-paket/' . $report_id);
        }

        return view('report.package.package_create', compact('report', 'report_id'));
    }

    public function editPackage(Request $request, $id)
    {
        $report_package  = Package::find($id);
        $report  = Report::find($report_package->report_id);

        if ($request->method() == 'POST') {
            try {
                DB::beginTransaction();
                    $package               = Package::find($id);
                    $package->name         = Auth::user()->name;
                    $package->address      = $request->address;
                    $package->province_id  = $report->province_id;
                    $package->school       = $request->school;
                    $package->periode      = date('Y-m-d', strtotime($request->periode));
                    $package->package      = $request->package;
                    $package->package_type = $request->package_type;
                    $package->save();
                DB::commit();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Data paket edukasi berhasil diubah');
            } catch (\Exception $e) {
                DB::rollback();

                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Data paket edukasi gagal diubah');
            }

            return redirect('laporan-paket/daftar-paket/' . $report->report_id);
        }

        return view('report.package.package_edit', compact('report', 'report_package'));
    }

    public function deleteReport(Request $request, $id)
    {
        DB::beginTransaction();

            Package::where('report_id', $id)->delete();
            Report::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data paket edukasi berhasil dihapus');

        return redirect()->back();
    }

    public function deletePackage(Request $request, $id)
    {
        Package::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data paket edukasi berhasil dihapus');

        return redirect()->back();
    }
}
