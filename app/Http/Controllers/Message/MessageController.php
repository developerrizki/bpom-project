<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Message\Message;
use App\Models\Message\Detail;
use App\User;
use DB;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $message = Message::where('sender_id', \Auth::user()->id)
                    ->orWhere('receipt_id', \Auth::user()->id)
                    ->orderBy('created_at', 'DESC')->get();

        return view('message.index', compact('message'));
    }

    public function create()
    {
        $receipt = User::orderBy('name', 'ASC');

        if (\Auth::user()->level_id == 1) {
            $receipt->where('level_id', 2);
        }

        if (\Auth::user()->level_id == 2) {
            $receipt->where('level_id', 1);
        }

        $receipt = $receipt->get();

        return view('message.create', compact('receipt'));
    }

    public function store(Request $request)
    {
        $message                = new Message();
        $message->sender_id     = \Auth::user()->id;
        $message->receipt_id    = $request->receipt_id;
        $message->subject       = $request->subject;

        if ($message->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Berhasil membuat pesan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Gagal membuat pesan');
        }

        return redirect('pesan/detil/' . $message->id);
    }

    public function show(Request $request, $id)
    {
        $message = Message::where('sender_id', \Auth::user()->id)
                    ->orWhere('receipt_id', \Auth::user()->id)
                    ->orderBy('created_at', 'DESC')->get();

        $info    = Message::find($id);

        $detail  = Detail::where('message_id', $id)->get();

        Message::where('id', $id)->where('receipt_id', \Auth::user()->id)->update(['is_read' => 1]);

        return view('message.show', compact('message', 'detail', 'info'));
    }

    public function sendMessage(Request $request, $id)
    {
        $detail                = new Detail();
        $detail->user_id       = \Auth::user()->id;
        $detail->message       = $request->message;
        $detail->message_id    = $id;

        if ($detail->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Berhasil mengirim pesan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Gagal mengirim pesan');
        }

        return redirect('pesan/detil/' . $id);
    }
}
