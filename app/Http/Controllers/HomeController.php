<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Master\Province;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $survey = \App\Models\Survey::where('is_online', 1)
                    ->orderBy('created_at', 'DESC')->limit(5)->get();

        $theory = \App\Models\Document::where('is_other', 0)
                    ->orderBy('created_at', 'DESC')->limit(5)->get();

        $other = \App\Models\Document::where('is_other', 1)
            ->orderBy('created_at', 'DESC')->limit(5)->get();

        $province_count = \App\Models\Master\Province::count();

        $school_count = \App\Models\Master\ProvinceTarget::select(\DB::raw('SUM(target) as total'))
                        ->where('target_year', date('Y'))->first();

        $school_of_intervention = \App\Models\School::whereRaw('year_pbkp_ks != ""')->where('pbkp_ks', 1);

        if ($request->has('start_year') && $request->has('end_year')) {
            $school_of_intervention->whereBetween('year_pbkp_ks', [
                $request->start_year,
                $request->end_year
            ]);
        }

        $school_of_intervention = $school_of_intervention->count();

        $canteen = \App\Models\Report\Audit::count();
        $certificate = \App\Models\Report\Certificate::count();

        $list_school = \App\Models\Report\School::join('schools', 'report_schools.school_id', '=', 'schools.school_id')
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id')->limit(20)->get();
        $list_canteen = \App\Models\Report\Audit::join('schools', 'report_audits.school_id', '=', 'schools.school_id')
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id')->limit(20)->get();
        $list_certificate = \App\Models\Report\Certificate::join('schools', 'report_certificates.school_id', '=', 'schools.school_id')
                        ->join('cities', 'schools.city_id', '=', 'cities.city_id')->limit(20)->get();

        $tot = \App\Models\Report::where('type', 0)->count();
        $advocacy = \App\Models\Report::where('type', 1)->count();
        $technical = \App\Models\Report::where('type', 2)->count();
        $trainning = \App\Models\Report::where('type', 3)->count();
        $audit = \App\Models\Report::where('type', 5)->count();
        $sampling = \App\Models\Report::where('type', 6)->count();
        $package = \App\Models\Report::where('type', 7)->count();
        $monev = \App\Models\Report::where('type', 8)->count();

        if (Auth::user()->level_id == 2) {
            $school_of_intervention = \App\Models\Report\School::join('reports', 'report_schools.report_id', '=', 'reports.report_id')
                        ->where('province_id', Auth::user()->province_id)->count();
            $canteen = \App\Models\Report\Audit::join('reports', 'report_audits.report_id', '=', 'reports.report_id')
                        ->where('province_id', Auth::user()->province_id)->count();
            $certificate = \App\Models\Report\Certificate::join('reports', 'report_certificates.report_id', '=', 'reports.report_id')
                        ->where('province_id', Auth::user()->province_id)->count();
            $school_count = \App\Models\Master\ProvinceTarget::select(\DB::raw('SUM(target) as total'))
                            ->where('target_year', date('Y'))->where('province_id', Auth::user()->province_id)->first();

            $tot = \App\Models\Report::where('type', 0)->where('province_id', Auth::user()->province_id)->count();
            $advocacy = \App\Models\Report::where('type', 1)->where('province_id', Auth::user()->province_id)->count();
            $technical = \App\Models\Report::where('type', 2)->where('province_id', Auth::user()->province_id)->count();
            $trainning = \App\Models\Report::where('type', 3)->where('province_id', Auth::user()->province_id)->count();
            $audit = \App\Models\Report::where('type', 5)->where('province_id', Auth::user()->province_id)->count();
            $sampling = \App\Models\Report::where('type', 6)->where('province_id', Auth::user()->province_id)->count();
            $package = \App\Models\Report::where('type', 7)->where('province_id', Auth::user()->province_id)->count();
            $monev = \App\Models\Report::where('type', 8)->where('province_id', Auth::user()->province_id)->count();
        }

        $data   = [
            'school_of_intervention' => $school_of_intervention,
            'canteen' => $canteen,
            'certificate' => $certificate,
            'list_school' => $list_school,
            'list_canteen' => $list_canteen,
            'list_certificate' => $list_certificate,
        ];

        $school_count_total = $school_count->total == 0 ? 1 : $school_count->total;

        $progress = [
            'tot' => $tot < 20 ? 0 : ($tot / $school_count_total) * 10,
            'advocacy' => $advocacy < 20 ? 0 : ($advocacy / $school_count_total) * 20,
            'technical' => $technical < 20 ? 0 : ($technical / $school_count_total) * 10,
            'trainning' => $trainning < 20 ? 0 : ($trainning / $school_count_total) * 10,
            'audit' => $audit < 20 ? 0 : ($audit / $school_count_total) * 10,
            'sampling' => $sampling < 20 ? 0 : ($sampling / $school_count_total) * 10,
            'package' => $package < 20 ? 0 : ($package / $school_count_total) * 10,
            'monev' => $monev < 20 ? 0 : ($monev / $school_count_total) * 20,
        ];

        $province = json_encode($this->json());

        return view('home', compact('survey', 'theory', 'other', 'province_count', 'school_count', 'data', 'progress', 'province'));
    }

    public function json()
    {
        if (Auth::user()->level_id == 2) {
            $provinces = Province::where('province_id', Auth::user()->province_id)->get();
        } else {
            $provinces = Province::all();
        }

        $json = [];

        if ($provinces) {
            foreach ($provinces as $province) {
                $json[] = [
                    'name' => $province->province,
                    'location' => [$province->latitude, $province->longitude]
                ];
            }
        }

        return $json;
    }
}
