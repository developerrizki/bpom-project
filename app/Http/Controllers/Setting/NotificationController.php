<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting\Notification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $notification = Notification::orderBy('created_at', 'DESC')->whereNotIn('user_id', [1])->paginate(20);

        if (\Auth::user()->level_id != 1) {
            $notification = Notification::orderBy('created_at', 'DESC')->whereIn('user_id', [1])->paginate(20);
            Notification::whereIn('user_id', [1])->update(['is_read' => 1]);
        } else {
            Notification::whereNotIn('user_id', [1])->update(['is_read' => 1]);
        }

        return view('setting.notification.index', compact('notification'));
    }

    public function delete(Request $request, $id)
    {
        Notification::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data notifikasi pengguna berhasil dihapus');

        return redirect()->back();
    }
}
