<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Master\City;
use App\Models\Master\Province;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 20;
    }

    public function index(Request $request)
    {
        if (Auth::user()->level_id != 1) {
            abort(403);
        }

        $city     = City::join('provinces', 'cities.province_id', '=', 'provinces.province_id')
                        ->orderBy('cities.province_id', 'ASC')->paginate($this->limit);

        if (isset($request->key) && isset($request->value)) {
            $city     = City::where($request->key, 'like', '%' . $request->value . '%')
                                ->join('provinces', 'cities.province_id', '=', 'provinces.province_id')
                                ->orderBy('cities.province_id', 'ASC')->paginate($this->limit);
        }

        $city->appends($request->all());

        return view('master.city.index', compact('city'));
    }

    public function create(Request $request)
    {
        $province     = Province::all();

        return view('master.city.create', compact('province'));
    }

    public function store(Request $request)
    {
        $city                 = new City();
        $city->city           = $request->city;
        $city->province_id    = $request->province_id;

        if ($city->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data kota berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data kota gagal ditambahkan');
        }

        return redirect('kota');
    }

    public function edit(Request $request, $id)
    {
        $province     = Province::all();
        $city         = City::find($id);

        return view('master.city.edit', compact('city', 'province'));
    }

    public function update(Request $request, $id)
    {
        $city                 = City::find($id);
        $city->city           = $request->city;
        $city->province_id    = $request->province_id;

        if ($city->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data kota berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data kota gagal diubah');
        }

        return redirect('kota');
    }

    public function delete(Request $request, $id)
    {
        City::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data kota berhasil dihapus');

        return redirect()->back();
    }
}
