<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Master\City;
use App\Models\Master\Province;
use App\Models\Master\ProvinceTarget as Target;

class ProvinceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (Auth::user()->level_id != 1) {
            abort(403);
        }

        $province     = Province::paginate(20);

        if (isset($request->key) && isset($request->value)) {
            $province     = Province::where($request->key, 'like', '%' . $request->value . '%')->paginate(20);
        }

        $province->appends($request->all());

        return view('master.province.index', compact('province'));
    }

    public function create(Request $request)
    {
        return view('master.province.create');
    }

    public function store(Request $request)
    {
        $province                 = new Province();
        $province->province       = $request->province;
        $province->latitude       = $request->latitude;
        $province->longitude      = $request->longitude;

        if ($province->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data provinsi berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data provinsi gagal ditambahkan');
        }

        return redirect('provinsi');
    }

    public function edit(Request $request, $id)
    {
        $province     = Province::find($id);

        return view('master.province.edit', compact('province'));
    }

    public function update(Request $request, $id)
    {
        $province                 = Province::find($id);
        $province->province       = $request->province;
        $province->latitude       = $request->latitude;
        $province->longitude      = $request->longitude;

        if ($province->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data provinsi berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data provinsi gagal diubah');
        }

        return redirect('provinsi');
    }

    public function delete(Request $request, $id)
    {
        try {
            DB::beginTransaction();
                $availableCity = City::where('province_id', $id)->count();

            if ($availableCity > 0) {
                City::where('province_id', $id)->delete();
            }

                Province::findOrFail($id)->delete();
            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data provinsi berhasil dihapus');
        } catch (\Exception $e) {
            DB::rollback();

            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', $e->getMessage());
        }

        return redirect()->back();
    }

    public function target(Request $request)
    {
        $province_id = $request->province_id;
        $province = Province::find($province_id);
        $target = Target::where('province_id', $province_id)->orderBy('target_year', 'DESC')->paginate(20);
        return view('master.province.target', compact('target', 'province_id', 'province'));
    }

    public function targetCreate(Request $request)
    {
        //store / edit
        if ($request->method() == "POST") {
            $target = new Target();
            $target->province_id    = $request->province_id;
            $target->target_year    = $request->target_year;
            $target->target         = $request->target;
            $target->target_canteen = $request->target_canteen;
            $target->target_pbkp_ks = $request->target_pbkp_ks;
            $target->save();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data target sekolah provinsi ini berhasil disimpan');

            return redirect('provinsi/target?province_id=' . $request->province_id);
        }

        $province_id = $request->province_id;

        return view('master.province.target_create', compact('province_id'));
    }

    public function targetEdit(Request $request, $id)
    {
        //store / edit
        if ($request->method() == "POST") {
            $target = new Target();
            $target->province_id    = $request->province_id;
            $target->target_year    = $request->target_year;
            $target->target         = $request->target;
            $target->target_canteen = $request->target_canteen;
            $target->target_pbkp_ks = $request->target_pbkp_ks;
            $target->save();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data target sekolah provinsi ini berhasil diubah');

            return redirect('provinsi/target?province_id=' . $request->province_id);
        }

        $province_id = $request->province_id;
        $target      = Target::find($id);

        return view('master.province.target_edit', compact('province_id', 'target'));
    }

    public function targetDelete(Request $request, $id)
    {
        Target::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data target provinsi berhasil dihapus');

        return redirect()->back();
    }
}
