<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Models\System\Level;
use App\Models\Master\Province;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (Auth::user()->level_id != 1) {
            abort(403);
        }

        $user     = User::orderBy('created_at', 'DESC')->whereNotIn('level_id', [1])->paginate(20);

        if (isset($request->key) && isset($request->value)) {
            $user = User::where($request->key, 'like', '%' . $request->value . '%')->orderBy('created_at', 'DESC')
                        ->whereNotIn('level_id', [1])->paginate(20);
        }

        return view('user.index', compact('user'));
    }

    public function create(Request $request)
    {
        $level    = Level::whereNotIn('level_id', [1])->orderBy('level', 'ASC')->get();
        $province = Province::all();

        return view('user.create', compact('level', 'province'));
    }

    public function store(Request $request)
    {
        $user               = new User();
        $user->name         = $request->name;
        $user->username     = $request->username;
        $user->email        = $request->email;
        $user->level_id     = $request->level_id;
        $user->password     = bcrypt($request->password);

        if (!empty($request->province_id)) {
            $user->province_id  = $request->province_id;
        }

        if ($user->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data pengguna berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data pengguna gagal ditambahkan');
        }

        return redirect('pengguna');
    }

    public function edit(Request $request, $id)
    {
        $user     = User::find($id);
        $level    = Level::whereNotIn('level_id', [1])->orderBy('level', 'ASC')->get();
        $province = Province::all();

        return view('user.edit', compact('user', 'level', 'province'));
    }

    public function update(Request $request, $id)
    {
        $user               = User::find($id);
        $user->name         = $request->name;
        $user->username     = $request->username;
        $user->email        = $request->email;
        $user->level_id     = $request->level_id;

        if (!empty($request->password) || $request->password != null) {
            $user->password  = bcrypt($request->password);
        }

        if (!empty($request->province_id)) {
            $user->province_id  = $request->province_id;
        }

        if ($user->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data pengguna berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data pengguna gagal diubah');
        }

        return redirect('pengguna');
    }

    public function delete(Request $request, $id)
    {
        User::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data pengguna berhasil dihapus');

        return redirect()->back();
    }
}
