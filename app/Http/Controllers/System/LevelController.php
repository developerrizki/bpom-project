<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\System\Level;

class LevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (Auth::user()->level_id != 1) {
            abort(403);
        }

        $level     = Level::orderBy('created_at', 'ASC')->whereNotIn('level_id', [1])->paginate(20);

        if (isset($request->key) && isset($request->value)) {
            $level = Level::where($request->key, 'like', '%' . $request->value . '%')->orderBy('created_at', 'ASC')
                        ->whereNotIn('level_id', [1])->paginate(20);
        }

        return view('user.level.index', compact('level'));
    }

    public function create(Request $request)
    {
        return view('user.level.create');
    }

    public function store(Request $request)
    {
        $level                 = new Level();
        $level->level          = $request->level;

        if ($level->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data level pengguna berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data level pengguna gagal ditambahkan');
        }

        return redirect('level');
    }

    public function edit(Request $request, $id)
    {
        $level     = Level::find($id);

        return view('user.level.edit', compact('level'));
    }

    public function update(Request $request, $id)
    {
        $level                 = Level::find($id);
        $level->level          = $request->level;

        if ($level->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data level pengguna berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data level pengguna gagal diubah');
        }

        return redirect('level');
    }

    public function delete(Request $request, $id)
    {
        $level = Level::findOrFail($id);

        if ($level && $level->users->count() > 0) {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data level tidak dapat dihapus. karena sudah terpakai pada data user');
        } else {
            $level->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data level pengguna berhasil dihapus');
        }

        return redirect()->back();
    }
}
