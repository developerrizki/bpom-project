<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('user.account');
    }

    public function update(Request $request, $id)
    {
        $user               = User::find($id);
        $user->name         = $request->name;
        $user->username     = $request->username;
        $user->email        = $request->email;

        if (!empty($request->password)) {
            $user->password     = bcrypt($request->password);
        }

        if ($user->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data akun Anda berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data akun Anda gagal diubah');
        }

        return redirect('akun');
    }
}
