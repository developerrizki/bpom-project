<?php

namespace App\Http\Controllers\Summary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Summary;
use App\Models\Master\Province;

class SummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->summary = new Summary();
    }

    public function index(Request $request)
    {
        $province   = Province::orderBy('province', 'ASC')->get();

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id)->get();
        }

        return view('summary.index', compact('province'));
    }

    public function create(Request $request)
    {
        $province_id = Summary::pluck('province_id')->toArray();

        $province   = Province::orderBy('province', 'ASC')->whereNotIn('province_id', $province_id)->get();

        if (Auth::user()->level_id == 2) {
            $province   = Province::orderBy('province', 'ASC')->where('province_id', Auth::user()->province_id)->whereNotIn('province_id', $province_id)->get();
        }

        return view('summary.create', compact('province'));
    }

    public function store(Request $request)
    {
        $summary                 = new Summary();
        $summary->summary        = $request->summary;
        $summary->province_id    = $request->province_id;
        $summary->created_user   = Auth::user()->id;

        if ($summary->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data ringkasan eksekutif berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data ringkasan eksekutif gagal ditambahkan');
        }

        return redirect('ringkasan-eksekutif/detil/' . $request->province_id);
    }

    public function edit(Request $request, $id)
    {
        $summary    = Summary::find($id);
        $province   = Province::orderBy('province', 'ASC')->get();

        return view('summary.edit', compact('summary', 'province'));
    }

    public function update(Request $request, $id)
    {
        $summary                 = Summary::find($id);
        $summary->summary        = $request->summary;
        $summary->province_id    = $request->province_id;
        $summary->updated_user   = Auth::user()->id;

        if ($summary->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data ringkasan eksekutif berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data ringkasan eksekutif gagal diubah');
        }

        return redirect('ringkasan-eksekutif/detil/' . $request->province);
    }

    public function show(Request $request, $province_id)
    {
        $summary     = Summary::where('province_id', $province_id)->first();

        $school_of_intervention = \App\Models\Report\School::join('reports', 'report_schools.report_id', '=', 'reports.report_id')
                    ->where('province_id', $province_id)->count();
        $canteen = \App\Models\Report\Audit::join('reports', 'report_audits.report_id', '=', 'reports.report_id')
                    ->where('province_id', $province_id)->count();
        $certificate = \App\Models\Report\Certificate::join('reports', 'report_certificates.report_id', '=', 'reports.report_id')
                    ->where('province_id', $province_id)->count();
        $school_count = \App\Models\Master\ProvinceTarget::select(\DB::raw('SUM(target) as total_school, SUM(target_canteen) as total_canteen, SUM(target_pbkp_ks) as total_pbkp_ks'))
                        ->where('target_year', date('Y'))->where('province_id', $province_id)->first();

        if (isset($request->year)) {
            $school_of_intervention = \App\Models\Report\School::join('reports', 'report_schools.report_id', '=', 'reports.report_id')
                        ->whereYear('report_schools.created_at', $request->year)
                        ->where('province_id', $province_id)->count();
            $canteen = \App\Models\Report\Audit::join('reports', 'report_audits.report_id', '=', 'reports.report_id')
                        ->whereYear('report_audits.created_at', $request->year)
                        ->where('province_id', $province_id)->count();
            $certificate = \App\Models\Report\Certificate::join('reports', 'report_certificates.report_id', '=', 'reports.report_id')
                        ->whereYear('report_certificates.created_at', $request->year)
                        ->where('province_id', $province_id)->count();
            $school_count = \App\Models\Master\ProvinceTarget::select(\DB::raw('SUM(target) as total_school, SUM(target_canteen) as total_canteen, SUM(target_pbkp_ks) as total_pbkp_ks'))
                        ->where('target_year', $request->year)->where('province_id', $province_id)->first();
        }

        $data   = [
            'school_of_intervention' => $school_of_intervention,
            'canteen' => $canteen,
            'certificate' => $certificate,
            'total_school' => (int) $school_count->total_school == "" ? 0 : $school_count->total_school,
            'total_canteen' => (int) $school_count->total_canteen == "" ? 0 : $school_count->total_canteen,
            'total_pbkp_ks' => (int) $school_count->total_pbkp_ks == "" ? 0 : $school_count->total_pbkp_ks,
        ];

        return view('summary.show', compact('summary', 'data'));
    }

    public function delete(Request $request, $id)
    {
        School::findOrFail($id)->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data ringkasan eksekutif berhasil dihapus');

        return redirect()->back();
    }
}
