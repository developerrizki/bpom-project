<?php

namespace App\Http\Controllers\Document;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Document;
use App\Models\Setting\Notification;

class OtherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->document = new Document();
        $this->is_other = 1;
    }

    public function index(Request $request)
    {
        $document = $this->document->dataPaginate($request, $this->is_other);

        return view('document.other.index', compact('document'));
    }

    public function create(Request $request)
    {
        return view('document.other.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
        ]);

        $file     = $request->file('file');

        DB::beginTransaction();

            $document = new Document();

        if ($file) {
            $document->file = $file->getClientOriginalName();
        }

                //save file to folder public
                $file->storeAs('public/dokumen-lainnya', $file->getClientOriginalName());

            $document->created_user = Auth::user()->id;
            $document->is_other     = 1;

        if ($document->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data informasi lainnya berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data informasi lainnya gagal ditambahkan');
        }

            $notification = new Notification();
            $notification->title = "Menambahkan Informasi Baru";
            $notification->description = "Super Admin BPOM berhasil menambahkan informasi baru dengan judul : " . $file->getClientOriginalName() . " ";
            $notification->user_id = Auth::user()->id;
            $notification->save();

        DB::commit();

        return redirect('dokumen-lainnya');
    }

    public function delete(Request $request, $id)
    {
        $document = Document::findOrFail($id);

        //delete files
        if (isset($document->file)) {
            Storage::delete('dokumen-lainnya/' . $document->file);
        }

        $document->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data dokumen berhasil dihapus');

        return redirect()->back();
    }
}
