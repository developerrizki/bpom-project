<?php

namespace App\Http\Controllers\Survey;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Survey;
use App\Models\Setting\Notification;

class OnlineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->survey     = new Survey();
        $this->is_online  = 1;
    }

    public function index(Request $request)
    {
        $survey = $this->survey->dataPaginate($request, $this->is_online);

        return view('survey.online.index', compact('survey'));
    }

    public function create(Request $request)
    {
        return view('survey.online.create');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

            $survey                 = new Survey();
            $survey->file           = $request->file;
            $survey->link           = $request->link;
            $survey->is_online      = $this->is_online;
            $survey->created_user   = Auth::user()->id;

        if ($survey->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Survey online berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Survey online gagal ditambahkan');
        }

            $notification = new Notification();
            $notification->title = "Menambahkan Survey Online Baru";
            $notification->description = "Super Admin BPOM berhasil menambahkan survey online baru dengan judul : " . $request->file . " ";
            $notification->user_id = Auth::user()->id;
            $notification->save();

        DB::commit();

        return redirect('survey-online');
    }

    public function delete(Request $request, $id)
    {
        $survey = Survey::findOrFail($id);

        $survey->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data survey berhasil dihapus');

        return redirect()->back();
    }
}
