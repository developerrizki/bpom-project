<?php

namespace App\Http\Controllers\Survey;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Survey;
use App\Models\Setting\Notification;

class OfflineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->survey     = new Survey();
        $this->is_online  = 0;
    }

    public function index(Request $request)
    {
        $survey = $this->survey->dataPaginate($request, $this->is_online);

        return view('survey.offline.index', compact('survey'));
    }

    public function create(Request $request)
    {
        return view('survey.offline.create');
    }

    public function store(Request $request)
    {
        $file     = $request->file('file');

        $request->validate([
            'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10240'
        ]);

        DB::beginTransaction();

            $survey = new Survey();

        if ($file) {
            $survey->file = $file->getClientOriginalName();
        }
                //save file to folder public
                $file->storeAs('public/survey-kuesioner', $file->getClientOriginalName());

            $survey->is_online      = $this->is_online;
            $survey->created_user = Auth::user()->id;

        if ($survey->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Survey kuesioner berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Survey kuesioner gagal ditambahkan');
        }

            $notification = new Notification();
            $notification->title = "Menambahkan Kuisioner Baru";
            $notification->description = "Super Admin BPOM berhasil menambahkan kuisioner baru dengan judul : " . $file->getClientOriginalName() . " ";
            $notification->user_id = Auth::user()->id;
            $notification->save();

        DB::commit();

        return redirect('survey-kuesioner');
    }

    public function delete(Request $request, $id)
    {
        $survey = Survey::findOrFail($id);

        //delete files
        if (isset($survey->file)) {
            Storage::delete('survey-kuesioner/' . $survey->file);
        }

        $survey->delete();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Data survey berhasil dihapus');

        return redirect()->back();
    }
}
