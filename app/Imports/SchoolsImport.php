<?php

namespace App\Imports;

use App\Models\School;
use App\Models\Master\City;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Support\Facades\Auth;

class SchoolsImport implements ToModel, WithStartRow, WithMultipleSheets
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!empty($row[0])) {
            if (!empty($id)) {
                $cityID = City::where('province_id', $id)->pluck('city_id')->toArray();

                if (in_array($row[1], $cityID)) {
                    return new School([
                        'name' => $row[0],
                        'city_id' => $row[1],
                        'status' => $row[2],
                        'accreditation' => $row[3],
                        'created_user' => Auth::user()->id,
                        'pbkp_ks' => $row[4],
                        'no_pbkp_ks' => empty($row[5]) ? null : $row[5],
                        'year_pbkp_ks' => empty($row[6]) ? null : $row[6],
                        'year_intervention' => empty($row[7]) ? null : $row[7]
                    ]);
                } else {
                    return true;
                }
            } else {
                return new School([
                    'name' => $row[0],
                    'city_id' => $row[1],
                    'status' => $row[2],
                    'accreditation' => $row[3],
                    'created_user' => Auth::user()->id,
                    'pbkp_ks' => $row[4],
                    'no_pbkp_ks' => empty($row[5]) ? null : $row[5],
                    'year_pbkp_ks' => empty($row[6]) ? null : $row[6],
                    'year_intervention' => empty($row[7]) ? null : $row[7]
                ]);
            }
        } else {
            return true;
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
}
