<?php

namespace App\Imports;

use App\Models\Report\Attendees;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class AttendeesImport implements ToModel, WithStartRow
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Attendees([
            'report_id' => $this->id,
            'name' => $row[0],
            'agency' => $row[1]
        ]);
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
