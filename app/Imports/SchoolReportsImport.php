<?php

namespace App\Imports;

use App\Models\Report\School;
use App\Models\School as SchoolMaster;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class SchoolReportsImport implements ToModel, WithStartRow
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $school = SchoolMaster::where('name', 'like', '%' . $row[0] . '%')->first();

        if ($school) {
            return new School([
                'report_id' => $this->id,
                'school_id' => $school ? $school->school_id : null,
                'canteen' => $row[1],
                'condition' => $row[2]
            ]);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
}
