<?php

namespace App\Imports;

use App\Models\Report\Community;
use App\Models\School;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class CommunityReportsImport implements ToModel, WithStartRow
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $school = School::where('name', 'like', '%' . $row[0] . '%')->first();

        if ($school) {
            return new Community([
                'report_id' => $this->id,
                'school_id' => $school ? $school->school_id : null,
                'school_community' => $row[1],
                'sum_community' => $row[2]
            ]);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
}
