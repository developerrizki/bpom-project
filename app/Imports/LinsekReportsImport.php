<?php

namespace App\Imports;

use App\Models\Report\Linsek;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class LinsekReportsImport implements ToModel, WithStartRow, WithMultipleSheets
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Linsek([
            'report_id' => $this->id,
            'city_id' => $row[0],
            'name' => $row[1],
            'participant' => $row[2]
        ]);
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
}
