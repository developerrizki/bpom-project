<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "provinces";
    protected $primaryKey = "province_id";

    public function city()
    {
        return $this->hasMany('App\Models\Master\City', 'province_id');
    }
}
