<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProvinceTarget extends Model
{
    protected $table = "province_targets";
    protected $primaryKey = "target_id";

    public function province()
    {
        return $this->belongsToMany('App\Models\Master\Province', 'province_id');
    }
}
