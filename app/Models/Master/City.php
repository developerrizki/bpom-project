<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "cities";
    protected $primaryKey = "city_id";

    public function province()
    {
        return $this->belongsTo('App\Models\Master\Province', 'province_id');
    }

    public function school()
    {
        return $this->hasMany('App\Models\School', 'city_id');
    }
}
