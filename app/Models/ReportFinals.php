<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportFinals extends Model
{
    protected $table        = "report_finals";
    protected $primaryKey   = "report_final_id";

    public function userCreate()
    {
        return $this->belongsToMany('App\User', 'created_user');
    }

    public function userUpdate()
    {
        return $this->belongsToMany('App\User', 'updated_user');
    }

    public function dataPaginate($request = array())
    {
        $school = ReportFinals::orderBy('created_at', 'DESC');

        if (($request->has('key') && isset($request->key)) && ($request->has('value') && isset($request->value))) {
            $school = $school->where($request->key, 'like', '%' . $request->value . '%');
        }

        $school = $school->paginate(20);
        $school->appends($request->all());

        return $school;
    }
}
