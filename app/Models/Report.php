<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table        = "reports";
    protected $primaryKey   = "report_id";

    public function interviewees()
    {
        return $this->hasMany('App\Models\Report\Interviewees', 'report_id');
    }

    public function summary()
    {
        return $this->hasMany('App\Models\Report\Summary', 'report_id');
    }

    public function galleries()
    {
        return $this->hasMany('App\Models\Report\Gallery', 'report_id');
    }

    public function files()
    {
        return $this->hasMany('App\Models\Report\File', 'report_id');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Report\Document', 'report_id');
    }

    public function attendees()
    {
        return $this->hasMany('App\Models\Report\Attendees', 'report_id');
    }

    public function attendanceFiles()
    {
        return $this->hasMany('App\Models\Report\AttendanceFile', 'report_id');
    }

    public function linsek()
    {
        return $this->hasMany('App\Models\Report\Linsek', 'report_id');
    }

    public function linsekFiles()
    {
        return $this->hasMany('App\Models\Report\LinsekFile', 'report_id');
    }

    public function package()
    {
        return $this->hasMany('App\Models\Report\Package', 'report_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Master\Province', 'province_id');
    }

    public function dataPaginate($request, $is_type = null)
    {
        $report = Report::orderBy('reports.created_at', 'DESC')->where('type', $is_type);

        if (($request->has('key') && isset($request->key)) && ($request->has('value') && isset($request->value))) {
            $report = $report->where($request->key, 'like', '%' . $request->value . '%');
        }

        if (Auth::user()->level_id != 1 || Auth::user()->level->level != "Admin Pusat") {
            $report = $report->where('created_user', Auth::user()->id);
        }

        if ($is_type == 0) {
            $report = $report->where('is_support', 0)->join('report_files', 'reports.report_id', '=', 'report_files.report_id');
        }

        $report = $report->paginate(20);
        $report->appends($request->all());

        return $report;
    }
}
