<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'user_levels';
    protected $primaryKey = 'level_id';

    public function users()
    {
        return $this->hasMany('App\User', 'level_id');
    }
}
