<?php

namespace App\Models\Message;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "messages";

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }

    public function receipt()
    {
        return $this->belongsTo('App\User', 'receipt_id', 'id');
    }
}
