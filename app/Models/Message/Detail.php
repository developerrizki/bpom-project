<?php

namespace App\Models\Message;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = "message_details";
}
