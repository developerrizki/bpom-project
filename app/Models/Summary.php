<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    protected $table        = "summaries";
    protected $primaryKey   = "summary_id";

    public function userCreate()
    {
        return $this->belongsToMany('App\User', 'created_user');
    }

    public function userUpdate()
    {
        return $this->belongsToMany('App\User', 'updated_user');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Master\Province', 'province_id');
    }
}
