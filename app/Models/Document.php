<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = "documents";
    protected $primaryKey = "document_id";

    public function userCreate()
    {
        return $this->belongsToMany('App\User', 'created_user');
    }

    public function userUpdate()
    {
        return $this->belongsToMany('App\User', 'updated_user');
    }

    public function dataPaginate($request = array(), $is_other = null)
    {
        $document = Document::orderBy('created_at', 'DESC');

        if (($request->has('key') && isset($request->key)) && ($request->has('value') && isset($request->value))) {
            $document = $document->where($request->key, 'like', '%' . $request->value . '%');
        }

        if (isset($is_other)) {
            $document = $document->where('is_other', $is_other);
        }

        $document = $document->paginate(20);
        $document->appends($request->all());

        return $document;
    }
}
