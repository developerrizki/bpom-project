<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table        = "surveys";
    protected $primaryKey   = "survey_id";

    public function userCreate()
    {
        return $this->belongsToMany('App\User', 'created_user');
    }

    public function userUpdate()
    {
        return $this->belongsToMany('App\User', 'updated_user');
    }

    public function dataPaginate($request = array(), $is_online = null)
    {
        $survey = Survey::orderBy('created_at', 'DESC');

        if (($request->has('key') && isset($request->key)) && ($request->has('value') && isset($request->value))) {
            $survey = $survey->where($request->key, 'like', '%' . $request->value . '%');
        }

        if ($is_online == 1 || $is_online == 0) {
            $survey = $survey->where('is_online', $is_online);
        }

        $survey = $survey->paginate(20);
        $survey->appends($request->all());

        return $survey;
    }
}
