<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table        = "schools";
    protected $primaryKey   = "school_id";
    protected $fillable     = [
        'name',
        'city_id',
        'status',
        'accreditation',
        'created_user',
        'pbkp_ks',
        'no_pbkp_ks',
        'year_pbkp_ks',
        'year_intervention',
    ];

    public function userCreate()
    {
        return $this->belongsToMany('App\User', 'created_user');
    }

    public function userUpdate()
    {
        return $this->belongsToMany('App\User', 'updated_user');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Master\City', 'city_id');
    }

    public function report()
    {
        return $this->hasOne('App\Models\Report\School', 'school_id');
    }

    public function certificate()
    {
        return $this->hasOne('App\Models\Report\Certificate', 'school_id');
    }

    public function audit()
    {
        return $this->hasOne('App\Models\Report\Audit', 'school_id');
    }

    public function community()
    {
        return $this->hasOne('App\Models\Report\Community', 'school_id');
    }

    public function dataPaginate($request = array(), $id = null)
    {
        $school = School::orderBy('school_id', 'ASC');

        if (($request->has('key') && isset($request->key)) && ($request->has('value') && isset($request->value))) {
            $school = $school->where($request->key, 'like', '%' . $request->value . '%');
        }

        if (isset($request->city_id)) {
            $school = $school->where('schools.city_id', $request->city_id);
        }

        if (isset($request->year_intervention)) {
            $school = $school->where('schools.year_intervention', $request->year_intervention);
        }

        if (isset($request->pbkp_ks)) {
            $school = $school->where('schools.pbkp_ks', $request->pbkp_ks);
        }

        if (isset($request->year_pbkp_ks)) {
            $school = $school->where('schools.year_pbkp_ks', $request->year_pbkp_ks);
        }

        if (isset($id)) {
            $school = $school->join('cities', 'cities.city_id', '=', 'schools.city_id')
                        ->join('provinces', 'provinces.province_id', '=', 'cities.province_id')
                        ->where('provinces.province_id', $id);
        }

        $school = $school->paginate(20);
        $school->appends($request->all());

        return $school;
    }
}
