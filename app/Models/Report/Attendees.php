<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Attendees extends Model
{
    protected $table        = "report_attendees";
    protected $primaryKey   = "report_attendance_id";
    protected $fillable     = [
        'report_id',
        'name',
        'agency'
    ];
}
