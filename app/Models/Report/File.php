<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table        = "report_files";
    protected $primaryKey   = "report_file_id";
}
