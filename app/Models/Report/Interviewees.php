<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Interviewees extends Model
{
    protected $table        = "report_interviewees";
    protected $primaryKey   = "report_interview_id";
    protected $fillable     = [
        'report_id',
        'name',
        'agency',
        'position',
        'theory'
    ];
}
