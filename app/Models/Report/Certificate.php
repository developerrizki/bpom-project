<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table        = "report_certificates";
    protected $primaryKey   = "report_certificate_id";

    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id');
    }
}
