<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class LinsekFile extends Model
{
    protected $table        = "report_linsek_file";
    protected $primaryKey   = "linsek_file_id";
}
