<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class AttendanceFile extends Model
{
    protected $table        = "attendance_files";
    protected $primaryKey   = "attendance_files_id";
}
