<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table        = "report_documents";
    protected $primaryKey   = "report_document_id";
}
