<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table        = "report_audits";
    protected $primaryKey   = "report_audit_id";

    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id');
    }
}
