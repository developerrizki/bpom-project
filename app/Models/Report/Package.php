<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table        = "report_packages";
    protected $primaryKey   = "report_package_id";

    public function userCreate()
    {
        return $this->belongsToMany('App\User', 'created_user');
    }

    public function userUpdate()
    {
        return $this->belongsToMany('App\User', 'updated_user');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Master\City', 'city_id');
    }
}
