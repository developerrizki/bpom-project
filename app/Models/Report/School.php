<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table        = "report_schools";
    protected $primaryKey   = "report_school_id";
    protected $fillable     = [
        'report_id', 'school_id', 'canteen', 'condition'
    ];

    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id');
    }
}
