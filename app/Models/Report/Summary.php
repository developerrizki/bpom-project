<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    protected $table        = "report_summaries";
    protected $primaryKey   = "report_summary_id";
}
