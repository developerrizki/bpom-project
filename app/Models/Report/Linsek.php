<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Linsek extends Model
{
    protected $table        = "report_linsek";
    protected $primaryKey   = "report_linsek_id";
    protected $fillable     = [
        'report_id', 'name', 'city_id', 'participant'
    ];

    public function city()
    {
        return $this->belongsTo('App\Models\Master\City', 'city_id');
    }
}
