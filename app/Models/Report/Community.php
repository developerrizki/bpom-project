<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    protected $table        = "report_communities";
    protected $primaryKey   = "report_community_id";
    protected $fillable     = [
        'report_id', 'school_id', 'school_community', 'sum_community'
    ];

    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id');
    }
}
