<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table        = "report_galleries";
    protected $primaryKey   = "report_gallery_id";
}
