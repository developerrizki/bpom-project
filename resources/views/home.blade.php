@extends('layouts.root')

@section('title', 'Beranda')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Filter Tahun Sekolah Sudah Terintervensi</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success"> <br>
                                <form action="{{ url('beranda') }}" class="form-horizontal" method="GET">
                                    @csrf
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Tahun Mulai</label>
                                                <input type="number" min="1900" max="2099" step="1" maxlength="4"
                                                    class="form-control" name="start_year"
                                                    placeholder="Masukan Tahun Filter"
                                                    value="{{ Request::has('start_year') ? Request::get('start_year') : date('Y') }}">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Tahun Selesai</label>
                                                <input type="number" min="1900" max="2099" step="1" maxlength="4"
                                                    class="form-control" name="end_year" placeholder="Masukan Tahun Filter"
                                                    value="{{ Request::has('end_year') ? Request::get('end_year') : date('Y') }}">
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-info btn-save btn-sm"
                                                    style="margin-top: 36px">Terapkan</button>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <a href="{{ url('beranda') }}"
                                                    class="btn btn-block btn-danger btn-save btn-sm"
                                                    style="margin-top: 36px">Batalkan Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">

                                <div class="statistics mt-3">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/progress.svg') }}" alt="">
                                            </div>
                                            {{-- <h4 class="text-info">70,5%</h4> --}}
                                            <h4 class="text-info">0%</h4>
                                            <p>Progress Kegiatan</p>
                                            <a class="btn btn-outline-primary border-radius-10"
                                                href="#modal-progress-kegiatan" data-toggle="modal">
                                                Selengkapnya
                                            </a>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/school.svg') }}" alt="">
                                            </div>
                                            {{-- <h4 class="text-success">789</h4> --}}
                                            <h4 class="text-success">{{ $data['school_of_intervention'] }}</h4>
                                            <p>Sekolah Sudah Terintervensi</p>
                                            {{-- <a class="btn btn-outline-primary border-radius-10" href="#modal-sekolah" data-toggle="modal"> --}}
                                            <a class="btn btn-outline-primary border-radius-10"
                                                href="{{ url('laporan-sertifikat') }}">
                                                Selengkapnya
                                            </a>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/shop.svg') }}" alt="">
                                            </div>
                                            {{-- <h4 class="text-warning">809</h4> --}}
                                            <h4 class="text-warning">{{ $data['canteen'] }}</h4>
                                            <p>Kantin Sudah Dibina</p>
                                            <a class="btn btn-outline-primary border-radius-10" href="#modal-kantin"
                                                data-toggle="modal">
                                                Selengkapnya
                                            </a>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/star.svg') }}" alt="">
                                            </div>
                                            {{-- <h4 class="text-danger">126</h4> --}}
                                            <h4 class="text-danger">{{ $data['certificate'] }}</h4>
                                            <p>Sekolah Penerima PBKP-KS</p>
                                            <a class="btn btn-outline-primary border-radius-10" href="#modal-sertifikasi"
                                                data-toggle="modal">
                                                Selengkapnya
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="success">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="title text-center">
                                            <h6>Peta Sebaran Sekolah Yang Sudah Diintervensi</h6>
                                        </div>
                                        <div class="map">
                                            <div id="map" style="width:100%; height:480px;"></div>
                                            {{-- <img src="{{ asset('/img/map.svg') }}" class="img-fluid" alt="" width="100%"> --}}
                                            {{-- <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJtwRkSdcHTCwRhfStG-dNe-M&key=AIzaSyBbWo1znuDQmcwzLYkcN1uU0zZUh0d_8uk&zoom=5" allowfullscreen></iframe> --}}
                                        </div>
                                        <div class="row justify-content-between">
                                            <div class="col-md-auto">
                                                <p class="font-weight-bold">Jumlah Provinsi: {{ $province_count }}
                                                    Provinsi</p>
                                            </div>
                                            <div class="col-md-auto">
                                                <p class="font-weight-bold">Target Sekolah: {{ $school_count->total }}
                                                    Sekolah</p>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-3">
                                        <div class="title text-center">
                                            <h6>Kalender Event</h6>
                                        </div>
                                        <div class="calendar-list">
                                            <div id="calendar-small"></div>
                                        </div>
                                         <div class="event-list mb-40 mt-30">
                                            <div class="item">
                                                <div>
                                                    <h6>10 Desember</h6>
                                                    <p>Meeting dengan pusat</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div>
                                                    <h6>18 Desember</h6>
                                                    <p>Meeting dengan pusat</p>
                                                </div>
                                            </div>
                                        </div>

                                        <a href="{{ url('kalender') }}" class="btn btn-outline-success btn-block">
                                            Selengkapnya
                                        </a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <ul class="nav justify-content-center" role="tablist">
                                            <li class="nav-item">
                                                <a href="#survey-online" class="nav-link active" role="tab"
                                                    data-toggle="tab" aria-selected="true">Survey Online</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content p-0 m-0">
                                            <div class="tab-pane active" id="survey-online" role="tabpanel">
                                                <div class="document-list">
                                                    @if (sizeof($survey) > 0)
                                                        @foreach ($survey as $key => $item)
                                                            <h6>
                                                                <a href="{{ url($item->link) }}"
                                                                    target="_blank">{{ $item->file }}</a>
                                                                <span class="datestamp"><i class="fas fa-clock"></i> &nbsp;
                                                                    {{ date('d m Y', strtotime($item->created_at)) }}</span>
                                                            </h6>
                                                        @endforeach
                                                    @else
                                                        <h6>Data Survey Belum Tersedia</h6>
                                                    @endif
                                                </div>
                                                <a href="{{ url('survey-online') }}"
                                                    class="btn btn-outline-success btn-block mt-30">Selengkapnya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <ul class="nav justify-content-center" role="tablist">
                                            <li class="nav-item">
                                                <a href="#materi" class="nav-link active" role="tab" data-toggle="tab"
                                                    aria-selected="true">Materi</a>
                                            </li>
                                            <li class="nav-item" style="border-left: #009953 1px solid;">
                                                <a href="#informasi-lainnya" class="nav-link" role="tab" data-toggle="tab"
                                                    aria-selected="false">Informasi Lainya</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content p-0 m-0">
                                            <div class="tab-pane active" id="materi" role="tabpanel">
                                                <div class="document-list">
                                                    @if (sizeof($theory) > 0)
                                                        @foreach ($theory as $key => $item)
                                                            <h6>
                                                                <a href="{{ Storage::url('dokumen-materi/' . $item->file) }}"
                                                                    target="_blank">{{ $item->file }}</a>
                                                                <span class="datestamp"><i class="fas fa-clock"></i> &nbsp;
                                                                    {{ date('d m Y', strtotime($item->created_at)) }}</span>
                                                            </h6>
                                                        @endforeach
                                                    @else
                                                        <h6>Data Materi Belum Tersedia</h6>
                                                    @endif
                                                </div>
                                                <a href="{{ url('dokumen-materi') }}"
                                                    class="btn btn-outline-success btn-block mt-30">Selengkapnya</a>
                                            </div>
                                            <div class="tab-pane" id="informasi-lainnya" role="tabpanel">
                                                <div class="document-list">
                                                    @if (sizeof($other) > 0)
                                                        @foreach ($other as $key => $item)
                                                            <h6>
                                                                <a href="{{ Storage::url('dokumen-lainnya/' . $item->file) }}"
                                                                    target="_blank">{{ $item->file }}</a>
                                                                <span class="datestamp"><i class="fas fa-clock"></i> &nbsp;
                                                                    {{ date('d m Y', strtotime($item->created_at)) }}</span>
                                                            </h6>
                                                        @endforeach
                                                    @else
                                                        <h6>Data Informasi Lainnya Belum Tersedia</h6>
                                                    @endif
                                                </div>
                                                <a href="{{ url('dokumen-lainnya') }}"
                                                    class="btn btn-outline-success btn-block mt-30">Selengkapnya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL -->
        <div id="modal-progress" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="modal-body text-center">
                        <h6 class="text-success font-weight-bold">
                            Semangat! Target sudah tercapai sebesar
                        </h6>
                        <img src="{{ asset('/img/popup.png') }}" class="img-fluid" alt="">
                        {{-- <h3 class="text-warning">70%</h3> --}}
                        <h3 class="text-warning">0%</h3>
                        <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70"
                                aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-progress-kegiatan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div class="title text-center">
                            <h6>
                                Progress Kegiatan
                            </h6>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th class="text-info">TOT di Pusat</th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['tot'] }}%</th>
                                            </tr>
                                            <tr>
                                                <th class="text-info">Advokasi</th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['advocacy'] }}%</th>
                                            </tr>
                                            <tr>
                                                <th class="text-info">Bimtek</th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['technical'] }}%</th>
                                            </tr>
                                            <tr>
                                                <th class="text-info">Pelatihan PBKP-KS</th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['trainning'] }}%</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th class="text-info">Audit PBKP-KS</th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['audit'] }}%</th>
                                            </tr>
                                            <tr>
                                                <th class="text-info">Sampling PJAS & Operasionalisasi Mobling </th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['sampling'] }}%</th>
                                            </tr>
                                            <tr>
                                                <th class="text-info">Paket Edukasi</th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['package'] }}%</th>
                                            </tr>
                                            <tr>
                                                <th class="text-info">Monitoring & Evaluasi</th>
                                                <td>:</td>
                                                <th class="text-success">{{ $progress['monev'] }}%</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-sekolah" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div class="title text-center">
                            <h6>
                                Sekolah Yang Sudah Di Intervensi
                            </h6>
                        </div>

                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Sekolah</th>
                                        <th>Nama Kota/Kabupaten</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (sizeof($data['list_school']) > 0)
                                        @foreach ($data['list_school'] as $key => $item)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->city }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3" class="text-center">Data Sekolah Tidak Tersedia.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-kantin" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div class="title text-center">
                            <h6>
                                Kantin Yang Sudah Dibina
                            </h6>
                        </div>

                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Sekolah</th>
                                        <th>Nama Kota/Kabupaten</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (sizeof($data['list_canteen']) > 0)
                                        @foreach ($data['list_canteen'] as $key => $item)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->city }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3" class="text-center">Data Kantin Sudah Dibina Tidak Tersedia.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-sertifikasi" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div class="title text-center">
                            <h6>
                                Penerima PBKP - KS
                            </h6>
                        </div>

                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Sekolah</th>
                                        <th>Nama Kota/Kabupaten</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (sizeof($data['list_certificate']) > 0)
                                        @foreach ($data['list_certificate'] as $key => $item)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->city }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3" class="text-center">Data Sekolah Penerima Piagam PBKP-KS
                                                Tersedia.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- END MODAL -->
    </main>
@endsection

@push('script')
    <script>
        // fake JSON call
        function getJSONMarkers() {
            // const markers = [
            //     {
            //         name:  "Aceh 0 Sekolah",
            //         location: [4.695135,96.7493993]
            //     }
            // ];

            const markers = {!! $province !!}
            return markers;
        }

        function initMap() {
            // Initialize Google Maps
            const mapOptions = {
                center: new google.maps.LatLng(-1.6611119, 115.5083405),
                zoom: 5
            }
            const map = new google.maps.Map(document.getElementById("map"), mapOptions);

            // Load JSON Data
            const provinceMarkers = getJSONMarkers();

            var infowindow = new google.maps.InfoWindow();

            // Initialize Google Markers
            for (province of provinceMarkers) {
                let marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(province.location[0], province.location[1]),
                    title: province.name
                })

                google.maps.event.addListener(marker, 'click', (function(marker) {
                    return function() {
                        infowindow.setContent(this.name);
                        infowindow.open(map, marker);
                    }
                })(marker));
            }
        }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDM-y_eKspsKCift3q9uaZECiUKbh4ORGY&callback=initMap">
    </script>
@endpush
