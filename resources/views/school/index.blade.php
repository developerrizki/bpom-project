@extends('layouts.root')

@section('title','Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Filter</h6>
                                        </div>
                                    </div>
                                </div>
                                <form action="{{ url('sekolah/filter') }}" class="form-horizontal" method="GET">
                                    @csrf
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <select name="province_id" id="province_id" class="form-control custom-select" style="padding: 11px !important;" required>
                                                    <option value="">Pilih Provinsi</option>
                                                    @foreach($optProvince as $key => $item)
                                                        <option value="{{ $item->province_id }}">{{ $item->province }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <select name="city_id" id="city_id" class="form-control custom-select" style="padding: 11px !important;">
                                                    <option value="">Kota</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="number" name="year_intervention" min="1900" max="2099" step="1" maxlength="4" placeholder="Masukan tahun intervensi" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info btn-save btn-sm btn-block">Cari</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="title mb-20">
                                    <h6>Peta Sebaran Sekolah Yang Sudah Diintervensi</h6>
                                </div>
                                <hr class="success mb-30">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="map">
                                            {{--  <img src="{{ asset('img/map.svg') }}" class="img-fluid" alt="" width="100%">  --}}
                                            {{-- <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJtwRkSdcHTCwRhfStG-dNe-M&key=AIzaSyBbWo1znuDQmcwzLYkcN1uU0zZUh0d_8uk&zoom=5" allowfullscreen></iframe> --}}
                                            <div id="map" style = "width:100%; height:480px;"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="font-weight-bold">Jumlah Provinsi: {{ $totalProvince }} Provinsi</p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p class="font-weight-bold">Target Sekolah: {{ $school_target->total }} Sekolah</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(\Auth::user()->level_id != 3)
                            <div class="row mb-20">
                                <div class="col-1">
                                    <a href="#modal-progress-kegiatan" data-toggle="modal" class="btn btn-success btn-sm">Import Data</a>
                                </div> 
                                <div class="col-2" style="margin-left: 25px">
                                    <a href="{{ url('sekolah/tambah') }}" class="btn btn-success btn-sm">+ &nbsp; Data Sekolah</a>
                                </div>  
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Data Sekolah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th>Nama Provinsi</th>
                                            <th width="10%"></th>
                                        </thead>
                                        <tbody>
                                            @if(sizeof($province) > 0)
                                                @foreach($province as $key => $item)
                                                    <tr>
                                                        <td>{{ $item->province }}</td>
                                                        <td>
                                                            <a href="{{ url('sekolah/detil/'. $item->province_id) }}" class="btn btn-primary btn-sm">Lihat Data</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="2">Data Provinsi Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    {!! $province->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->

    <div id="modal-progress-kegiatan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="modal-body">
                    <div class="title text-center">
                        <h6>
                            Import Data Sekolah
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('sekolah/import') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" style="margin-bottom: 20px">
                                    <input type="file" name="file" class="form-control" required style="margin-bottom: 10px">
                                    <a href="{{ asset('excels/import-data-sekolah.xlsx') }}" class="text-danger">Unduh format excel terlebih dahulu. sebelum melakukan import data (Klik Disini)</a>
                                </div>
                                <div class="form-group" style="margin-bottom: 20px">
                                    <button type="submit" class="btn btn-sm btn-success">Import</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Batal</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('#province_id').bind('change', function(){
            var province_id = $(this).val();
            var url = '{{ url("getCity") }}';

            $.get(url,{province_id : province_id}, function(){
                console.log('loading ...');
            }).done(function(data){
                $('#city_id').html(data);
                $('#city_id').attr('disabled',false);
            }).fail(function(){
                alert('Gagal mengambil data Kota/Kabupaten');
            });
        })
    </script>

    <script>

        // fake JSON call
        function getJSONMarkers() {
            const markers = {!! $jsonProvince !!}
            return markers;
        }

        function initMap() {
            // Initialize Google Maps
            const mapOptions = {
                center:new google.maps.LatLng(-1.6611119,115.5083405),
                zoom: 5
            }
            const map = new google.maps.Map(document.getElementById("map"), mapOptions);

            // Load JSON Data
            const provinceMarkers = getJSONMarkers();

			var infowindow = new google.maps.InfoWindow();

            // Initialize Google Markers
            for(province of provinceMarkers) {
                let marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(province.location[0], province.location[1]),
                    title: province.name
                })

                google.maps.event.addListener(marker, 'click', (function(marker) {
					return function() {
						infowindow.setContent(province.name);
						infowindow.open(map, marker);
					}
				})(marker));
            }
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDM-y_eKspsKCift3q9uaZECiUKbh4ORGY&callback=initMap">
    </script>
@endpush