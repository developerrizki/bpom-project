@extends('layouts.root')

@section('title','Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Data Sekolah &rsaquo; Ubah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('sekolah/ubah/'.$school->school_id) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Sekolah</label>
                                        <input type="text" class="form-control" name="name" placeholder="Masukan nama sekolah" value="{{ $school->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Provinsi</label>
                                        <select name="province" id="province" class="form-control custom-select">
                                            <option value="">Pilih Provinsi</option>
                                            @foreach($province as $key => $item)
                                                <option value="{{ $item->province_id }}" {{ $school->city->province_id == $item->province_id ? "selected" : "" }}>{{ $item->province }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kota / Kabupaten</label>
                                        <select name="city" id="city" class="form-control custom-select">
                                            <option value="">Pilih Kota / Kabupaten</option>
                                            @foreach($city as $key => $item)
                                                <option value="{{ $item->city_id }}" {{ $school->city_id == $item->city_id ? "selected" : "" }}>{{ $item->city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Status</label>
                                        <select name="status" id="status" class="form-control custom-select">
                                            <option value="">Pilih Status Sekolah</option>
                                            <option value="0" {{ $school->status == 0 ? "selected" : "" }}>Negeri</option>
                                            <option value="1" {{ $school->status == 1 ? "selected" : "" }}>Swasta</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Akreditasi Sekolah</label>
                                        <input type="text" class="form-control" name="accreditation" placeholder="Masukan akreditasi sekolah" value="{{ $school->accreditation }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Tahun Intervensi</label>
                                        <input type="text" class="form-control" name="year_intervention" placeholder="Masukan tahun intervensi sekolah" value="{{ $school->year_intervention }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">PBKP-KS</label>
                                        <select name="pbkp_ks" id="pbkp_ks" class="form-control custom-select">
                                            <option value="">Pilih</option>
                                            <option value="0" {{ $school->pbkp_ks == 0 ? "selected" : "" }}>Belum</option>
                                            <option value="1" {{ $school->pbkp_ks == 1 ? "selected" : "" }}>Sudah</option>
                                        </select>
                                    </div>
                                    {{-- @if($school->pbkp_ks == 1) --}}
                                        <div class="form-group" id="no_pbkp_ks" {{ $school->pbkp_ks == 0 ? 'style=display:none' : ""}}>
                                            <label for="" class="control-label">Nomor Sertifikat PBKP-KS</label>
                                            <input type="text" class="form-control" name="no_pbkp_ks" placeholder="Masukan nomor sertifikat PBKP-KS" value="{{ $school->no_pbkp_ks }}">
                                        </div>
                                        <div class="form-group" id="year_pbkp_ks" {{ $school->pbkp_ks == 0 ? 'style=display:none' : ""}}>
                                            <label for="" class="control-label">Tahun perolehan Sertifikat PBKP-KS</label>
                                            <input type="text" class="form-control" name="year_pbkp_ks" placeholder="Masukan tahun perolehan sertifikat PBKP-KS" value="{{ $school->year_pbkp_ks }}" maxlength="4">
                                        </div>
                                    {{-- @endif --}}
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script>
        $('#province').bind('change', function(){
            var province_id = $(this).val();
            var url = '{{ url("getCity") }}';

            $.get(url,{province_id : province_id}, function(){
                console.log('loading ...');
            }).done(function(data){
                $('#city').html(data);
                $('#city').attr('disabled',false);
            }).fail(function(){
                alert('Gagal mengambil data Kota/Kabupaten');
            });
        })

        $('#pbkp_ks').bind('change', function(){
            var pbkp_ks = $(this).val();
            if(pbkp_ks == 1) {
                $('#no_pbkp_ks').show();
                $('#year_pbkp_ks').show();
            }else{
                $('#no_pbkp_ks').hide();
                $('#year_pbkp_ks').hide();
                
                $('input[name="no_pbkp_ks"]').val('');
                $('input[name="year_pbkp_ks"]').val('');
            }
        });
    </script>
@endpush