@extends('layouts.root')

@section('title','Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Pencarian</h6>
                                        </div>
                                    </div>
                                </div>
                                <form action="{{ url('sekolah/detil/'.$province->province_id) }}" class="form-horizontal" method="GET">
                                    @csrf
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="form-group">
                                                <select name="key" id="key" class="form-control custom-select" style="padding: 11px !important;" required>
                                                    <option value="">Pencarian Berdasarkan</option>
                                                    <option value="name" {!! Request::get('key') == "name" ? "selected" : "" !!} >Nama Sekolah</option>
                                                    <option value="year_intervention" {!! Request::get('key') == "year_intervention" ? "selected" : "" !!} >Tahun Intervensi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="value" placeholder="Masukan data pencarian" value="{{ Request::get('value') }}" style="padding: 11px !important;" required>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info btn-save btn-sm btn-block">Cari</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row mb-20">
                            <div class="col-1">
                                <a href="#modal-progress-kegiatan" data-toggle="modal" class="btn btn-success btn-sm">Import Data</a>
                            </div> 
                            <div class="col-2" style="margin-left: 25px">
                                <a href="{{ url('sekolah/tambah') }}" class="btn btn-success btn-sm">+ &nbsp; Data Sekolah</a>
                            </div>  
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6> <a href="{{ url('sekolah') }}">Data Sekolah</a>  &rsaquo; Provinsi {{ ucwords($province->province) }}</h6>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                {{--  <div class="col-6">
                                                    <a href="#" class="btn btn-block btn-info btn-sm">Unggah Dari Excel</a>
                                                </div>  --}}
                                                {{-- <div class="col-12">
                                                    <a href="{{ url('sekolah/tambah') }}" class="btn btn-block btn-success btn-sm">+ &nbsp; Data Sekolah</a>
                                                </div>   --}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <hr class="success mt-0 mb-30" style="height:2px;">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th>No</th>
                                            <th>Nama Sekolah</th>
                                            <th>Status</th>
                                            <th>Provinsi</th>
                                            <th>Kab / Kota</th>
                                            <th>Akreditasi</th>
                                            <th>Tahun Intervensi</th>
                                            <th>Status PBKP-KS</th>
                                            <th>Nomor PBKP-KS</th>
                                            <th>Tahun Penerimaan PBKP-KS</th>
                                            <th width="15%"></th>
                                        </thead>
                                        <tbody>
                                            @if(sizeof($school) > 0)
                                                @foreach($school as $key => $item)
                                                    <tr>
                                                        <td>{{ ($school->currentpage()-1) * $school->perpage() + $key+1 }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->status == 0 ? "Negeri" : "Swasta"}}</td>
                                                        <td>{{ $item->province}}</td>
                                                        <td>{{ $item->city}}</td>
                                                        <td>{{ $item->accreditation }}</td>
                                                        <td>{{ $item->year_intervention }}</td>
                                                        <td>{{ $item->pbkp_ks == 0 ? "Belum" : "Sudah" }}</td>
                                                        <td>{{ !empty($item->no_pbkp_ks) ? $item->no_pbkp_ks : "-" }}</td>
                                                        <td>{{ !empty($item->year_pbkp_ks) ? $item->year_pbkp_ks : "-" }}</td>
                                                        <td>
                                                            @if(\Auth::user()->level_id == 1 || \Auth::user()->level_id == 2)
                                                                <a href="{{ url('sekolah/ubah/'. $item->school_id) }}" class="btn btn-info btn-sm" title="Ubah" data-toggle="tooltip" ><i class="icon fa fa-pen"></i></a>
                                                                <button class="btn btn-danger btn-delete btn-sm" title="Hapus" data-toggle="tooltip" data-action="{{ url('sekolah/hapus/'. $item->school_id) }}"><i class="icon fa fa-trash"></i></button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6" class="text-center">Data Sekolah Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    {!! $school->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
    <div id="modal-progress-kegiatan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="modal-body">
                    <div class="title text-center">
                        <h6>
                            Import Data Sekolah
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('sekolah/import') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" style="margin-bottom: 20px">
                                    <input type="file" name="file" class="form-control" required style="margin-bottom: 10px">
                                    <a href="{{ asset('excels/import-data-sekolah.xlsx') }}" class="text-danger">Unduh format excel terlebih dahulu. sebelum melakukan import data (Klik Disini)</a>
                                </div>
                                <input type="hidden" name="province_id" value="{{ $province->province_id }}">
                                <div class="form-group" style="margin-bottom: 20px">
                                    <button type="submit" class="btn btn-sm btn-success">Import</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Batal</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Data Sekolah');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush