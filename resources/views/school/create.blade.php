@extends('layouts.root')

@section('title','Sekolah')

@push('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Data Sekolah &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('sekolah/tambah') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Sekolah</label>
                                        <input type="text" class="form-control" name="name" placeholder="Masukan nama sekolah" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Provinsi</label>
                                        <select name="province" id="province" class="form-control custom-select js-select-2" required>
                                            <option value="">Pilih Provinsi</option>
                                            @foreach($province as $key => $item)
                                                <option value="{{ $item->province_id }}">{{ $item->province }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kota / Kabupaten</label>
                                        <select name="city" id="city" class="form-control custom-select js-select-2" required disabled>
                                            <option value="">Pilih Kota / Kabupaten</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Status</label>
                                        <select name="status" id="status" class="form-control custom-select" required>
                                            <option value="">Pilih Status Sekolah</option>
                                            <option value="0">Negeri</option>
                                            <option value="1">Swasta</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Akreditasi Sekolah</label>
                                        <input type="text" class="form-control" name="accreditation" placeholder="Masukan akreditasi sekolah" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Tahun Intervensi</label>
                                        <input type="text" class="form-control" name="year_intervention" placeholder="Masukan tahun intervensi sekolah" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">PBKP-KS</label>
                                        <select name="pbkp_ks" id="pbkp_ks" class="form-control custom-select">
                                            <option value="">Pilih</option>
                                            <option value="0">Belum</option>
                                            <option value="1">Sudah</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="no_pbkp_ks" style="display:none">
                                        <label for="" class="control-label">Nomor Sertifikat PBKP-KS</label>
                                        <input type="text" class="form-control" name="no_pbkp_ks" placeholder="Masukan nomor sertifikat PBKP-KS">
                                    </div>
                                    <div class="form-group" id="year_pbkp_ks" style="display:none">
                                        <label for="" class="control-label">Tahun perolehan Sertifikat PBKP-KS</label>
                                        <input type="number" class="form-control" name="year_pbkp_ks" placeholder="Masukan tahun perolehan sertifikat PBKP-KS" maxlength="4">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm" disabled>Simpan</button>
                                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $('#province').bind('change', function(){
            var province_id = $(this).val();
            var url = '{{ url("getCity") }}';

            $.get(url,{province_id : province_id}, function(){
                console.log('loading ...');
            }).done(function(data){
                $('#city').html(data);
                $('#city').attr('disabled',false);
            }).fail(function(){
                alert('Gagal mengambil data Kota/Kabupaten');
            });
        })

        $('#pbkp_ks').bind('change', function(){
            var value = $(this).val();

            if(value != "" || value != null)
                $('.btn-save').attr('disabled',false);
        })

        $('#pbkp_ks').bind('change', function(){
            var pbkp_ks = $(this).val();
            if(pbkp_ks == 1) {
                $('#no_pbkp_ks').show();
                $('#year_pbkp_ks').show();
            }else{
                $('#no_pbkp_ks').hide();
                $('#year_pbkp_ks').hide();
            }
        });

        $(document).ready(function() {
            $('.js-select-2').select2();
        });
    </script>
@endpush