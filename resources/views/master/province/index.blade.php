@extends('layouts.root')

@section('title','Provinsi')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Pencarian</h6>
                                        </div>
                                    </div>
                                </div>
                                <form action="{{ url('provinsi') }}" class="form-horizontal" method="GET">
                                    @csrf
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="form-group">
                                                <select name="key" id="key" class="form-control custom-select" style="padding: 11px !important;" required>
                                                    <option value="">Pencarian Berdasarkan</option>
                                                    <option value="province" {!! Request::get('key') == "province" ? "selected" : "" !!} >Provinsi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="value" placeholder="Masukan data pencarian" value="{{ Request::get('value') }}" style="padding: 11px !important;" required>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info btn-save btn-sm btn-block">Cari</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6> <a href="{{ url('provinsi') }}">Provinsi</a></h6>
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-12">
                                                    <a href="{{ url('provinsi/tambah') }}" class="btn btn-block btn-success btn-sm">+ &nbsp; Tambah Baru</a>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30" style="height:2px;">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th width="5%">No</th>
                                            <th>Provinsi</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th width="20%"></th>
                                        </thead>
                                        <tbody>
                                            @if(sizeof($province) > 0)
                                                @foreach($province as $key => $item)
                                                    <tr>
                                                        <td>{{ ($province->currentpage()-1) * $province->perpage() + $key+1 }}</td>
                                                        <td>{{ $item->province }}</td>
                                                        <td>{{ $item->latitude }}</td>
                                                        <td>{{ $item->longitude }}</td>
                                                        <td>
                                                            <a href="{{ url('provinsi/target?province_id='.$item->province_id) }}" class="btn btn-success btn-sm" title="Target" data-toggle="tooltip" ><i class="icon fa fa-search"></i></a>
                                                            <a href="{{ url('provinsi/ubah/'. $item->province_id) }}" class="btn btn-info btn-sm" title="Ubah" data-toggle="tooltip" ><i class="icon fa fa-pen"></i></a>
                                                            <button class="btn btn-danger btn-delete btn-sm" title="Hapus" data-toggle="tooltip" data-action="{{ url('provinsi/hapus/'. $item->province_id) }}"><i class="icon fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3" class="text-center">Data Provinsi Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    {!! $province->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Hapus Data Provinsi');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush