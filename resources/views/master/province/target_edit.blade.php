@extends('layouts.root')

@section('title','Target')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6><a href="{{ url('provinsi') }}">Provinsi</a> &rsaquo; <a href="javascript:history.back()">Target</a> &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('provinsi/target/tambah') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Tahun</label>
                                        <input type="text" class="form-control" name="target_year" placeholder="Masukan tahun" value="{{ $target->target_year }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Target Sekolah</label>
                                        <input type="text" class="form-control" name="target" placeholder="Masukan target sekolah" value="{{ $target->target }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Target Kantin</label>
                                        <input type="text" class="form-control" name="target_canteen" placeholder="Masukan target kantin" value="{{ $target->target_canteen }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Target PBKP-KS</label>
                                        <input type="text" class="form-control" name="target_pbkp_ks" placeholder="Masukan target pbkp-ks" value="{{ $target->target_pbkp_ks }}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="javascript:history.back()" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                    <input type="hidden" name="province_id" value="{{ $province_id }}">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection