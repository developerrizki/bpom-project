@extends('layouts.root')

@section('title','Provinsi')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Provinsi &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('provinsi/ubah/'.$province->province_id) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Provinsi</label>
                                        <input type="text" class="form-control" name="province" placeholder="Masukan nama provinsi" value="{{ $province->province }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Latitude</label>
                                        <input type="text" class="form-control" name="latitude" value="{{ $province->latitude }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Longitude</label>
                                        <input type="text" class="form-control" name="longitude" value="{{ $province->longitude }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('provinsi') }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection