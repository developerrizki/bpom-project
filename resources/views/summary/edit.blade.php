@extends('layouts.root')

@section('title','Ringkasan Eksekutif')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Ringkasan Eksekutif &rsaquo; Ubah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('ringkasan-eksekutif/ubah/'.$summary->summary_id) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Provinsi</label>
                                        <select name="province_id" id="province_id" class="form-control custom-select">
                                            <option value="">Pilih Provinsi</option>
                                            @foreach($province as $key => $item)
                                                <option value="{{ $item->province_id }}" {{ $summary->province_id == $item->province_id ? "selected" : "" }}>{{ $item->province }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Ringkasan Ekskutif</label>
                                        <textarea name="summary" id="summary" cols="30" rows="20" class="form-control">{{ $summary->summary }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('ringkasan-eksekutif') }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script>
        $('#province').bind('change', function(){
            var province_id = $(this).val();
            var url = '{{ url("getCity") }}';

            $.get(url,{province_id : province_id}, function(){
                console.log('loading ...');
            }).done(function(data){
                $('#city').html(data);
                $('#city').attr('disabled',false);
            }).fail(function(){
                alert('Gagal mengambil data Kota/Kabupaten');
            });
        })
    </script>
@endpush