@extends('layouts.root')

@section('title','Ringkasan Eksekutif')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Ringkasan Eksekutif &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('ringkasan-eksekutif/tambah') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Provinsi</label>
                                        <select name="province_id" id="province_id" class="form-control custom-select">
                                            <option value="">Pilih Provinsi</option>
                                            @foreach($province as $key => $item)
                                                <option value="{{ $item->province_id }}">{{ $item->province }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Ringkasan Ekskutif</label>
                                        <textarea name="summary" id="summary" class="form-control" cols="30" rows="20"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('ringkasan-eksekutif') }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection