@extends('layouts.root')

@section('title','Ringkasan Ekskutif')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Ringkasan Ekskutif</h6>
                                        </div>
                                    </div>

                                    @if(\Auth::user()->level_id != 3)
                                        <div class="col-md-auto">
                                            <div class="action mb-20">
                                                <div class="row justify-content-end">
                                                    {{--  <div class="col-6">
                                                        <a href="#" class="btn btn-block btn-info btn-sm">Unggah Dari Excel</a>
                                                    </div>  --}}
                                                    {{-- <div class="col-12">
                                                        <a href="{{ url('ringkasan-eksekutif/tambah') }}" class="btn btn-block btn-success btn-sm">+ &nbsp; Ringkasan Ekskutif</a>
                                                    </div>   --}}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th>Nama Provinsi</th>
                                            <th width="10%"></th>
                                        </thead>
                                        <tbody>
                                            @if(sizeof($province) > 0)
                                                @foreach($province as $key => $item)
                                                    <tr>
                                                        <td>{{ $item->province }}</td>
                                                        <td>
                                                            <a href="{{ url('ringkasan-eksekutif/detil/'. $item->province_id) }}" class="btn btn-primary btn-sm">Lihat Ringkasan</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="2">Data Provinsi Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection