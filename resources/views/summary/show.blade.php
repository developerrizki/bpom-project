@extends('layouts.root')

@section('title','Ringkasan Eksekutif')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif

                        <div class="card">
                            <div class="card-body">
                                <div class="filter">
                                    <form action="{{ url('ringkasan-eksekutif/detil/'. Request::route('id')) }}">
                                        @csrf
                                        <div class="row justify-content-end">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select name="year" class="form-control custom-select" onchange="this.form.submit()">
                                                        <option value="">Pilih Tahun</option>
                                                        <option {{ Request::get('year') == "2016" ? "selected" : "" }} value="2016">2016</option>
                                                        <option {{ Request::get('year') == "2017" ? "selected" : "" }} value="2017">2017</option>
                                                        <option {{ Request::get('year') == "2018" ? "selected" : "" }} value="2018">2018</option>
                                                        <option {{ Request::get('year') == "2019" ? "selected" : "" }} value="2019">2019</option>
                                                        <option {{ Request::get('year') == "2020" ? "selected" : "" }} value="2020">2020</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
<br>
                                <div class="statistics">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/progress.svg') }}" alt="">
                                            </div>
                                            {{--  <h4 class="text-info">70,5%</h4>  --}}
                                            <h4 class="text-info">{{ $data['total_school'] }}</h4>
                                            <p>Target Sekolah</p>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/progress.svg') }}" alt="">
                                            </div>
                                            {{--  <h4 class="text-info">70,5%</h4>  --}}
                                            <h4 class="text-info">{{ $data['total_canteen'] }}</h4>
                                            <p>Target Kantin</p>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/progress.svg') }}" alt="">
                                            </div>
                                            {{--  <h4 class="text-info">70,5%</h4>  --}}
                                            <h4 class="text-info">{{ $data['total_pbkp_ks'] }}</h4>
                                            <p>Target PBKP-KS</p>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/school.svg') }}" alt="">
                                            </div>
                                            {{--  <h4 class="text-success">789</h4>  --}}
                                            <h4 class="text-success">{{ $data['school_of_intervention'] }}</h4>
                                            <p>Sekolah Sudah Terintervensi</p>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/shop.svg') }}" alt="">
                                            </div>
                                            {{--  <h4 class="text-warning">809</h4>  --}}
                                            <h4 class="text-warning">{{ $data['canteen'] }}</h4>
                                            <p>Kantin Sudah Dibina</p>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="icon">
                                                <img src="{{ asset('/img/icons/stats/star.svg') }}" alt="">
                                            </div>
                                            {{--  <h4 class="text-danger">126</h4>  --}}
                                            <h4 class="text-danger">{{ $data['certificate'] }}</h4>
                                            <p>Sekolah Penerima Piagam PBKP-KS</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection