@extends('layouts.root_login')

@section('title','Login')

@section('content')
    <main class="main">
        <div class="page-login">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 left" style="background-image: url(img/bg-login.png);">
                        <div class="caption">
                            <h5>Sistem Aplikasi Sekolah Pangan Aman</h5>
                            <h2>SIAP SAPA</h2>
                        </div>
                        <div class="social-media" style="font-size:12px">
                            <a href="#" target="_blank">
                                <div class="icon">
                                    <i class="fab fa-facebook-f"></i>
                                </div>
                                BPOM RI <br> klubpompi
                            </a>
                            <a href="#" target="_blank">
                                <div class="icon">
                                    <i class="fab fa-twitter"></i>
                                </div>
                                @bpom_ri <br> @klubpompi
                            </a>
                            <a href="#" target="_blank">
                                <div class="icon">
                                    <i class="fab fa-instagram"></i>
                                </div>
                                @bpom_ri <br> @klubpompi
                            </a>
                        </div> <br>
                    </div>
                    <div class="col-md-8">
                        <div class="right">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="logo">
                                        <img src="{{ asset('img/logo.png') }}" alt="" style="width:150px; height:145px;">
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger m-t-20">
                                            <ul style="margin-bottom:0">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" class="form-control" placeholder="username" name="username" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        </div>
                                        <div class="form-group mb-20">
                                            <label>Password</label>
                                            <input id="password" type="password" class="form-control" placeholder="password" name="password" required autocomplete="current-password">
                                        </div>
                                        <div class="form-group">
                                            <input type="checkbox" name="show-pin" id="show-pin" onclick="showPin()"/>
                                            Lihat Password
                                        </div>
                                        {{-- <div class="form-group text-right">
                                            <a href="#" class="forgot-password">Forgot your password?</a>
                                        </div> --}}
                                         <div class="form-group">
                                            @if(env('GOOGLE_RECAPTCHA_KEY'))
                                                <div class="g-recaptcha"
                                                    data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}">
                                                </div>
                                            @endif
                                        </div> 
                                        <button type="submit" class="btn btn-success btn-block text-uppercase btn-lg">Masuk</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        $('body').addClass('login-layout');

        function showPin() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
@endpush