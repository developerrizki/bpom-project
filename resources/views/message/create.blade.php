@extends('layouts.root')

@section('title','Pesan')

@push('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Pesan &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('pesan/tambah') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Subject</label>
                                        <input type="text" class="form-control" name="subject" placeholder="Masukan nama subject pesan anda" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Penerima</label>
                                        <select name="receipt_id" id="receipt_id" class="form-control custom-select js-select-2" required>
                                            <option value="">Pilih Penerima</option>
                                            @foreach($receipt as $key => $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('pesan') }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.js-select-2').select2();
        });
    </script>
@endpush