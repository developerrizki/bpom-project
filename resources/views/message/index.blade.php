@extends('layouts.root')

@section('title','Pesan')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body pb-0">
                                        <div class="row align-items-center justify-content-between">
                                            <div class="col-md-auto">
                                                <div class="title mb-20">
                                                    <h6>Messages</h6>
                                                </div>
                                            </div>
                                            <div class="col-md-auto">
                                                <div class="action mb-20">
                                                    <div class="row justify-content-end">
                                                        <div class="col-auto">
                                                            <a href="{{ url('pesan/tambah') }}" class="btn btn-success px-3"><i class="fas fa-pencil-alt"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="success mt-0 mb-30">
                                    </div>
                                    <div class="message-list perfect-scrollbar">
                                        <ul>
                                            @if (count($message) > 0)
                                                @foreach ($message as $key => $item)
                                                    <li class="">
                                                        <a href="{{ url('pesan/detil/'.$item->id) }}">
                                                            <p class="name">
                                                                {!! \Auth::user()->id == $item->sender_id ? '<span class="badge badge-danger">keluar</span>' : '<span class="badge badge-success">masuk</span>' !!}
                                                                {{ \Auth::user()->id == $item->receipt_id ? $item->sender->name : $item->receipt->name }}</p>
                                                            <p class="excerpt">{{ $item->subject }}</p>
                                                            <p class="timestamp">{{ date('d M Y H:i:s', strtotime($item->created_at)) }}</p>
                                                        </a>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li>
                                                    Data pesan belum tersedia.
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection