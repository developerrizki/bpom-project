<!DOCTYPE html>
<html>
<head>
    <title> @yield('title') - BPOM</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- FONT -->
    <link href="{{ asset('fonts/font-awesome/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&display=swap" rel="stylesheet"> 

    <!-- STYLE -->
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('lib/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('lib/fullcalendar/fullcalendar.css') }}">
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('lib/owl-carousel/owl-carousel.css') }}">
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/theme.css') }}">

    <script src='http://www.google.com/recaptcha/api.js'></script>
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="home.php">
                    <img src="{{ asset('img/logo.png') }}" alt="">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <img src="{{ asset('img/icons/burger.svg') }}" alt="">
                </button>

                <div class="collapse navbar-collapse text-center" id="navbar-menu">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="{{ asset('img/icons/settings.svg') }}" alt="">
                                </div>
                                Akun Saya
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="{{ asset('img/icons/mail.svg') }}" alt="">
                                </div>
                                Message
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="{{ asset('img/icons/notification.svg') }}" alt="">
                                </div>
                                Notifikasi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="{{ asset('img/icons/logout.svg') }}" alt="">
                                </div>
                                Log Out
                            </a>
                        </li>
                    </ul>
                </div>
    
            </div>
        </nav>
    </header>

    @yield('content')

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p class="copyright">
                        Copyright 2019. BPOM. All Right Reserved
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-migrate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/bootstrap/libraries/popper.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/bootstrap/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/fullcalendar/fullcalendar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/owl-carousel/owl-carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/theme.js') }}"></script>

    @stack('script')

</body>
</html>
