<aside class="sidebar">
    <nav class="sidebar-nav">
        @if(\Auth::user()->level_id == 1)
            <ul id="sidebarnav">
                <li class="nav-item">
                    <a href="{{ url('beranda') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/home.svg') }}" alt="">
                        </div>
                        Beranda
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/survey.svg') }}" alt="">
                        </div>
                        User
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('level') }}" class="nav-link">
                                Level
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('pengguna') }}" class="nav-link">
                                User
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/survey.svg') }}" alt="">
                        </div>
                        Master Data
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('provinsi') }}" class="nav-link">
                                Provinsi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('kota') }}" class="nav-link">
                                Kota
                            </a>
                        </li>
                    </ul>
                </li>

                {{--  <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Sistem
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('module') }}" class="nav-link">
                                Module
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('task') }}" class="nav-link">
                                Task
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('role') }}" class="nav-link">
                                Role
                            </a>
                        </li>
                    </ul>
                </li>  --}}

                <li class="nav-item">
                    <a href="{{ url('sekolah') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/school.svg') }}" alt="">
                        </div>
                        Sekolah
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('kalender') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/event.svg') }}" alt="">
                        </div>
                        Kalender Event
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Dokumen
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('dokumen-materi') }}" class="nav-link">
                                Materi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('dokumen-lainnya') }}" class="nav-link">
                                Informasi Lainnya
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Laporan
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('laporan-tot') }}" class="nav-link">
                                TOT di Pusat
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-advokasi') }}" class="nav-link">
                                Advokasi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-bimtek') }}" class="nav-link">
                                Bimtek PJAS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-pelatihan') }}" class="nav-link">
                                Pelatihan PBKP - KS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-sertifikat') }}" class="nav-link">
                                Sertifikat PBKP - KS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-audit') }}" class="nav-link">
                                Audit PBKP - KS
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ url('laporan-sampling') }}" class="nav-link">
                                Sampling PJAS & Operasional Mobling
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="{{ url('laporan-paket') }}" class="nav-link">
                                Paket Edukasi
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ url('laporan-monitoring') }}" class="nav-link">
                                Monitoring dan Evaluasi
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="{{ url('laporan-akhir') }}" class="nav-link">
                                Laporan Akhir
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/survey.svg') }}" alt="">
                        </div>
                        Survey
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('survey-online') }}" class="nav-link">
                                Online
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('survey-kuesioner') }}" class="nav-link">
                                Kuesioner
                            </a>
                        </li>
                    </ul>
                </li> --}}

                <li class="nav-item">
                    <a href="{{ url('ringkasan-eksekutif') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Ringkasan Eksekutif
                    </a>
                </li>
            </ul>

            @elseif(\Auth::user()->level_id == 3 || \Auth::user()->level->level == "Admin Pusat")
            <ul id="sidebarnav">
                <li class="nav-item">
                    <a href="{{ url('beranda') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/home.svg') }}" alt="">
                        </div>
                        Beranda
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('sekolah') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/school.svg') }}" alt="">
                        </div>
                        Sekolah
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('kalender') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/event.svg') }}" alt="">
                        </div>
                        Kalender Event
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Dokumen
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('dokumen-materi') }}" class="nav-link">
                                Materi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('dokumen-lainnya') }}" class="nav-link">
                                Informasi Lainnya
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Laporan
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('laporan-tot') }}" class="nav-link">
                                TOT di Pusat
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-advokasi') }}" class="nav-link">
                                Advokasi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-bimtek') }}" class="nav-link">
                                Bimtek PJAS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-pelatihan') }}" class="nav-link">
                                Pelatihan PBKP - KS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-sertifikat') }}" class="nav-link">
                                Sertifikat PBKP - KS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-audit') }}" class="nav-link">
                                Audit PBKP - KS
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ url('laporan-sampling') }}" class="nav-link">
                                Sampling PJAS & Operasional Mobling
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="{{ url('laporan-paket') }}" class="nav-link">
                                Paket Edukasi
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ url('laporan-monitoring') }}" class="nav-link">
                                Monitoring dan Evaluasi
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="{{ url('laporan-akhir') }}" class="nav-link">
                                Laporan Akhir
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/survey.svg') }}" alt="">
                        </div>
                        Survey
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('survey-online') }}" class="nav-link">
                                Online
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('survey-kuesioner') }}" class="nav-link">
                                Kuesioner
                            </a>
                        </li>
                    </ul>
                </li> --}}

                <li class="nav-item">
                    <a href="{{ url('ringkasan-eksekutif') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Ringkasan Eksekutif
                    </a>
                </li>
            </ul>
        @elseif(\Auth::user()->level_id == 2)
            <ul id="sidebarnav">
                <li class="nav-item">
                    <a href="{{ url('beranda') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/home.svg') }}" alt="">
                        </div>
                        Beranda
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('sekolah') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/school.svg') }}" alt="">
                        </div>
                        Sekolah
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('kalender') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/event.svg') }}" alt="">
                        </div>
                        Kalender Event
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Dokumen
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('dokumen-materi') }}" class="nav-link">
                                Materi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('dokumen-lainnya') }}" class="nav-link">
                                Informasi Lainnya
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Laporan
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('laporan-tot') }}" class="nav-link">
                                TOT di Pusat
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-advokasi') }}" class="nav-link">
                                Advokasi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-bimtek') }}" class="nav-link">
                                Bimtek PJAS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-pelatihan') }}" class="nav-link">
                                Pelatihan PBKP - KS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-sertifikat') }}" class="nav-link">
                                Sertifikat PBKP - KS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('laporan-audit') }}" class="nav-link">
                                Audit PBKP - KS
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ url('laporan-sampling') }}" class="nav-link">
                                Sampling PJAS & Operasional Mobling
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="{{ url('laporan-paket') }}" class="nav-link">
                                Paket Edukasi
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ url('laporan-monitoring') }}" class="nav-link">
                                Monitoring dan Evaluasi
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="{{ url('laporan-akhir') }}" class="nav-link">
                                Laporan Akhir
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="nav-item">
                    <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/survey.svg') }}" alt="">
                        </div>
                        Survey
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="nav-item">
                            <a href="{{ url('survey-online') }}" class="nav-link">
                                Online
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('survey-kuesioner') }}" class="nav-link">
                                Kuesioner
                            </a>
                        </li>
                    </ul>
                </li> --}}

                <li class="nav-item">
                    <a href="{{ url('ringkasan-eksekutif') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Ringkasan Eksekutif
                    </a>
                </li>
            </ul>
        @else
            <ul id="sidebarnav">
                <li class="nav-item">
                    <a href="{{ url('beranda') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/home.svg') }}" alt="">
                        </div>
                        Beranda
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('sekolah') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('/img/icons/sidebar/school.svg') }}" alt="">
                        </div>
                        Sekolah
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('ringkasan-eksekutif') }}" class="nav-link">
                        <div class="icon">
                            <img src="{{ asset('img/icons/sidebar/document.svg') }}" alt="">
                        </div>
                        Ringkasan Eksekutif
                    </a>
                </li>
            </ul>
        @endif
    </nav>
</aside>