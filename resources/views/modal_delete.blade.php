<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog hide" role="document" aria-hidden="true">
        <form id="formDelete" method="POST">
            @csrf
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="modal-body text-center">
                    <h6 class="font-weight-bold">
                        Anda yakin akan menghapus data ini ?
                    </h6>
                    <hr>
                    <button type="submit" class="btn btn-danger btn-modal-delete">Ya</button>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Tidak</button>
                </div>
            </div>
        </form>
    </div>
</div>
