@extends('layouts.root')

@section('title','Paket Edukasi')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Paket Edukasi &rsaquo; &rsaquo; Ubah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-paket/daftar-paket/ubah/'.$report_package->report_package_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Alamat</label>
                                        <textarea name="address" id="address" cols="30" rows="5" class="form-control" autofocus>{{ $report_package->address }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jumlah Sekolah yang diberikan/dikirimkan paket edukasi</label>
                                        <input type="number" class="form-control" name="school" value="{{ $report_package->school }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Periode pemberian/pengiriman paket edukasi</label>
                                        <input type="date" class="form-control" name="periode" required value="{{ $report_package->periode }}" placeholder="Masukan periode pemberian/pengiriman paket edukasi">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jumlah Paket</label>
                                        <input type="number" class="form-control" name="package" value="{{ $report_package->package }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jenis Paket</label>
                                        <input type="text" class="form-control" name="package_type" required value="{{ $report_package->package_type }}" placeholder="Masukan jenis paket">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-paket/daftar-paket/'.$report_package->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection