@extends('layouts.root')

@section('title','Paket Edukasi')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Paket Edukasi &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-paket/tambah/'.$province_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Laporan</label>
                                        <input type="text" class="form-control" name="name" autofocus required placeholder="Masukan nama laporan">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Tanggal Pelaksanaan</label>
                                        <input type="date" class="form-control" name="date" required placeholder="Masukan tanggal laporan">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Tempat Pelaksanaan Kegiatan</label>
                                        <textarea name="description" id="description" class="form-control"  cols="30" rows="5" placeholder="Masukan deskripsi laporan"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-paket') }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection