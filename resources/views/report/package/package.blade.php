@extends('layouts.root')

@section('title','Paket Edukasi')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-paket/detil/'. $report->province_id) }}">Paket Edukasi</a> &rsaquo; Daftar Paket
                                            </h6>
                                        </div>
                                    </div>

                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                {{--  <div class="col-6">
                                                    <a href="{{ url('laporan-paket/import-narasumber/'.$report->report_id) }}" class="btn btn-block btn-info btn-sm">Unggah Dari Excel</a>
                                                </div>    --}}
                                                <div class="col-12">
                                                    <a href="{{ url('laporan-paket/daftar-paket/tambah/'.$report->report_id) }}" class="btn btn-block btn-success btn-sm">+ &nbsp; Paket Edukasi</a>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th width="5%">No</th>
                                            <th>Nama Balai</th>
                                            <th>Alamat</th>
                                            <th>Provinsi</th>
                                            <th>Jumlah Sekolah yang diberikan/dikirimkan paket edukasi</th>
                                            <th>Periode pemberian/pengiriman paket edukasi</th>
                                            <th>Jumlah Paket</th>
                                            <th>Jenis Paket</th>
                                            <th width="15%"></th>
                                        </thead>
                                        <tbody>
                                            @if(sizeof($report->package) > 0)
                                                @foreach($report->package as $key => $item)
                                                    <tr>
                                                        <td>{{ $key+1 }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->address }}</td>
                                                        <td>{{ isset($report->province) ? $report->province->province : "" }}</td>
                                                        <td>{{ $item->school }} Sekolah</td>
                                                        <td>{{ date('F Y', strtotime($item->periode)) }}</td>
                                                        <td>{{ $item->package }} Paket</td>
                                                        <td>{{ $item->package_type }}</td>
                                                        <td>
                                                            <a href="{{ url('laporan-paket/daftar-paket/ubah/'. $item->report_package_id) }}" class="btn btn-info btn-sm" title="Ubah" data-toggle="tooltip" ><i class="icon fa fa-pen"></i></a>
                                                            <button class="btn btn-danger btn-delete btn-sm" title="Hapus" data-toggle="tooltip" data-action="{{ url('laporan-paket/hapus-daftar-paket/'. $item->report_package_id) }}"><i class="icon fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="9" class="text-center">Data Paket Edukasi Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush