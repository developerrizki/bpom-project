@extends('layouts.root')

@section('title','Laporan Bimtek Daftar Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo; 
                                                <a href="{{ url('laporan-bimtek/show/'. $report->report_id) }}">Detail Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/daftar-sekolah/'. $report->report_id) }}">Daftar Sekolah</a>  &rsaquo;
                                                Ubah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-bimtek/daftar-sekolah/ubah/'.$report_school->report_school_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Sekolah</label>
                                        <select name="school_id" id="school_id" class="form-control custom-select">
                                            <option value="">Pilih Sekolah</option>
                                            @foreach($school as $key => $item)
                                                <option value="{{ $item->school_id }}" {{ $report_school->school_id == $item->school_id ? "selected" : "" }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kantin</label>
                                        {{-- <input type="text" class="form-control" name="canteen" value="{{ $report_school->canteen }}" required placeholder="Masukan keterangan kantin"> --}}
                                        <select name="canteen" id="canteen" class="form-control">
                                            <option value="">Pilih Keterangan Kantin</option>
                                            <option value="Ada" {{ $report_school->canteen == "Ada" ? "selected" : "" }}>Ada</option>
                                            <option value="Tidak Ada" {{ $report_school->canteen == "Tidak Ada" ? "selected" : "" }}>Tidak Ada</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kondisi</label>
                                        <input type="text" class="form-control" name="condition" value="{{ $report_school->condition }}" required placeholder="Masukan keterangan kondisi">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-bimtek/daftar-sekolah/'.$report_school->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection