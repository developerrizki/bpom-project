@extends('layouts.root')

@section('title','Laporan Bimtek Peserta LINSEK')

@push('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo;  
                                                <a href="{{ url('laporan-bimtek/show/'. $report->report_id) }}">Detail Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/linsek/'. $report->report_id) }}">Peserta LINSEK</a>  &rsaquo;
                                                Tambah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-bimtek/linsek/tambah/'.$report_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Instansi</label>
                                        <input type="text" class="form-control" name="name" autofocus required placeholder="Masukan nama instansi">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Provinsi</label>
                                        <select name="province" id="province" class="form-control js-select-2 custom-select" disabled>
                                            <option value="">Pilih Provinsi</option>
                                            @foreach($province as $key => $item)
                                                <option value="{{ $item->province_id }}" {{ $report->province_id == $item->province_id ? "selected" : "" }}>{{ $item->province }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kota / Kabupaten</label>
                                        <select name="city_id" id="city_id" class="form-control custom-select">
                                            <option value="">Pilih Kota / Kabupaten</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jumlah Peserta</label>
                                        <input type="number" class="form-control" name="participant" required placeholder="Masukan instansi">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-bimtek/linsek/'.$report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.js-select-2').select2();
        });
    </script>
    <script>
        $(document).ready(function(){
            var province_id = '{{ $report->province_id }}';
            var url = '{{ url("getCity") }}';

            $.get(url,{province_id : province_id}, function(){
                console.log('loading ...');
            }).done(function(data){
                $('#city_id').html(data);
            }).fail(function(){
                alert('Gagal mengambil data Kota/Kabupaten');
            });
        })
    </script>
@endpush