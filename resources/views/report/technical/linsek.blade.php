@extends('layouts.root')

@section('title', 'Laporan Bimtek Peserta LINSEK')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (Session::has('status'))
                            @if (Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i>
                                        Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!
                                    </h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a> &rsaquo;
                                                <a
                                                    href="{{ url('laporan-bimtek/detil/' . $report->province_id) }}">{{ $report->province->province }}</a>
                                                &rsaquo;
                                                <a href="{{ url('laporan-bimtek/show/' . $report->report_id) }}">Detail
                                                    Bimtek</a> &rsaquo;
                                                Peserta LINSEK
                                            </h6>
                                        </div>
                                    </div>

                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-6">
                                                    <a href="#modal-import-linsek" data-toggle="modal"
                                                        class="btn btn-info btn-sm">Unggah Dari Excel</a>
                                                </div>
                                                <div class="col-6">
                                                    <a href="{{ url('laporan-bimtek/linsek/tambah/' . $report->report_id) }}"
                                                        class="btn btn-success btn-sm">+ &nbsp; Peserta LINSEK</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th width="5%">No</th>
                                            <th>Nama Instansi</th>
                                            <th>Kota/Kabupaten</th>
                                            <th>Jumlah Peserta</th>
                                            <th width="10%"></th>
                                        </thead>
                                        <tbody>
                                            @php $total_participant = 0; @endphp
                                            @if (sizeof($report->linsek) > 0)
                                                @foreach ($report->linsek as $key => $item)
                                                    <tr>
                                                        <td width="5%">{{ $key + 1 }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->city ? $item->city->city : '' }}</td>
                                                        <td>{{ $item->participant }}</td>
                                                        <td width="15%">
                                                            <a href="{{ url('laporan-bimtek/linsek/ubah/' . $item->report_linsek_id) }}"
                                                                class="btn btn-info btn-sm" title="Ubah"
                                                                data-toggle="tooltip"><i class="icon fa fa-pen"></i></a>
                                                            <button class="btn btn-danger btn-delete btn-sm" title="Hapus"
                                                                data-toggle="tooltip"
                                                                data-action="{{ url('laporan-bimtek/hapus-linsek/' . $item->report_linsek_id) }}"><i
                                                                    class="icon fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>

                                                    @php $total_participant += $item->participant; @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Data Peserta pada Laporan Bimtek Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" class="text-primary">Total Peserta</th>
                                                <th colspan="2" class="text-primary">{{ $total_participant }} Orang</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Dokumen</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-12">
                                                    <a href="{{ url('laporan-bimtek/linsek/tambah-dokumen/' . $report->report_id) }}"
                                                        class="btn btn-block btn-info btn-sm">Unggah Lampiran</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                @if (sizeof($report->linsekFiles) > 0)
                                    <div class="document-grid">
                                        @foreach ($report->linsekFiles as $key => $item)
                                            <div class="col-sm-3 text-center">
                                                <a href="{{ Storage::url('dokumen-linsek/' . $item->file) }}"
                                                    title="{{ $item->file }}" target="_blank" class="item">
                                                    <div class="icon">
                                                        <img src="{{ asset('img/icons/folder.svg') }}" alt="">
                                                    </div>
                                                    <p>{{ strlen($item->file) > 15 ? substr(ucwords($item->file), 0, 20) . ' ...' : ucwords($item->file) }}
                                                    </p>
                                                </a>
                                                <button class="btn btn-danger btn-delete btn-sm mt-3"
                                                    data-action="{{ url('laporan-bimtek/linsek/hapus-dokumen/' . $item->linsek_file_id) }}">
                                                    <i class="icon fa fa-trash"></i> &nbsp; Hapus Dokumen
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <h5 class="text-center">Dokumen belum tersedia.</h5>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->

    <div id="modal-import-linsek" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="modal-body">
                    <div class="title text-center">
                        <h6>
                            Import Data Peserta Linsek
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('laporan-bimtek/linsek/import/' . $report->report_id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" style="margin-bottom: 20px">
                                    <input type="file" name="file" class="form-control" required
                                        style="margin-bottom: 10px">
                                    <a href="{{ asset('excels/import-data-linsek.xlsx') }}" class="text-danger">Unduh
                                        format excel terlebih dahulu. sebelum melakukan import data (Klik Disini)</a>
                                </div>
                                <div class="form-group" style="margin-bottom: 20px">
                                    <button type="submit" class="btn btn-sm btn-success">Import</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Batal</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function() {
            var action = $(this).data('action');

            $('#modalDelete').modal('show');

            $('#formDelete').attr('action', action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
