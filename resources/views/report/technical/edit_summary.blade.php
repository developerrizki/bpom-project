@extends('layouts.root')

@section('title','Laporan Bimtek Provinsi')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo; 
                                                <a href="{{ url('laporan-bimtek/show/'. $report->report_id) }}">Detail Bimtek</a>  &rsaquo; 
                                                Ubah Summary
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-bimtek/ubah-summary/'.$summary->report_summary_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Hasil Diskusi / Kesepakatan</label>
                                        <textarea name="summary" id="summary" class="form-control"  cols="30" rows="5" placeholder="Masukan Hasil Diskusi / Kesepakatan">{{ $summary->summary }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-bimtek/show/'.$summary->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection