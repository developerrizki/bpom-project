@extends('layouts.root')

@section('title','Laporan Bimtek LINSEK')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo; 
                                                <a href="{{ url('laporan-bimtek/show/'. $report->report_id) }}">Detail Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/linsek/'. $report->report_id) }}">Peserta LINSEK</a>  &rsaquo;
                                                Ubah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-bimtek/linsek/ubah/'.$linsek->report_linsek_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Instansi</label>
                                        <input type="text" class="form-control" name="name" autofocus required value="{{ $linsek->name }}" placeholder="Masukan nama instansi">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Provinsi</label>
                                        <select name="province" id="province" class="form-control custom-select" disabled>
                                            <option value="">Pilih Provinsi</option>
                                            @foreach($province as $key => $item)
                                                <option value="{{ $item->province_id }}" {{ $linsek->city->province_id == $item->province_id ? "selected" : "" }}>{{ $item->province }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kota / Kabupaten</label>
                                        <select name="city_id" id="city_id" class="form-control custom-select">
                                            <option value="">Pilih Kota / Kabupaten</option>
                                            @foreach($city as $key => $item)
                                                <option value="{{ $item->city_id }}" {{ $linsek->city_id == $item->city_id ? "selected" : "" }}>{{ $item->city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jumlah Peserta</label>
                                        <input type="number" class="form-control" name="participant" required value="{{ $linsek->participant }}" placeholder="Masukan instansi">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-bimtek/linsek/'.$linsek->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection