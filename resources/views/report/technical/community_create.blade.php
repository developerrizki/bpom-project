@extends('layouts.root')

@section('title','Laporan Bimtek Komunitas Sekolah')

@push('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo;  
                                                <a href="{{ url('laporan-bimtek/show/'. $report->report_id) }}">Detail Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/komunitas/'. $report->report_id) }}">Komunitas Sekolah</a>  &rsaquo;
                                                Tambah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-bimtek/komunitas/tambah/'.$report_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Sekolah</label>
                                        <select name="school_id" id="school_id" class="form-control js-select-2 custom-select">
                                            <option value="">Pilih Sekolah</option>
                                            @foreach($school as $key => $item)
                                                <option value="{{ $item->school_id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Komunitas Sekolah</label>
                                        <textarea name="school_community" id="school_community" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jumlah</label>
                                        <input type="number" class="form-control" name="sum_community" required placeholder="Masukan jumlah">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-bimtek/komunitas/'.$report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.js-select-2').select2();
        });
    </script>
@endpush