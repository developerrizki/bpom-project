@extends('layouts.root')

@section('title','Laporan Bimtek')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div> <br>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo; 
                                                <a href="{{ url('laporan-bimtek/show/'. $report->report_id) }}">Detail Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/foto-kegiatan/'. $report->report_id) }}">Foto Kegiatan</a>  &rsaquo;
                                                Tambah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-bimtek/foto-kegiatan/tambah/'.$report->report_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">File</label>
                                        <input type="file" class="form-control" name="file" required><br>
                                        <span class="text-danger">File Ext : .jpeg | .jpg | .png | .svg | .bmp</span> <br>
                                        <span class="text-danger">Maks. Ukuran File : 10 mb</span> <br>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-bimtek/foto-kegiatan/'.$report->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection