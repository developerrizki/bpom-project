@extends('layouts.root')

@section('title','Laporan Bimtek')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>                                                
                                                <a href="{{ url('laporan-bimtek') }}">Laporan Bimtek</a>  &rsaquo;
                                                <a href="{{ url('laporan-bimtek/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo; 
                                                <a href="{{ url('laporan-bimtek/show/'. $report->report_id) }}">Detail Bimtek</a>  &rsaquo; 
                                                Ubah Narasumber
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-bimtek/ubah-narasumber/'.$interviewees->report_interview_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Narasumber</label>
                                        <input type="text" class="form-control" name="name" autofocus required value="{{ $interviewees->name }}" placeholder="Masukan nama narasumber">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Instansi</label>
                                        <input type="text" class="form-control" name="agency" required value="{{ $interviewees->agency }}" placeholder="Masukan nama instansi">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jabatan</label>
                                        <input type="text" class="form-control" name="position" required value="{{ $interviewees->position }}" placeholder="Masukan nama jabatan">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Materi yang Disampaikan</label>
                                        <textarea name="theory" id="theory" class="form-control"  cols="30" rows="5" placeholder="Masukan judul materi yang disampaikan">{{ $interviewees->theory }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-bimtek/show/'.$interviewees->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection