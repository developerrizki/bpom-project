@extends('layouts.root')

@section('title','Laporan Pelatihan Daftar Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Laporan Pelatihan &rsaquo; Daftar Sekolah &rsaquo; Ubah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-pelatihan/daftar-sekolah/ubah/'.$report_school->report_school_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Sekolah</label>
                                        <select name="school_id" id="school_id" class="form-control custom-select">
                                            <option value="">Pilih Sekolah</option>
                                            @foreach($school as $key => $item)
                                                <option value="{{ $item->school_id }}" {{ $report_school->school_id == $item->school_id ? "selected" : "" }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kantin</label>
                                        <input type="text" class="form-control" name="canteen" value="{{ $report_school->canteen }}" required placeholder="Masukan keterangan kantin">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kondisi</label>
                                        <input type="text" class="form-control" name="condition" value="{{ $report_school->condition }}" required placeholder="Masukan keterangan kondisi">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-pelatihan/daftar-sekolah/'.$report_school->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection