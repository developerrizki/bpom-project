@extends('layouts.root')

@section('title', 'Laporan Pelatihan Peserta LINSEK')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (Session::has('status'))
                            @if (Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i>
                                        Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!
                                    </h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Pelatihan &rsaquo; Peserta LINSEK</h6>
                                        </div>
                                    </div>

                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                {{-- <div class="col-6">
                                                    <a href="{{ url('laporan-pelatigan/import-narasumber/'.$report->report_id) }}" class="btn btn-block btn-info btn-sm">Unggah Dari Excel</a>
                                                </div> --}}
                                                <div class="col-12">
                                                    <a href="{{ url('laporan-pelatigan/linsek/tambah/' . $report->report_id) }}"
                                                        class="btn btn-block btn-success btn-sm">+ &nbsp; Peserta LINSEK</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th width="5%">No</th>
                                            <th>Nama Instansi</th>
                                            <th>Kota/Kabupaten</th>
                                            <th>Jumlah Peserta</th>
                                            <th width="10%"></th>
                                        </thead>
                                        <tbody>
                                            @if (sizeof($report->linsek) > 0)
                                                @php $total_participant = 0; @endphp
                                                @foreach ($report->linsek as $key => $item)
                                                    <tr>
                                                        <td width="5%">{{ $key + 1 }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->city->city }}</td>
                                                        <td>{{ $item->participant }}</td>
                                                        <td width="15%">
                                                            <a href="{{ url('laporan-pelatigan/linsek/ubah/' . $item->report_linsek_id) }}"
                                                                class="btn btn-info btn-sm" title="Ubah"
                                                                data-toggle="tooltip"><i class="icon fa fa-pen"></i></a>
                                                            <button class="btn btn-danger btn-delete btn-sm" title="Hapus"
                                                                data-toggle="tooltip"
                                                                data-action="{{ url('laporan-pelatigan/hapus-linsek/' . $item->report_linsek_id) }}"><i
                                                                    class="icon fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>

                                                    @php $total_participant += $item->participant; @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Data Peserta pada Laporan Bimtek Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" class="text-primary">Total Peserta</th>
                                                <th colspan="2" class="text-primary">{{ $total_participant }} Orang</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Dokumen</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-12">
                                                    <a href="{{ url('laporan-pelatigan/linsek/tambah-dokumen/' . $report->report_id) }}"
                                                        class="btn btn-block btn-info btn-sm">Unggah Lampiran</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                @if (sizeof($report->linsekFiles) > 0)
                                    <div class="document-grid">
                                        @foreach ($report->linsekFiles as $key => $item)
                                            <a href="{{ Storage::url('dokumen-daftar-hadir/' . $item->file) }}"
                                                title="{{ $item->file }}" target="_blank" class="item">
                                                <div class="icon">
                                                    <img src="{{ asset('img/icons/folder.svg') }}" alt="">
                                                </div>
                                                <p>{{ strlen($item->file) > 15 ? substr(ucwords($item->file), 0, 20) . ' ...' : ucwords($item->file) }}
                                                </p>
                                            </a>
                                        @endforeach
                                    </div>
                                @else
                                    <h5 class="text-center">Dokumen belum tersedia.</h5>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function() {
            var action = $(this).data('action');

            $('#modalDelete').modal('show');

            $('#formDelete').attr('action', action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
