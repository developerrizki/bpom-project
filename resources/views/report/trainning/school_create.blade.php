@extends('layouts.root')

@section('title','Laporan Pelatihan Daftar Sekolah')

@push('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Laporan Pelatihan &rsaquo; Daftar Sekolah &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-pelatihan/daftar-sekolah/tambah/'.$report_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Sekolah</label>
                                        <select name="school_id" id="school_id" class="form-control custom-select js-select-2">
                                            <option value="">Pilih Sekolah</option>
                                            @foreach($school as $key => $item)
                                                <option value="{{ $item->school_id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kantin</label>
                                        <input type="text" class="form-control" name="canteen" required placeholder="Masukan keterangan kantin">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kondisi</label>
                                        <input type="text" class="form-control" name="condition" required placeholder="Masukan keterangan kondisi">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-pelatihan/daftar-sekolah/'.$report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.js-select-2').select2();
        });
    </script>
@endpush