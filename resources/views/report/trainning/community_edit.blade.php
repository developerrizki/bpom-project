@extends('layouts.root')

@section('title','Laporan Pelatihan Komunitas Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-pelatihan') }}">Laporan Pelatihan PBKP-KS</a>
                                                &rsaquo; 
                                                <a href="{{ url('laporan-pelatihan/detil/'.$report->province_id) }}">{{ $report->province->province }}</a>
                                                &rsaquo; 
                                                <a href="{{ url('laporan-pelatihan/show/'.$report->report_id) }}">Detail Pelatihan PKB-KS</a>
                                                &rsaquo; 
                                                <a href="{{ URL::previous() }}">Komunitas Sekolah</a>
                                                &rsaquo; 
                                                Ubah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-pelatihan/komunitas/ubah/'.$community->report_community_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Sekolah</label>
                                        <select name="school_id" id="school_id" class="form-control custom-select">
                                            <option value="">Pilih Sekolah</option>
                                            @foreach($school as $key => $item)
                                                <option value="{{ $item->school_id }}" {{ $community->school_id == $item->school_id ? "selected" : "" }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Komunitas Sekolah</label>
                                        <input type="text" class="form-control" name="school_community" value="{{ $community->school_community }}" required placeholder="Masukan keterangan kantin">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Jumlah</label>
                                        <input type="text" class="form-control" name="sum_community" value="{{ $community->sum_community }}" required placeholder="Masukan keterangan kondisi">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-pelatihan/komunitas/'.$community->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection