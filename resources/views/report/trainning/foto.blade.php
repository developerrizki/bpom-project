@extends('layouts.root')

@section('title','Laporan Pelatihan - Foto Kegiatan')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-pelatihan') }}">Laporan Pelatihan PBKP-KS</a>
                                                &rsaquo; 
                                                <a href="{{ url('laporan-pelatihan/detil/'.$report->province_id) }}">{{ $report->province->province }}</a>
                                                &rsaquo; 
                                                <a href="{{ url('laporan-pelatihan/show/'.$report->report_id) }}">Detail Pelatihan PKB-KS</a>
                                                &rsaquo; 
                                                Foto Kegiatan
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-12">
                                                    <a href="{{ url('laporan-pelatihan/foto-kegiatan/tambah/'.$report->report_id) }}" class="btn btn-block btn-success btn-sm">+ &nbsp; Foto Kegiatan</a>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                @if(sizeof($report->galleries) > 0)
                                <div class="row">
                                    @foreach($report->galleries as $key => $item)
                                        <div class="col-lg-4 col-md-6 mb-30 text-center">
                                            <a href="#" data-toggle="modal">
                                                <img src="{{ Storage::url('foto-kegiatan/'.$item->file) }}" class="img-fluid" alt="">
                                            </a>

                                            <button class="btn btn-danger btn-delete btn-sm mt-3" data-action="{{ url('laporan-pelatihan/foto-kegiatan/hapus/'. $item->report_gallery_id) }}">
                                                <i class="icon fa fa-trash"></i> &nbsp; Hapus Foto
                                            </button>
                                        </div>
                                    @endforeach
                                </div>
                                @else
                                    <h5 class="text-center">Foto kegiatan belum tersedia.</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Data Sekolah');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush