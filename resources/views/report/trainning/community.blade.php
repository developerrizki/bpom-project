@extends('layouts.root')

@section('title','Laporan Pelatihan Komunitas Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-pelatihan') }}">Laporan Pelatihan PBKP-KS</a>
                                                &rsaquo; 
                                                <a href="{{ url('laporan-pelatihan/detil/'.$report->province_id) }}">{{ $report->province->province }}</a>
                                                &rsaquo; 
                                                <a href="{{ url('laporan-pelatihan/show/'.$report->report_id) }}">Detail</a>
                                                &rsaquo; 
                                                Komunitas Sekolah
                                            </h6>
                                        </div>
                                    </div>

                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                 <div class="col-6">
                                                    <a href="#modal-import-komunitas" data-toggle="modal" class="btn btn-info btn-sm">Unggah Dari Excel</a>
                                                </div>   
                                                <div class="col-6">
                                                    <a href="{{ url('laporan-pelatihan/komunitas/tambah/'.$report->report_id) }}" class="btn btn-success btn-sm">+ &nbsp; Komunitas Sekolah</a>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th width="5%">No</th>
                                            <th>Nama Sekolah</th>
                                            <th>Kota/Kabupaten</th>
                                            <th>Komunitas Sekolah</th>
                                            <th>Jumlah</th>
                                            <th width="15%"></th>
                                        </thead>
                                        <tbody>
                                            @if(sizeof($school) > 0)
                                                @foreach($school as $key => $item)
                                                    <tr>
                                                        <td>{{ ($school->currentpage()-1) * $school->perpage() + $key+1 }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->city->city }}</td>
                                                        <td>{{ $item->community->school_community }}</td>
                                                        <td>{{ $item->community->sum_community }}</td>
                                                        <td>
                                                            <a href="{{ url('laporan-pelatihan/komunitas/ubah/'. $item->community->report_community_id) }}" class="btn btn-info btn-sm" title="Ubah" data-toggle="tooltip" ><i class="icon fa fa-pen"></i></a>
                                                            <button class="btn btn-danger btn-delete btn-sm" title="Hapus" data-toggle="tooltip" data-action="{{ url('laporan-pelatihan/hapus-komunitas/'. $item->community->report_community_id) }}"><i class="icon fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6" class="text-center">Data Sekolah Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    {!! $school->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->

    <div id="modal-import-komunitas" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="modal-body">
                    <div class="title text-center">
                        <h6>
                            Import Data Peserta Komunitas Sekolah
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('laporan-pelatihan/komunitas/import/'. $report->report_id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" style="margin-bottom: 20px">
                                    <input type="file" name="file" class="form-control" required style="margin-bottom: 10px">
                                    <a href="{{ asset('excels/import-data-komunitas.xlsx') }}" class="text-danger">Unduh format excel terlebih dahulu. sebelum melakukan import data (Klik Disini)</a>
                                </div>
                                <div class="form-group" style="margin-bottom: 20px">
                                    <button type="submit" class="btn btn-sm btn-success">Import</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Batal</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush