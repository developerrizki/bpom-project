@extends('layouts.root')

@section('title','Audit PBKP-KS')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-audit') }}">Audit PBKP-KS</a>  &rsaquo;
                                                <a href="{{ url('laporan-audit/detil/'. $report->province_id) }}">Laporan Audit PBKP-KS</a>  &rsaquo; 
                                                <a href="{{ url('laporan-audit/daftar-sekolah/'. $report->report_id) }}">Detail Audit PBKP-KS</a>  &rsaquo;
                                                <a href="{{ url('laporan-audit/foto-audit/'. $report->report_id) }}">Foto Audit</a>  &rsaquo;
                                                Tambah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-audit/foto-audit/tambah/'.$report->report_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">File</label>
                                        <input type="file" class="form-control" name="file" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-audit/foto-audit/'.$report->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection