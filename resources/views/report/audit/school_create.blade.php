@extends('layouts.root')

@section('title','Audit PBKP-KS Daftar Sekolah')

@push('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-audit') }}">Audit PBKP-KS</a>  &rsaquo;
                                                <a href="{{ url('laporan-audit/detil/'. $report->province_id) }}">Laporan Audit PBKP-KS</a>  &rsaquo; 
                                                <a href="{{ url('laporan-audit/daftar-sekolah/'. $report->report_id) }}">Daftar Sekolah</a>  &rsaquo;
                                                Tambah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-audit/daftar-sekolah/tambah/'.$report_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Sekolah</label>
                                        <select name="school_id" id="school_id" class="form-control js-select-2 custom-select" required>
                                            <option value="">Pilih Sekolah</option>
                                            @foreach($school as $key => $item)
                                                <option value="{{ $item->school_id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kepala Sekolah</label>
                                        <input type="text" class="form-control" name="headmaster" required placeholder="Masukan keterangan Kepala Sekolah">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Alamat</label>
                                        <textarea name="address" id="address" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Nilai Audit Sarana (%)</label>
                                        <input type="text" class="form-control" name="score" required placeholder="Masukan keterangan Nilai Audit Sarana">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Temuan Ketidaksesuaian</label>
                                        <textarea name="incompatibility" id="incompatibility" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-audit/daftar-sekolah/'.$report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.js-select-2').select2();
        });
    </script>
@endpush