@extends('layouts.root')

@section('title','Audit PBKP-KS  Daftar Sekolah')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-audit') }}">Audit PBKP-KS</a>  &rsaquo;
                                                <a href="{{ url('laporan-audit/detil/'. $report->province_id) }}">Laporan Audit PBKP-KS</a>  &rsaquo; 
                                                <a href="{{ url('laporan-audit/daftar-sekolah/'. $report->report_id) }}">Daftar Sekolah</a>  &rsaquo;
                                                Ubah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-audit/daftar-sekolah/ubah/'.$report_school->report_audit_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Sekolah</label>
                                        <select name="school_id" id="school_id" class="form-control custom-select">
                                            <option value="">Pilih Sekolah</option>
                                            @foreach($school as $key => $item)
                                                <option value="{{ $item->school_id }}" {{ $report_school->school_id == $item->school_id ? "selected" : "" }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kepala Sekolah</label>
                                        <input type="text" class="form-control" name="headmaster" value="{{ $report_school->headmaster }}" required placeholder="Masukan keterangan Kepala Sekolah">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Alamat</label>
                                        <textarea name="address" id="address" cols="30" rows="5" class="form-control">{{ $report_school->address }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Nilai Audit Sarana (%)</label>
                                        <input type="text" class="form-control" name="score" value="{{ $report_school->score }}" required placeholder="Masukan keterangan Nilai Audit Sarana">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Temuan Ketidaksesuaian</label>
                                        <textarea name="incompatibility" id="incompatibility" cols="30" rows="5" class="form-control">{{ $report_school->incompatibility }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-audit/daftar-sekolah/'.$report_school->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection