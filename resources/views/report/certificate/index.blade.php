@extends('layouts.root')

@section('title','Sertifikat PBKP-KS Provinsi')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Pencarian Provinsi</h6>
                                        </div>
                                    </div>
                                </div>
                                <form action="{{ url('laporan-sertifikat') }}" class="form-horizontal" method="GET">
                                    @csrf
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="province" placeholder="Masukan Data Provinsi Yang Dicari" value="{{ Request::has('province') ? Request::get('province') : "" }}">
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info btn-save btn-sm btn-block">Cari</button>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <a href="{{ url('laporan-sertifikat') }}" class="btn btn-danger btn-default btn-sm btn-block">Batal Pencarian</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Sertifikat PBKP-KS</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th>No</th>
                                            <th>Provinsi</th>
                                            <th>Tahun Perolehan Sertifikat PBKP-KS</th>
                                            <th>Jumlah Sekolah Penerima PBKP-KS</th>
                                        </thead>
                                        <tbody>
                                            @if(sizeof($school) > 0)
                                                @php $total = 0; @endphp
                                                @foreach($school as $key => $item)
                                                    <tr>
                                                        <td width="5%">{{ $key+1 }}</td>
                                                        <td>
                                                            <a href="{{ url('sekolah/detil/'.$item->id_province.'?pbkp_ks=1&year_pbkp_ks='.$item->year_pbkp_ks) }}" target="_blank">{{ $item->name_province }}</a>
                                                        </td>
                                                        <td>{{ $item->year_pbkp_ks }}</td>
                                                        <td>{{ $item->sum_school }}</td>
                                                        {{-- <td width="10%">
                                                            <a href="{{ url('laporan-sertifikat/detil/'. $item->province_id) }}" class="btn btn-primary btn-sm">Lihat Laporan</a>
                                                        </td> --}}
                                                    </tr>
                                                    @php $total += $item->sum_school; @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Data Provinsi Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" class="text-right">
                                                    <strong>Total Sekolah Penerima PBKP-KS</strong>
                                                </td>
                                                <td>
                                                    <strong>{{ $total }}</strong>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection