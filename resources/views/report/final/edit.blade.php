@extends('layouts.root')

@section('title','Akhir')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Data Akhir &rsaquo; Ubah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-akhir/ubah/'.$report->report_final_id) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label>Judul Laporan</label>
                                        <input type="text" name="title" class="form-control" value="{{ $report->title }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Gambar Halaman Depan</label>
                                        <div class="custom-file">
                                            <input type="file" name="file" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div> <br> <br>
                                        <a href="{{ Storage::url('laporan-akhir/'. $report->file ) }}">{{ $report->file }}</a>
                                    </div>
                                    <div class="form-group">
                                        <label>Ringkasan Eksekutif</label>
                                        <input type="text" name="summary" class="form-control" value="{{ $report->summary }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Kata Pengantar</label>
                                        <textarea class="form-control" name="foreword" rows="10">{{ $report->foreword }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Pendahuluan</label>
                                        <textarea class="form-control" name="preliminary" rows="10">{{ $report->preliminary }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Tujuan</label>
                                        <textarea class="form-control" name="destination" rows="10">{{ $report->destination }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Target dan Capaian</label>
                                        <textarea class="form-control" name="target" rows="10">{{ $report->target }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Kendala dan Strategi</label>
                                        <textarea class="form-control" name="obstacle" rows="10">{{ $report->obstacle }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Rekomendasi dan Tindak Lanjut</label>
                                        <textarea class="form-control" name="recommendation" rows="10">{{ $report->recommendation }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Penutup</label>
                                        <textarea class="form-control" name="closing" rows="10">{{ $report->closing }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-akhir') }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script>
        $('#province').bind('change', function(){
            var province_id = $(this).val();
            var url = '{{ url("getCity") }}';

            $.get(url,{province_id : province_id}, function(){
                console.log('loading ...');
            }).done(function(data){
                $('#city').html(data);
                $('#city').attr('disabled',false);
            }).fail(function(){
                alert('Gagal mengambil data Kota/Kabupaten');
            });
        })
    </script>
@endpush