@extends('layouts.root')

@section('title','Laporan Advokasi Daftar Hadir')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-advokasi') }}">Laporan Advokasi</a>  &rsaquo;
                                                <a href="{{ url('laporan-advokasi/detil/'. $report->province_id) }}">{{ $report->province->province }}</a>  &rsaquo; 
                                                <a href="{{ url('laporan-advokasi/show/'. $report->report_id) }}">Detail Advokasi</a>  &rsaquo;
                                                <a href="{{ url('laporan-advokasi/daftar-hadir/'. $report->report_id) }}">Daftar Hadir</a>  &rsaquo;
                                                Tambah
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('laporan-advokasi/daftar-hadir/ubah/'.$attendees->report_attendance_id) }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama Narasumber</label>
                                        <input type="text" class="form-control" name="name" autofocus required value="{{ $attendees->name }}" placeholder="Masukan nama">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Instansi</label>
                                        <input type="text" class="form-control" name="agency" required value="{{ $attendees->agency }}" placeholder="Masukan instansi">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('laporan-advokasi/daftar-hadir/'.$attendees->report_id) }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection