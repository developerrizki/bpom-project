@extends('layouts.root')

@section('title', 'Laporan Advokasi Daftar Hadir')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (Session::has('status'))
                            @if (Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i>
                                        Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!
                                    </h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>
                                                <a href="{{ url('laporan-advokasi') }}">Laporan Advokasi</a> &rsaquo;
                                                <a
                                                    href="{{ url('laporan-advokasi/detil/' . $report->province_id) }}">{{ $report->province->province }}</a>
                                                &rsaquo;
                                                <a href="{{ url('laporan-advokasi/show/' . $report->report_id) }}">Detail
                                                    Advokasi</a> &rsaquo;
                                                Daftar Hadir
                                            </h6>
                                        </div>
                                    </div>

                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-sm-6">
                                                    <a href="#modal-import-absen" data-toggle="modal"
                                                        class="btn btn-info btn-sm">Unggah Dari Excel</a>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{ url('laporan-advokasi/daftar-hadir/tambah/' . $report->report_id) }}"
                                                        class="btn btn-success btn-sm">+ &nbsp; Daftar Hadir</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th width="5%">No</th>
                                            <th>Nama</th>
                                            <th>Instansi</th>
                                            <th width="10%"></th>
                                        </thead>
                                        <tbody>
                                            @if (sizeof($report->attendees) > 0)
                                                @foreach ($report->attendees as $key => $item)
                                                    <tr>
                                                        <td width="5%">{{ $key + 1 }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->agency }}</td>
                                                        <td width="15%">
                                                            <a href="{{ url('laporan-advokasi/daftar-hadir/ubah/' . $item->report_attendance_id) }}"
                                                                class="btn btn-info btn-sm" title="Ubah"
                                                                data-toggle="tooltip"><i class="icon fa fa-pen"></i></a>
                                                            <button class="btn btn-danger btn-delete btn-sm" title="Hapus"
                                                                data-toggle="tooltip"
                                                                data-action="{{ url('laporan-advokasi/hapus-daftar-hadir/' . $item->report_attendance_id) }}"><i
                                                                    class="icon fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Data Peserta pada Laporan Advokasi Tidak Tersedia.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" class="text-primary">Total Peserta</th>
                                                <th class="text-center text-primary">{{ sizeof($report->attendees) }}
                                                    Orang</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Dokumen</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-12">
                                                    <a href="{{ url('laporan-advokasi/daftar-hadir/tambah-dokumen/' . $report->report_id) }}"
                                                        class="btn btn-block btn-info btn-sm">Unggah Lampiran</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                @if (sizeof($report->attendanceFiles) > 0)
                                    <div class="document-grid">
                                        @foreach ($report->attendanceFiles as $key => $item)
                                            <div class="col-sm-3 text-center">
                                                <a href="{{ Storage::url('dokumen-daftar-hadir/' . $item->file) }}"
                                                    title="{{ $item->file }}" target="_blank" class="item">
                                                    <div class="icon">
                                                        <img src="{{ asset('img/icons/folder.svg') }}" alt="">
                                                    </div>
                                                    <p>{{ strlen($item->file) > 15 ? substr(ucwords($item->file), 0, 20) . ' ...' : ucwords($item->file) }}
                                                    </p>
                                                </a>
                                                <button class="btn btn-danger btn-delete btn-sm mt-3"
                                                    data-action="{{ url('laporan-advokasi/daftar-hadir/hapus-dokumen/' . $item->attendance_files_id) }}">
                                                    <i class="icon fa fa-trash"></i> &nbsp; Hapus Dokumen
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <h5 class="text-center">Dokumen belum tersedia.</h5>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->

    <div id="modal-import-absen" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="modal-body">
                    <div class="title text-center">
                        <h6>
                            Import Data Daftar Hadir
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('laporan-advokasi/daftar-hadir/import/' . $report->report_id) }}"
                                method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" style="margin-bottom: 20px">
                                    <input type="file" name="file" class="form-control" required
                                        style="margin-bottom: 10px">
                                    <a href="{{ asset('excels/import-data-absen.xlsx') }}" class="text-danger">Unduh
                                        format excel terlebih dahulu. sebelum melakukan import data (Klik Disini)</a>
                                </div>
                                <div class="form-group" style="margin-bottom: 20px">
                                    <button type="submit" class="btn btn-sm btn-success">Import</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Batal</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function() {
            var action = $(this).data('action');

            $('#modalDelete').modal('show');

            $('#formDelete').attr('action', action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
