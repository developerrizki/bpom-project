@extends('layouts.root')

@section('title','Training of Trainer')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif
                        
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="title mb-20">
                                            <h6><a href="{{ url('laporan-tot') }}">Training of Trainer</a> &rsaquo; Detail</h6>
                                        </div>
                                        <hr class="success mt-0 mb-30" style="height:2px;">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Nama Laporan</label> <br>
                                            {{ $tot->name }}
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="control-label">Deskripsi Laporan</label> <br>
                                            {!! $tot->description !!}
                                        </div>
                                        <div class="form-group">
                                            <label for="file" class="control-label">File Laporan</label> <br>
                                            <a href="{{ Storage::url('laporan-tot/'.$file->file) }}">{{ $file->file }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-12">
                                                    <a href="#modal-file-pendukung" data-toggle="modal" class="btn btn-block btn-success btn-sm">+ &nbsp; Upload File Pendukung</a>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                @if($tot->files->where('is_support', 1)->count() > 0)
                                <div class="document-grid">
                                    @foreach($tot->files->where('is_support', 1) as $key => $item)
                                        <div class="col-sm-3 text-center">
                                            <a href="{{ Storage::url('laporan-tot/'.$item->file) }}" title="{{ $item->file }}" target="_blank" class="item">
                                                <div class="icon">
                                                    <img src="{{ asset('img/icons/document.png') }}" alt="">
                                                </div>
                                                <p>{{ strlen($item->file) > 15 ? substr(ucwords($item->file),0,20)." ..." : ucwords($item->file) }}</p>
                                            </a>
                                            <button class="btn btn-danger btn-delete btn-sm mt-3" data-action="{{ url('laporan-tot/file/hapus/'. $item->report_file_id) }}">
                                                <i class="icon fa fa-trash"></i> &nbsp; Hapus File
                                            </button>
                                        </div>
                                    @endforeach
                                </div>
                                @else
                                    <h5 class="text-center">File Pendukung belum tersedia.</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->

    <div id="modal-file-pendukung" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="modal-body">
                    <div class="title text-center">
                        <h6>
                            Unggah File Pendukung
                        </h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('laporan-tot/file/tambah/'. $tot->report_id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" style="margin-bottom: 20px">
                                    <input type="file" name="file" class="form-control" required style="margin-bottom: 10px">
                                </div>
                                <div class="form-group" style="margin-bottom: 20px">
                                    <button type="submit" class="btn btn-sm btn-success">Unggah</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Batal</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modal_delete')
@endsection


@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Data Sekolah');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush