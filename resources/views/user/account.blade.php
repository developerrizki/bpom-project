@extends('layouts.root')

@section('title','Akun Anda')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        
                        @if ($errors->any())
                            <div class="alert alert-danger m-t-20">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                
                        @if(Session::has('status'))
                            @if(Session::get('status') == '200')
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @elseif(Session::get('status') == 'err')
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-ban"></i> Gagal!</h3>
                                    <p class="mb-0">{{ Session::get('msg') }}</p>
                                </div>
                            @endif
                        @endif

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Akun Saya</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form method="POST" action="{{ url('akun/ubah/'.Auth::user()->id) }}">
                                    @csrf
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-primary" width="20%">Nama</td>
                                                <td width="5%">:</td>
                                                <td>
                                                    <input type="text" class="form-control p-0 border-0" name="name" value="{{ Auth::user()->name }}" placeholder="Masukan nama Anda" autofocus>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-primary" width="20%">Username</td>
                                                <td width="5%">:</td>
                                                <td>
                                                    <input type="text" class="form-control p-0 border-0" name="username" value="{{ Auth::user()->username }}" placeholder="Masukan username Anda">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-primary" width="20%">Email</td>
                                                <td width="5%">:</td>
                                                <td>
                                                    <input type="text" class="form-control p-0 border-0" name="email" value="{{ Auth::user()->email }}" placeholder="Masukan email Anda">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-primary" width="20%">Kata Sandi Baru</td>
                                                <td width="5%">:</td>
                                                <td>
                                                    <input type="password" name="password" class="form-control p-0 border-0" placeholder="Masukan kata sandi Anda">
                                                    <div class="text-danger">Catatan: Kosongkan jika tidak ingin mengganti kata sandi</div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="form-group text-right">
                                        <button class="btn btn-success" type="submit">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection