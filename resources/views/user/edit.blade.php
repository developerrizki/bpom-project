@extends('layouts.root')

@section('title','Pengguna')

@section('content')
    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Pengguna &rsaquo; Tambah</h6>
                                        </div>
                                    </div>
                                </div>
                                <hr class="success mt-0 mb-30">
                                <form action="{{ url('pengguna/ubah/'. $user->id) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="control-label">Nama</label>
                                        <input type="text" class="form-control" name="name" required placeholder="Masukan nama pengguna" autofocus value="{{ $user->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Username</label>
                                        <input type="text" class="form-control" name="username" required placeholder="Masukan username pengguna" value="{{ $user->username }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Email</label>
                                        <input type="email" class="form-control" name="email" required placeholder="Masukan email pengguna" value="{{ $user->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Kata Sandi</label>
                                        <input type="password" class="form-control" name="password" placeholder="Masukan kata sandi pengguna">
                                        <span class="text-danger">Catatan: Kosongkan jika tidak ingin mengganti kata sandi</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Level</label>
                                        <select name="level_id" id="level_id" required class="form-control custom-select">
                                            <option value="">Pilih Level</option>
                                            @foreach($level as $key => $item)
                                                <option value="{{ $item->level_id }}" {{ $user->level_id == $item->level_id ? "selected" : "" }}>{{ $item->level }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @if($user->level_id == 2)
                                        <div class="form-group" id="province">
                                            <label for="" class="control-label">Provinsi</label>
                                            <select name="province_id" id="province_id" required class="form-control custom-select">
                                                <option value="">Pilih Provinsi</option>
                                                @foreach($province as $key => $item)
                                                    <option value="{{ $item->province_id }}" {{ $user->province_id == $item->province_id ? "selected" : "" }}>{{ $item->province }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-save btn-sm">Simpan</button>
                                        <a href="{{ url('pengguna') }}" class="btn btn-danger btn-sm">Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main> <!-- main -->
@endsection

@push('script')
    <script>
        $('#level_id').bind('change', function(){
            var level_id = $(this).val();
            if(level_id == 2) {
                $('#province').show();
            }else{
                $('#province').hide();
            }
        });
    </script>
@endpush