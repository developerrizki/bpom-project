@extends('layouts.root')

@section('title','Kalender Kegiatan')

@push('style')
    <style>
        #calendar.fc .fc-toolbar {
            display: inherit !important;
        }
    </style>
@endpush

@section('content')

    <main class="main">
        <div class="page-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        @include("layouts.sidebar")
                    </div>
                    <div class="col-lg-8">

                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-md-auto">
                                        <div class="title mb-20">
                                            <h6>Kalender Events</h6>
                                        </div>
                                    </div>
                                    {{--  <div class="col-md-8">
                                        <div class="action mb-20">
                                            <div class="row justify-content-end">
                                                <div class="col-md-5 col-7">
                                                    <form>
                                                        <select name="" class="form-control custom-select">
                                                            <option value="">{{ date('F Y') }}</option>
                                                        </select>
                                                    </form>
                                                </div>
                                                <div class="col-auto">
                                                    <a href="#modal-event-add" data-toggle="modal" class="btn btn-success btn-sm">Add Event</a>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>  --}}
                                </div>
                                <hr class="mt-0 mb-30">
                                <div class="calendar-list">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL -->
        <div id="modal-event-detail" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div class="title text-center">
                            <h6>
                                Event Detail
                            </h6>
                        </div>

                        <div class="media">
                            <div class="date">
                                <h5>Kamis</h5>
                                <h3>18</h3>
                            </div>
                            <div class="media-body">
                                <p class="font-weight-bold">Meeting bersama semua balai</p>
                                <p>02.00 PM</p>
                                <p>Ruang Meeting Gedung F</p>
                            </div>
                        </div>
                        <hr>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum fringilla sapien id porta. Vestibulum sit amet laoreet odio. Ut non arcu leo. Nam finibus dolor nec est efficitur placerat. Curabitur cursus semper pellentesque.</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-event-add" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div class="title text-center">
                            <h6>
                                Event Detail
                            </h6>
                        </div>

                        <form action="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="date" class="form-control" placeholder="Select Date">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="time" class="form-control" placeholder="Select Time">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Where">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Event Description"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-success" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL -->
    </main> <!-- main -->
    
@endsection

@push('script')
    <script>
        $(document).ready(function(){
            $('.fc-next-button').attr('disabled', false);
            $('.fc-next-button').removeClass('disabled');
            $('.fc-next-button').click(function() {
                $('#calendar').fullCalendar('next');
            });

            $('.fc-prev-button').attr('disabled', false);
            $('.fc-prev-button').removeClass('disabled');
            $('.fc-prev-button').click(function() {
                $('#calendar').fullCalendar('prev');
            });
        })
    </script>
@endpush
