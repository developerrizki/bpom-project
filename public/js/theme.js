$(document).ready(function(){
    //Navbar Scrolling Background
    // $(window).on("scroll", function() {
    //     var bodyScroll = $(window).scrollTop(),
    //         navbar = $(".navbar")
    //     if (bodyScroll > 100) {
    //         navbar.removeClass("slideInUp");
    //         navbar.addClass("fixed-top scroll animated slideInDown");
    //     } else {
    //         navbar.removeClass("fixed-top animated scroll slideInDown");
    //         navbar.addClass("animated slideInUp");
    //     }
    // });

    // add active state on menu for current page
    var url = window.location.pathname;
    var slug = url.substring(url.lastIndexOf('/')+1);
    var activeSlug = $('.navbar-nav li a[href="'+ slug +'"]');

    activeSlug.addClass('active');

    //Sidebar Multilevel
    $(function() {
        "use strict";
        let url = window.location + "";
        let path = url.replace(window.location.protocol + "//" + window.location.host + "/", "");
        let element = $('ul#sidebarnav a').filter(function() {
            return this.href === url || this.href === path;// || url.href.indexOf(this.href) === 0;
        });

        element.parentsUntil(".sidebar-nav").each(function (index){
            if($(this).is("li") && $(this).children("a").length !== 0)
            {
                $(this).children("a").addClass("active");
                $(this).parent("ul#sidebarnav").length === 0
                    ? $(this).addClass("active")
                    : $(this).addClass("selected");
            }
            else if(!$(this).is("ul") && $(this).children("a").length === 0)
            {
                $(this).addClass("selected");
                
            }
            else if($(this).is("ul")){
                $(this).addClass('in');
            }
            
        });

        element.addClass("active"); 
        $('#sidebarnav a').on('click', function (e) {
            
            if (!$(this).hasClass("active")) {
                // hide any open menus and remove all other classes
                $("ul", $(this).parents("ul:first")).removeClass("in");
                $("a", $(this).parents("ul:first")).removeClass("active");
                
                // open our new menu and add the open class
                $(this).next("ul").addClass("in");
                $(this).addClass("active");
                
            }
            else if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this).parents("ul:first").removeClass("active");
                $(this).next("ul").removeClass("in");
            }
        })
        $('#sidebarnav >li >a.has-arrow').on('click', function (e) {
            e.preventDefault();
        });
        
    });

    //calendar
    var d = new Date(), y = d.getFullYear(), m = d.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);
    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap4',
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        defaultDate: d.getDay(),
        validRange: {
            start: firstDay,
            end: lastDay
        },
        buttonIcons: {
            prev: 'left-single-arrow',
            next: 'right-single-arrow'
        },
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: false, // allow "more" link when too many events
        defaultView: $(window).width() < 768 ? 'listMonth':'month',
        events: [
            {
                title: 'Flight to Makassar (GA 661)',
                start: '2020-02-21T12:35:00',
                end: '2020-02-21T13:30:00'
            }
        ]
    });

    $('#calendar-small').fullCalendar({
        themeSystem: 'bootstrap4',
        header: {
            left: '',
            center: 'title',
            right: ''
        },
        defaultDate: d.getDay(),
        validRange: {
            start: firstDay,
            end: lastDay
        },
        prev: 'left-single-arrow',
        next: 'right-single-arrow',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        defaultView: $(window).width() < 768 ? 'listMonth':'month',
        events: [
            {
                title: 'Flight to Makassar (GA 661)',
                start: '2020-02-21T12:35:00',
                end: '2020-02-21T13:30:00'
            }
        ]
    });

    // $('.calendar-list').find('.fc-prev-button').click(function () {
    //     $('#calendar-small').fullCalendar('prev');
    //     $('#calendar').fullCalendar('prev');
    // });
    // $('.calendar-list').find('.fc-next-button').click(function () {
    //     $('#calendar-small').fullCalendar('next');
    //     $('#calendar').fullCalendar('next');
    // });

    $(window).on('resize',function(){location.reload();});

    $('.slider-single').owlCarousel({
        loop: true,
        autoplay: true,
        items: 1,
        responsiveClass: true,
        dots: false,
        nav: true,
        navText : ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>']
    });

});