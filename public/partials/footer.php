    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p class="copyright">
                        Copyright 2019. BPOM. All Right Reserved
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/moment.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate.js"></script>
    <script type="text/javascript" src="assets/lib/bootstrap/libraries/popper.js"></script>
    <script type="text/javascript" src="assets/lib/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="assets/lib/fullcalendar/fullcalendar.js"></script>
    <script type="text/javascript" src="assets/lib/owl-carousel/owl-carousel.js"></script>
    <script type="text/javascript" src="assets/js/theme.js"></script>
</body>
</html>