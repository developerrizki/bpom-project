<aside class="sidebar">
    <nav class="sidebar-nav">
        <ul id="sidebarnav">
            <li class="nav-item">
                <a href="home.php" class="nav-link">
                    <div class="icon">
                        <img src="assets/img/icons/sidebar/home.svg" alt="">
                    </div>
                    Home
                </a>
            </li>

            <li class="nav-item">
                <a href="school.php" class="nav-link">
                    <div class="icon">
                        <img src="assets/img/icons/sidebar/school.svg" alt="">
                    </div>
                    Sekolah
                </a>
            </li>

            <li class="nav-item">
                <a href="calendar.php" class="nav-link">
                    <div class="icon">
                        <img src="assets/img/icons/sidebar/event.svg" alt="">
                    </div>
                    Kalender Event
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <div class="icon">
                        <img src="assets/img/icons/sidebar/document.svg" alt="">
                    </div>
                    Dokumen
                </a>
                <ul aria-expanded="false" class="collapse first-level">
                    <li class="nav-item">
                        <a href="dokumen-materi.php" class="nav-link">
                            Materi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dokumen-lainnya.php" class="nav-link">
                            Informasi Lainnya
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <div class="icon">
                        <img src="assets/img/icons/sidebar/document.svg" alt="">
                    </div>
                    Laporan
                </a>
                <ul aria-expanded="false" class="collapse first-level">
                    <li class="nav-item">
                        <a href="laporan-tot.php" class="nav-link">
                            TOT di Pusat
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-advokasi.php" class="nav-link">
                            Advokasi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-bimtek.php" class="nav-link">
                            Bimtek PJAS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-pelatihan.php" class="nav-link">
                            Pelatihan PBKP - PKS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-sertifikat.php" class="nav-link">
                            Sertifikat PBKP - PKS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-audit.php" class="nav-link">
                            Audit PBKP - PKS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-sampling.php" class="nav-link">
                            Sampling PJAS & Operasional Mobling
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-paket.php" class="nav-link">
                            Paket Edukasi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-monitoring.php" class="nav-link">
                            Monitoring dan Evaluasi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan-akhir.php" class="nav-link">
                            Laporan Akhir
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <div class="icon">
                        <img src="assets/img/icons/sidebar/survey.svg" alt="">
                    </div>
                    Survey
                </a>
                <ul aria-expanded="false" class="collapse first-level">
                    <li class="nav-item">
                        <a href="survey-online.php" class="nav-link">
                            Online
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="survey-offline.php" class="nav-link">
                            Offline
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</aside>