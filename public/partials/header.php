<!DOCTYPE html>
<html>
<head>
    <title>BPOM</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- FONT -->
    <link href="assets/fonts/font-awesome/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&display=swap" rel="stylesheet"> 

    <!-- STYLE -->
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" media="all" href="assets/lib/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" media="all" href="assets/lib/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" type="text/css" media="all" href="assets/lib/owl-carousel/owl-carousel.css">
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/theme.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="home.php">
                    <img src="assets/img/logo.png" alt="">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <img src="assets/img/icons/burger.svg" alt="">
                </button>

                <div class="collapse navbar-collapse text-center" id="navbar-menu">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="assets/img/icons/settings.svg" alt="">
                                </div>
                                Akun Saya
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="assets/img/icons/mail.svg" alt="">
                                </div>
                                Message
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="assets/img/icons/notification.svg" alt="">
                                </div>
                                Notifikasi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <div class="img">
                                    <img src="assets/img/icons/logout.svg" alt="">
                                </div>
                                Log Out
                            </a>
                        </li>
                    </ul>
                </div>
    
            </div>
        </nav>
    </header>