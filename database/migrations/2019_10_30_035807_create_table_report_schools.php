<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReportSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_schools', function (Blueprint $table) {
            $table->bigIncrements('report_school_id');
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->bigInteger('school_id')->unsigned()->nullable();
            $table->string('canteen',100)->nullable();
            $table->string('condition', 100)->nullable();
            $table->timestamps();

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('school_id')->references('school_id')->on('schools')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_schools');
    }
}
