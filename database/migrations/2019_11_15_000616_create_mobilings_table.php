<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobilingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_mobilings', function (Blueprint $table) {
            $table->bigIncrements('report_mobiling_id');
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->text('address')->nullable();
            $table->text('community')->nullable();
            $table->string('product_type',255)->nullable();
            $table->integer('participant_kie')->default(0)->nullable();
            $table->text('theory_kie')->nullable();
            $table->integer('is_documentation')->default(0)->nullable()->comment('0: Tidak Ada, 1 : Ada');
            $table->integer('is_evaluation')->default(0)->nullable()->comment('0: Tidak Ada, 1 : Ada');
            $table->timestamps();

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_mobilings');
    }
}
