<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReportCommunities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_communities', function (Blueprint $table) {
            $table->bigIncrements('report_community_id');
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->bigInteger('school_id')->unsigned()->nullable();
            $table->string('school_comunity',150)->nullable();
            $table->integer('sum_community')->default(0);
            $table->timestamps();

            $table->foreign('school_id')->references('school_id')->on('schools')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_communities');
    }
}
