<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinceTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province_targets', function (Blueprint $table) {
            $table->bigIncrements('target_id');
            $table->integer('province_id')->unsigned()->nullable();
            $table->integer('target_year')->default(2019)->nullable();
            $table->integer('target')->default(0)->nullable();
            $table->timestamps();

            $table->foreign('province_id')->references('province_id')->on('provinces')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province_targets');
    }
}
