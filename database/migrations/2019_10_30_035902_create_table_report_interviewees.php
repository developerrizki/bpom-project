<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReportInterviewees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_interviewees', function (Blueprint $table) {
            $table->bigIncrements('report_interview_id');
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->string('name',100)->nullable();
            $table->string('agency', 100)->nullable();
            $table->string('position', 100)->nullable();
            $table->text('theory')->nullable();
            $table->timestamps();

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_interviewees');
    }
}
