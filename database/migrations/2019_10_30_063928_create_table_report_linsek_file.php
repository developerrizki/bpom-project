<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReportLinsekFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_linsek_file', function (Blueprint $table) {
            $table->bigIncrements('linsek_file_id');
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->text('file')->nullable();
            $table->timestamps();

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_linsek_file');
    }
}
