<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReportLinsek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_linsek', function (Blueprint $table) {
            $table->bigIncrements('report_linsek_id');
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->string('name',100)->nullable();
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->integer('participant')->nullable();
            $table->timestamps();

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_linsek');
    }
}
