<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sender_id')->unsigned()->nullable();
            $table->bigInteger('receipt_id')->unsigned()->nullable();
            $table->string('subject',200);
            $table->integer('is_read')->default(0)->comment('0: unread, 1: read');
            $table->timestamps();

            $table->foreign('sender_id')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('receipt_id')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
