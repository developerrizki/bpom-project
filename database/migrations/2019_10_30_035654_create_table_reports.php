<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('report_id');
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('type')->default(0)->comment('0: TOT, 1: Advokasi, 2: Bimtek, 3: Pelatihan, 4: Setifikat, 5: Audit, 6: Sampling, 7: Paket, 8: Monev, 9: Akhir');
            $table->bigInteger('created_user')->unsigned()->nullable();
            $table->bigInteger('updated_user')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('created_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('updated_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
