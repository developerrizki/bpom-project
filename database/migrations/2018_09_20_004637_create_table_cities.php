<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('cities');

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('city_id');
            $table->string('city')->nullable();
            $table->integer('province_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('province_id')->references('province_id')->on('provinces')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
