<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_packages', function (Blueprint $table) {
            $table->bigIncrements('report_package_id');
            $table->string('name',50)->nullable();
            $table->text('address')->nullable();
            $table->integer('province_id')->unsigned()->nullable();
            $table->integer('school')->default(0);
            $table->date('periode')->nullable();
            $table->integer('package')->default(0);
            $table->string('package_type',100)->nullable();
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('province_id')->references('province_id')->on('provinces')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_packages');
    }
}
