<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMonitoringDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring_details', function (Blueprint $table) {
            $table->bigIncrements('monitoring_detail_id');
            $table->bigInteger('monitoring_id')->unsigned()->nullable();
            $table->integer('is_bimtek')->nullable()->comment('0: Bimtek, 1: Pelatihan');
            $table->integer('participant')->nullable();
            $table->integer('pretest')->nullable();
            $table->integer('posttest')->nullable();
            $table->text('item')->nullable();
            $table->timestamps();

            $table->foreign('monitoring_id')->references('monitoring_id')->on('monitorings')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring_details');
    }
}
