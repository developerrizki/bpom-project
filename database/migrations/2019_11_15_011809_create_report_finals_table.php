<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportFinalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_finals', function (Blueprint $table) {
            $table->bigIncrements('report_id');
            $table->string('title',200)->nullable();
            $table->text('file')->nullable();
            $table->string('summary', 255)->nullable();
            $table->text('foreword')->nullable();
            $table->text('preliminary')->nullable();
            $table->text('destination')->nullable();
            $table->text('target')->nullable();
            $table->text('obstacle')->nullable();
            $table->text('recommendetion')->nullable();
            $table->text('closing')->nullable();
            $table->bigInteger('created_user')->unsigned()->nullable();
            $table->bigInteger('updated_user')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('created_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('updated_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_finals');
    }
}
