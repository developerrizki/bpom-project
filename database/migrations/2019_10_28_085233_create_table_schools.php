<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('school_id');
            $table->string('name',100);
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('status')->default(0)->comment('0: negeri, 1: swasta');
            $table->char('accreditaion',2)->nullable();
            $table->bigInteger('created_user')->unsigned()->nullable();
            $table->bigInteger('updated_user')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('created_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('updated_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
