<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSamplingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_samplings', function (Blueprint $table) {
            $table->bigIncrements('report_sampling_id');
            $table->bigInteger('report_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('location',150)->nullable();
            $table->integer('sample_code')->nullable();
            $table->string('product_name',150)->nullable();
            $table->string('seller_name',150)->nullable();
            $table->string('processing_name',150)->nullable();
            $table->string('type_of_food',150)->nullable();
            $table->string('registration_no',150)->nullable();
            $table->integer('is_boraks')->nullable()->default(0)->comment('0:negatif, 1:positif');
            $table->integer('is_formalin')->nullable()->default(0)->comment('0:negatif, 1:positif');
            $table->integer('is_rodhamin_b')->nullable()->default(0)->comment('0:negatif, 1:positif');
            $table->integer('is_methanyl_yellow')->nullable()->default(0)->comment('0:negatif, 1:positif');
            $table->string('result',150)->nullable();
            $table->string('conclution',150)->nullable();
            $table->timestamps();

            $table->foreign('report_id')->references('report_id')->on('reports')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('city_id')->references('city_id')->on('cities')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_samplings');
    }
}
