<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMonitoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitorings', function (Blueprint $table) {
            $table->bigIncrements('monitoring_id');
            $table->bigInteger('monitoring_category_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('place')->nullable();
            $table->datetime('time')->nullable();
            $table->string('sum_participant')->nullable();
            $table->string('timeline')->nullable();
            $table->string('information')->nullable();
            $table->string('description')->nullable();
            $table->bigInteger('created_user')->unsigned()->nullable();
            $table->bigInteger('updated_user')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('monitoring_category_id')->references('monitoring_category_id')->on('monitoring_categories')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('created_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            $table->foreign('updated_user')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitorings');
    }
}
