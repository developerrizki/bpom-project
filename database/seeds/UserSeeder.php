<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = array([
            'name' => 'Super Admin',
            'username' => 'superbpom',
            'email' => 'super@bpom.go.id',
            'password' => bcrypt('super890'),
            'level_id' => 1
        ],
        [
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@bpom.go.id',
            'password' => bcrypt('123456789'),
            'level_id' => 2
        ],
        [
            'name' => 'User',
            'username' => 'user',
            'email' => 'user@bpom.go.id',
            'password' => bcrypt('123456789'),
            'level_id' => 3
        ]);

        for ($i=0; $i < sizeof($user); $i++) { 
            DB::table('users')->insert($user[$i]);
        }
    }
}
