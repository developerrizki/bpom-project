<?php

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = ["Level","User","Module","Task","Role","Menu","Dashboard"];

        for($i=0; $i < sizeof($module); $i++)
        {
            DB::table('sys_modules')->insert(
                ['modules_id' => $i+1 ,'module' => $module[$i]]
            );
        }
    }
}
