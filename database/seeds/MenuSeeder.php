<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = [
            [
                'menu_id' =>  1,
                'module_id' =>  7,
                'menu' =>  "Dasbor",
                'menu_icon' =>  "si si-cup",
                'menu_url' =>  "home",
                'menu_is_sub' =>  0,
                'menu_parent' =>  null,
                'menu_position' =>  0,
                'created_at' =>  "2019-06-16 22:32:23",
                'updated_at' =>  "2019-06-21 09:35"
            ],
            [
                'menu_id' =>  2,
                'module_id' =>  null,
                'menu' =>  "Pengguna",
                'menu_icon' =>  "si si-user",
                'menu_url' =>  "",
                'menu_is_sub' =>  0,
                'menu_parent' =>  null,
                'menu_position' =>  1,
                'created_at' =>  "2019-06-16 22:34:58",
                'updated_at' =>  "2019-06-21 09:41"
            ],
            [
                'menu_id' =>  3,
                'module_id' =>  1,
                'menu' =>  "Users Level",
                'menu_icon' =>  null,
                'menu_url' =>  "level",
                'menu_is_sub' =>  1,
                'menu_parent' =>  2,
                'menu_position' =>  0,
                'created_at' =>  "2019-06-16 22:35:51",
                'updated_at' =>  "2019-06-16 22:51"
            ],
            [
                'menu_id' =>  4,
                'module_id' =>  2,
                'menu' =>  "Users",
                'menu_icon' =>  null,
                'menu_url' =>  "user",
                'menu_is_sub' =>  1,
                'menu_parent' =>  2,
                'menu_position' =>  1,
                'created_at' =>  "2019-06-16 22:36:20",
                'updated_at' =>  "2019-06-17 15:26:20"
            ],
            [
                'menu_id' =>  9,
                'module_id' =>  null,
                'menu' =>  "Sistem",
                'menu_icon' =>  "si si-screen-desktop",
                'menu_url' =>  "",
                'menu_is_sub' =>  0,
                'menu_parent' =>  null,
                'menu_position' =>  2,
                'created_at' =>  "2019-06-16 22:40:25",
                'updated_at' =>  "2019-06-21 09:51:49"
            ],
            [
                'menu_id' =>  10,
                'module_id' =>  3,
                'menu' =>  "Modules",
                'menu_icon' =>  null,
                'menu_url' =>  "system/module",
                'menu_is_sub' =>  1,
                'menu_parent' =>  9,
                'menu_position' =>  0,
                'created_at' =>  "2019-06-16 22:41:42",
                'updated_at' =>  "2019-06-17 00:30:58"
            ],
            [
                'menu_id' =>  11,
                'module_id' =>  4,
                'menu' =>  "Task",
                'menu_icon' =>  null,
                'menu_url' =>  "system/task",
                'menu_is_sub' =>  1,
                'menu_parent' =>  9,
                'menu_position' =>  1,
                'created_at' =>  "2019-06-16 22:48:08",
                'updated_at' =>  "2019-06-16 22:08"
            ],
            [
                'menu_id' =>  12,
                'module_id' =>  5,
                'menu' =>  "Menu",
                'menu_icon' =>  null,
                'menu_url' =>  "system/menu",
                'menu_is_sub' =>  1,
                'menu_parent' =>  9,
                'menu_position' =>  2,
                'created_at' =>  "2019-06-16 22:49:07",
                'updated_at' =>  "2019-06-17 14:23:12"
            ],
            [
                'menu_id' =>  13,
                'module_id' =>  6,
                'menu' =>  "Role",
                'menu_icon' =>  null,
                'menu_url' =>  "system/role",
                'menu_is_sub' =>  1,
                'menu_parent' =>  9,
                'menu_position' =>  3,
                'created_at' =>  "2019-06-17 14:22:42:",
                'updated_at' =>  "2019-06-17 14:33:03"
            ]
        ];            

        for($i=0; $i < sizeof($menu); $i++)
        {
            DB::table('sys_menus')->insert($menu[$i]);
        }
    }
}
