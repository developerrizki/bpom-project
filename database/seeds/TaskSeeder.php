<?php

use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = DB::table('sys_modules')->get();

        $task 	= array("list","create","edit","view","delete");

        foreach ($module as $key => $value) 
        {
        	for ($i=0; $i < sizeof($task); $i++) { 
        		DB::table('sys_tasks')->insert([
        			"modules_id" 	=> $value->modules_id,
        			"tasks" 		=> $task[$i]
	        	]);
        	}
        }
    }
}
