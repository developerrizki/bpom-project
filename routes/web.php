<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

//ajax get city
Route::get('getCity', function (Request $request) {
    if ($request->ajax()) {
        $city = \App\Models\Master\City::where('province_id', $request->province_id)->get();

        $html = "<option value=''>Pilih Kota</option>";

        foreach ($city as $key => $value) {
            $html .= "<option value='" . $value->city_id . "'>" . $value->city . "</option>";
        }

        return $html;
    }
});

Auth::routes();

Route::get('/beranda', 'HomeController@index')->name('beranda');
Route::get('/kalender', 'Calendar\CalendarController@index');

Route::namespace('User')->prefix('akun')->group(function () {
    Route::get('/', 'UserController@index');
    Route::get('/tambah', 'UserController@create');
    Route::post('/tambah', 'UserController@store');
    Route::get('/ubah/{id}', 'UserController@edit');
    Route::post('/ubah/{id}', 'UserController@update');
    Route::get('/detil/{id}', 'UserController@show');
    Route::delete('/hapus/{id}', 'UserController@delete');
});

Route::namespace('School')->prefix('sekolah')->group(function () {
    Route::get('/', 'SchoolController@index');
    Route::get('/tambah', 'SchoolController@create');
    Route::post('/tambah', 'SchoolController@store');
    Route::get('/ubah/{id}', 'SchoolController@edit');
    Route::post('/ubah/{id}', 'SchoolController@update');
    Route::post('/import', 'SchoolController@import');
    Route::get('/detil/{id}', 'SchoolController@show');
    Route::get('/filter', 'SchoolController@filter');
    Route::delete('/hapus/{id}', 'SchoolController@delete');
});

Route::namespace('Document')->prefix('dokumen-materi')->group(function () {
    Route::get('/', 'TheoryController@index');
    Route::get('/tambah', 'TheoryController@create');
    Route::post('/tambah', 'TheoryController@store');
    Route::get('/ubah/{id}', 'TheoryController@edit');
    Route::post('/ubah/{id}', 'TheoryController@update');
    Route::get('/detil/{id}', 'TheoryController@show');
    Route::delete('/hapus/{id}', 'TheoryController@delete');
});

Route::namespace('Document')->prefix('dokumen-lainnya')->group(function () {
    Route::get('/', 'OtherController@index');
    Route::get('/tambah', 'OtherController@create');
    Route::post('/tambah', 'OtherController@store');
    Route::get('/ubah/{id}', 'OtherController@edit');
    Route::post('/ubah/{id}', 'OtherController@update');
    Route::get('/detil/{id}', 'OtherController@show');
    Route::delete('/hapus/{id}', 'OtherController@delete');
});

Route::namespace('Survey')->prefix('survey-online')->group(function () {
    Route::get('/', 'OnlineController@index');
    Route::get('/tambah', 'OnlineController@create');
    Route::post('/tambah', 'OnlineController@store');
    Route::get('/ubah/{id}', 'OnlineController@edit');
    Route::post('/ubah/{id}', 'OnlineController@update');
    Route::get('/detil/{id}', 'OnlineController@show');
    Route::delete('/hapus/{id}', 'OnlineController@delete');
});

Route::namespace('Report')->prefix('laporan-akhir')->group(function () {
    Route::get('/', 'FinalController@index');
    Route::get('/tambah', 'FinalController@create');
    Route::post('/tambah', 'FinalController@store');
    Route::get('/ubah/{id}', 'FinalController@edit');
    Route::post('/ubah/{id}', 'FinalController@update');
    Route::get('/detil/{id}', 'FinalController@show');
    Route::delete('/hapus/{id}', 'FinalController@delete');
});

Route::namespace('System')->prefix('level')->group(function () {
    Route::get('/', 'LevelController@index');
    Route::get('/tambah', 'LevelController@create');
    Route::post('/tambah', 'LevelController@store');
    Route::get('/ubah/{id}', 'LevelController@edit');
    Route::post('/ubah/{id}', 'LevelController@update');
    Route::delete('/hapus/{id}', 'LevelController@delete');
});

Route::namespace('System')->prefix('pengguna')->group(function () {
    Route::get('/', 'UserController@index');
    Route::get('/tambah', 'UserController@create');
    Route::post('/tambah', 'UserController@store');
    Route::get('/ubah/{id}', 'UserController@edit');
    Route::post('/ubah/{id}', 'UserController@update');
    Route::delete('/hapus/{id}', 'UserController@delete');
});



Route::namespace('Master')->group(function () {
    Route::prefix('provinsi')->group(function () {
        Route::get('/', 'ProvinceController@index');
        Route::get('/tambah', 'ProvinceController@create');
        Route::post('/tambah', 'ProvinceController@store');
        Route::get('/ubah/{id}', 'ProvinceController@edit');
        Route::post('/ubah/{id}', 'ProvinceController@update');
        Route::delete('/hapus/{id}', 'ProvinceController@delete');
        //Target
        Route::get('/target', 'ProvinceController@target');
        Route::get('/target/tambah', 'ProvinceController@targetCreate');
        Route::post('/target/tambah', 'ProvinceController@targetCreate');
        Route::get('/target/ubah/{id}', 'ProvinceController@targetEdit');
        Route::post('/target/ubah/{id}', 'ProvinceController@targetEdit');
        Route::delete('target/hapus/{id}', 'ProvinceController@targetDelete');
    });

    Route::prefix('kota')->group(function () {
        Route::get('/', 'CityController@index');
        Route::get('/tambah', 'CityController@create');
        Route::post('/tambah', 'CityController@store');
        Route::get('/ubah/{id}', 'CityController@edit');
        Route::post('/ubah/{id}', 'CityController@update');
        Route::delete('/hapus/{id}', 'CityController@delete');
    });
});

Route::namespace('Survey')->prefix('survey-kuesioner')->group(function () {
    Route::get('/', 'OfflineController@index');
    Route::get('/tambah', 'OfflineController@create');
    Route::post('/tambah', 'OfflineController@store');
    Route::get('/ubah/{id}', 'OfflineController@edit');
    Route::post('/ubah/{id}', 'OfflineController@update');
    Route::get('/detil/{id}', 'OfflineController@show');
    Route::delete('/hapus/{id}', 'OfflineController@delete');
});

Route::namespace('Report')->prefix('laporan-tot')->group(function () {
    Route::get('/', 'TotController@index');
    Route::get('/tambah', 'TotController@create');
    Route::post('/tambah', 'TotController@store');
    Route::post('/file/tambah/{id}', 'TotController@storeFile');
    Route::get('/ubah/{id}', 'TotController@edit');
    Route::post('/ubah/{id}', 'TotController@update');
    Route::get('/detil/{id}', 'TotController@show');
    Route::delete('/hapus/{id}', 'TotController@delete');
    Route::delete('/file/hapus/{id}', 'TotController@deleteFile');
});

Route::namespace('Report')->prefix('laporan-advokasi')->group(function () {
    Route::get('/', 'AdvocacyController@index');
    Route::get('/detil/{id}', 'AdvocacyController@detail');
    Route::get('/show/{id}', 'AdvocacyController@show');

    Route::get('/foto-kegiatan/{id}', 'AdvocacyController@foto');
    Route::get('/foto-kegiatan/tambah/{id}', 'AdvocacyController@createFoto');
    Route::post('/foto-kegiatan/tambah/{id}', 'AdvocacyController@createFoto');
    Route::delete('/foto-kegiatan/hapus/{id}', 'AdvocacyController@deleteFoto');

    Route::any('/dokumen/{id}', 'AdvocacyController@document');
    Route::get('/dokumen/tambah/{id}', 'AdvocacyController@createDocument');
    Route::post('/dokumen/tambah/{id}', 'AdvocacyController@createDocument');
    Route::delete('/dokumen/hapus/{id}', 'AdvocacyController@deleteDocument');

    Route::get('/daftar-hadir/{id}', 'AdvocacyController@attendance');
    Route::get('/daftar-hadir/tambah/{id}', 'AdvocacyController@createAttendance');
    Route::post('/daftar-hadir/tambah/{id}', 'AdvocacyController@createAttendance');
    Route::get('/daftar-hadir/ubah/{id}', 'AdvocacyController@editAttendance');
    Route::post('/daftar-hadir/ubah/{id}', 'AdvocacyController@editAttendance');
    Route::post('/daftar-hadir/import/{id}', 'AdvocacyController@importAttendance');
    Route::get('/daftar-hadir/tambah-dokumen/{id}', 'AdvocacyController@createAttendanceFile');
    Route::post('/daftar-hadir/tambah-dokumen/{id}', 'AdvocacyController@createAttendanceFile');
    Route::delete('/daftar-hadir/hapus-dokumen/{id}', 'AdvocacyController@deleteAttendanceFile');

    Route::get('/tambah/{province_id}', 'AdvocacyController@createReport');
    Route::post('/tambah/{province_id}', 'AdvocacyController@createReport');
    Route::get('/ubah/{id}', 'AdvocacyController@editReport');
    Route::post('/ubah/{id}', 'AdvocacyController@editReport');

    Route::get('/tambah-narasumber/{report_id}', 'AdvocacyController@createInterview');
    Route::post('/tambah-narasumber/{report_id}', 'AdvocacyController@createInterview');
    Route::post('/import-narasumber/{report_id}', 'AdvocacyController@importInterview');
    Route::get('/ubah-narasumber/{id}', 'AdvocacyController@editInterview');
    Route::post('/ubah-narasumber/{id}', 'AdvocacyController@editInterview');

    Route::get('/tambah-summary/{report_id}', 'AdvocacyController@createSummary');
    Route::post('/tambah-summary/{report_id}', 'AdvocacyController@createSummary');
    Route::get('/ubah-summary/{id}', 'AdvocacyController@editSummary');
    Route::post('/ubah-summary/{id}', 'AdvocacyController@editSummary');

    Route::delete('/hapus/{id}', 'AdvocacyController@deleteReport');
    Route::delete('/hapus-narasumber/{id}', 'AdvocacyController@deleteInteview');
    Route::delete('/hapus-summary/{id}', 'AdvocacyController@deleteSummary');
    Route::delete('/hapus-daftar-hadir/{id}', 'AdvocacyController@deleteAttendance');
});

Route::namespace('Report')->prefix('laporan-bimtek')->group(function () {
    Route::get('/', 'TechnicalController@index');
    Route::get('/detil/{id}', 'TechnicalController@detail');
    Route::get('/show/{id}', 'TechnicalController@show');

    Route::get('/foto-kegiatan/{id}', 'TechnicalController@foto');
    Route::get('/foto-kegiatan/tambah/{id}', 'TechnicalController@createFoto');
    Route::post('/foto-kegiatan/tambah/{id}', 'TechnicalController@createFoto');
    Route::delete('/foto-kegiatan/hapus/{id}', 'TechnicalController@deleteFoto');

    Route::any('/dokumen/{id}', 'TechnicalController@document');
    Route::get('/dokumen/tambah/{id}', 'TechnicalController@createDocument');
    Route::post('/dokumen/tambah/{id}', 'TechnicalController@createDocument');
    Route::delete('/dokumen/hapus/{id}', 'TechnicalController@deleteDocument');

    Route::get('/linsek/{id}', 'TechnicalController@linsek');
    Route::get('/linsek/tambah/{id}', 'TechnicalController@createLinsek');
    Route::post('/linsek/tambah/{id}', 'TechnicalController@createLinsek');
    Route::get('/linsek/ubah/{id}', 'TechnicalController@editLinsek');
    Route::post('/linsek/ubah/{id}', 'TechnicalController@editLinsek');
    Route::post('/linsek/import/{id}', 'TechnicalController@importLinsek');
    Route::get('/linsek/tambah-dokumen/{id}', 'TechnicalController@createLinsekFile');
    Route::post('/linsek/tambah-dokumen/{id}', 'TechnicalController@createLinsekFile');
    Route::delete('/linsek/hapus-dokumen/{id}', 'TechnicalController@deleteLinsekFile');

    Route::get('/daftar-sekolah/{id}', 'TechnicalController@school');
    Route::get('/daftar-sekolah/tambah/{id}', 'TechnicalController@createSchool');
    Route::post('/daftar-sekolah/tambah/{id}', 'TechnicalController@createSchool');
    Route::get('/daftar-sekolah/ubah/{id}', 'TechnicalController@editSchool');
    Route::post('/daftar-sekolah/ubah/{id}', 'TechnicalController@editSchool');
    Route::post('/daftar-sekolah/import/{id}', 'TechnicalController@importSchool');

    Route::get('/komunitas/{id}', 'TechnicalController@community');
    Route::get('/komunitas/tambah/{id}', 'TechnicalController@createCommunity');
    Route::post('/komunitas/tambah/{id}', 'TechnicalController@createCommunity');
    Route::get('/komunitas/ubah/{id}', 'TechnicalController@editCommunity');
    Route::post('/komunitas/ubah/{id}', 'TechnicalController@editCommunity');
    Route::post('/komunitas/import/{id}', 'TechnicalController@importCommunity');

    Route::get('/tambah/{province_id}', 'TechnicalController@createReport');
    Route::post('/tambah/{province_id}', 'TechnicalController@createReport');
    Route::get('/ubah/{id}', 'TechnicalController@editReport');
    Route::post('/ubah/{id}', 'TechnicalController@editReport');

    Route::get('/tambah-narasumber/{report_id}', 'TechnicalController@createInterview');
    Route::post('/tambah-narasumber/{report_id}', 'TechnicalController@createInterview');
    Route::get('/ubah-narasumber/{id}', 'TechnicalController@editInterview');
    Route::post('/ubah-narasumber/{id}', 'TechnicalController@editInterview');
    Route::post('/import-narasumber/{id}', 'TechnicalController@importInterview');

    Route::get('/tambah-summary/{report_id}', 'TechnicalController@createSummary');
    Route::post('/tambah-summary/{report_id}', 'TechnicalController@createSummary');
    Route::get('/ubah-summary/{id}', 'TechnicalController@editSummary');
    Route::post('/ubah-summary/{id}', 'TechnicalController@editSummary');

    Route::delete('/hapus/{id}', 'TechnicalController@deleteReport');
    Route::delete('/hapus-narasumber/{id}', 'TechnicalController@deleteInteview');
    Route::delete('/hapus-summary/{id}', 'TechnicalController@deleteSummary');
    Route::delete('/hapus-linsek/{id}', 'TechnicalController@deleteLinsek');
    Route::delete('/hapus-daftar-sekolah/{id}', 'TechnicalController@deleteSchool');
    Route::delete('/hapus-komunitas/{id}', 'TechnicalController@deleteCommunity');
});

Route::namespace('Report')->prefix('laporan-pelatihan')->group(function () {
    Route::get('/', 'TrainningController@index');
    Route::get('/detil/{id}', 'TrainningController@detail');
    Route::get('/show/{id}', 'TrainningController@show');

    Route::get('/foto-kegiatan/{id}', 'TrainningController@foto');
    Route::get('/foto-kegiatan/tambah/{id}', 'TrainningController@createFoto');
    Route::post('/foto-kegiatan/tambah/{id}', 'TrainningController@createFoto');
    Route::delete('/foto-kegiatan/hapus/{id}', 'TrainningController@deleteFoto');

    Route::get('/dokumen/{id}', 'TrainningController@document');
    Route::get('/dokumen/tambah/{id}', 'TrainningController@createDocument');
    Route::post('/dokumen/tambah/{id}', 'TrainningController@createDocument');
    Route::delete('/dokumen/hapus/{id}', 'TrainningController@deleteDocument');

    Route::get('/linsek/{id}', 'TrainningController@linsek');
    Route::get('/linsek/tambah/{id}', 'TrainningController@createLinsek');
    Route::post('/linsek/tambah/{id}', 'TrainningController@createLinsek');
    Route::get('/linsek/ubah/{id}', 'TrainningController@editLinsek');
    Route::post('/linsek/ubah/{id}', 'TrainningController@editLinsek');
    Route::get('/linsek/tambah-dokumen/{id}', 'TrainningController@createLinsekFile');
    Route::post('/linsek/tambah-dokumen/{id}', 'TrainningController@createLinsekFile');

    Route::get('/daftar-sekolah/{id}', 'TrainningController@school');
    Route::get('/daftar-sekolah/tambah/{id}', 'TrainningController@createSchool');
    Route::post('/daftar-sekolah/tambah/{id}', 'TrainningController@createSchool');
    Route::get('/daftar-sekolah/ubah/{id}', 'TrainningController@editSchool');
    Route::post('/daftar-sekolah/ubah/{id}', 'TrainningController@editSchool');

    Route::get('/komunitas/{id}', 'TrainningController@community');
    Route::get('/komunitas/tambah/{id}', 'TrainningController@createCommunity');
    Route::post('/komunitas/tambah/{id}', 'TrainningController@createCommunity');
    Route::get('/komunitas/ubah/{id}', 'TrainningController@editCommunity');
    Route::post('/komunitas/ubah/{id}', 'TrainningController@editCommunity');
    Route::post('/komunitas/import/{id}', 'TrainningController@importCommunity');

    Route::get('/tambah/{province_id}', 'TrainningController@createReport');
    Route::post('/tambah/{province_id}', 'TrainningController@createReport');
    Route::get('/ubah/{id}', 'TrainningController@editReport');
    Route::post('/ubah/{id}', 'TrainningController@editReport');

    Route::get('/tambah-narasumber/{report_id}', 'TrainningController@createInterview');
    Route::post('/tambah-narasumber/{report_id}', 'TrainningController@createInterview');
    Route::get('/ubah-narasumber/{id}', 'TrainningController@editInterview');
    Route::post('/ubah-narasumber/{id}', 'TrainningController@editInterview');
    Route::post('/import-narasumber/{id}', 'TrainningController@importInterview');

    Route::get('/tambah-summary/{report_id}', 'TrainningController@createSummary');
    Route::post('/tambah-summary/{report_id}', 'TrainningController@createSummary');
    Route::get('/ubah-summary/{id}', 'TrainningController@editSummary');
    Route::post('/ubah-summary/{id}', 'TrainningController@editSummary');

    Route::delete('/hapus/{id}', 'TrainningController@deleteReport');
    Route::delete('/hapus-narasumber/{id}', 'TrainningController@deleteInteview');
    Route::delete('/hapus-summary/{id}', 'TrainningController@deleteSummary');
    Route::delete('/hapus-linsek/{id}', 'TrainningController@deleteLinsek');
    Route::delete('/hapus-daftar-sekolah/{id}', 'TrainningController@deleteSchool');
    Route::delete('/hapus-komunitas/{id}', 'TrainningController@deleteCommunity');
});


Route::namespace('Report')->prefix('laporan-sertifikat')->group(function () {
    Route::get('/', 'CertificateController@index');
    Route::get('/detil/{id}', 'CertificateController@detail');
    Route::get('/show/{id}', 'CertificateController@show');

    Route::get('/daftar-sekolah/{id}', 'CertificateController@school');
    Route::get('/daftar-sekolah/tambah/{id}', 'CertificateController@createSchool');
    Route::post('/daftar-sekolah/tambah/{id}', 'CertificateController@createSchool');
    Route::get('/daftar-sekolah/ubah/{id}', 'CertificateController@editSchool');
    Route::post('/daftar-sekolah/ubah/{id}', 'CertificateController@editSchool');

    Route::get('/tambah/{province_id}', 'CertificateController@createReport');
    Route::post('/tambah/{province_id}', 'CertificateController@createReport');
    Route::get('/ubah/{id}', 'CertificateController@editReport');
    Route::post('/ubah/{id}', 'CertificateController@editReport');

    Route::delete('/hapus/{id}', 'CertificateController@deleteReport');
    Route::delete('/hapus-daftar-sekolah/{id}', 'CertificateController@deleteSchool');
});

Route::namespace('Report')->prefix('laporan-audit')->group(function () {
    Route::get('/', 'AuditController@index');
    Route::get('/detil/{id}', 'AuditController@detail');
    Route::get('/show/{id}', 'AuditController@show');

    Route::any('/dokumen/{id}', 'AuditController@document');
    Route::get('/dokumen/tambah/{id}', 'AuditController@createDocument');
    Route::post('/dokumen/tambah/{id}', 'AuditController@createDocument');
    Route::delete('/dokumen/hapus/{id}', 'AuditController@deleteDocument');

    Route::get('/daftar-sekolah/{id}', 'AuditController@school');
    Route::get('/daftar-sekolah/tambah/{id}', 'AuditController@createSchool');
    Route::post('/daftar-sekolah/tambah/{id}', 'AuditController@createSchool');
    Route::get('/daftar-sekolah/ubah/{id}', 'AuditController@editSchool');
    Route::post('/daftar-sekolah/ubah/{id}', 'AuditController@editSchool');

    Route::get('/tambah/{province_id}', 'AuditController@createReport');
    Route::post('/tambah/{province_id}', 'AuditController@createReport');
    Route::get('/ubah/{id}', 'AuditController@editReport');
    Route::post('/ubah/{id}', 'AuditController@editReport');

    Route::get('/foto-audit/{id}', 'AuditController@foto');
    Route::get('/foto-audit/tambah/{id}', 'AuditController@createFoto');
    Route::post('/foto-audit/tambah/{id}', 'AuditController@createFoto');
    Route::delete('/foto-audit/hapus/{id}', 'AuditController@deleteFoto');

    Route::delete('/hapus/{id}', 'AuditController@deleteReport');
    Route::delete('/hapus-daftar-sekolah/{id}', 'AuditController@deleteSchool');
});

Route::namespace('Report')->prefix('laporan-paket')->group(function () {
    Route::get('/', 'PackagesController@index');
    Route::get('/detil/{id}', 'PackagesController@detail');
    Route::get('/show/{id}', 'PackagesController@show');

    Route::get('/daftar-paket/{id}', 'PackagesController@package');
    Route::get('/daftar-paket/tambah/{id}', 'PackagesController@createPackage');
    Route::post('/daftar-paket/tambah/{id}', 'PackagesController@createPackage');
    Route::get('/daftar-paket/ubah/{id}', 'PackagesController@editPackage');
    Route::post('/daftar-paket/ubah/{id}', 'PackagesController@editPackage');

    Route::get('/tambah/{province_id}', 'PackagesController@createReport');
    Route::post('/tambah/{province_id}', 'PackagesController@createReport');
    Route::get('/ubah/{id}', 'PackagesController@editReport');
    Route::post('/ubah/{id}', 'PackagesController@editReport');

    Route::delete('/hapus/{id}', 'PackagesController@deleteReport');
    Route::delete('/hapus-daftar-paket/{id}', 'PackagesController@deletePackage');
});

Route::namespace('Summary')->prefix('ringkasan-eksekutif')->group(function () {
    Route::get('/', 'SummaryController@index');
    Route::get('/tambah', 'SummaryController@create');
    Route::post('/tambah', 'SummaryController@store');
    Route::get('/ubah/{id}', 'SummaryController@edit');
    Route::post('/ubah/{id}', 'SummaryController@update');
    Route::get('/detil/{id}', 'SummaryController@show');
    Route::delete('/hapus/{id}', 'SummaryController@delete');
});

Route::namespace('Setting')->prefix('notifikasi')->group(function () {
    Route::get('/', 'NotificationController@index');
    Route::get('/detil/{id}', 'NotificationController@show');
    Route::delete('/hapus/{id}', 'NotificationController@delete');
});

Route::namespace('Message')->prefix('pesan')->group(function () {
    Route::get('/', 'MessageController@index');
    Route::get('/tambah', 'MessageController@create');
    Route::post('/tambah', 'MessageController@store');
    Route::get('/ubah/{id}', 'MessageController@edit');
    Route::post('/ubah/{id}', 'MessageController@update');
    Route::post('/kirim/{id}', 'MessageController@sendMessage');
    Route::get('/detil/{id}', 'MessageController@show');
    Route::delete('/hapus/{id}', 'MessageController@delete');
});
